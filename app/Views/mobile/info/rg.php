<?= $this->extend('layouts/mobile/promotion') ?>

<?= $this->section('content') ?>
<div class="site-content-container" data-container-background="general">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="info-center-container responsible-gaming-container standard-form-content">
                    <h2 class="info-center-content-title">Responsible Gambling</h2>
                    <h4>RESPONSIBLE GAMBLING</h4>
                    <div class="blue_box">
                        <p><strong>KEBIJAKAN PERJUDIAN <?= getenv('APP_NAME') ?></strong></p>
                        <p><strong><?= getenv('APP_NAME') ?> </strong>berkomitmen untuk mengesahkan taruhan yang bertanggung jawab seperti halnya mempromosikan kesadaran akan masalah judi dan meningkatkan pencegahan, intervensi dan pelayanan. Kebijakan Pertanggungjawaban Permainan <strong><?= getenv('APP_NAME') ?> </strong>menetapkan komitmennya untuk meminimalisir efek negatif dari masalah judi dan untuk mempromosikan praktek perjudian yang bertanggung jawab.</p>
                        <p>Kami percaya ini tanggung jawab kami untuk anda, pelanggan kami, untuk memastikan bahwa anda menikmati pengalaman bertaruh di situs kami, sementara tetap menyadari penuh terhadap kerugian sosial dan keuangan yang terkait dengan masalah perjudian.</p>
                        <p>Dalam rangka membantu pemain kami dalam pertanggunjawaban perjudian, kami memastikan bahwa semua staf kami memiliki kesadaran pertanggunjawaban perjudian. Silahkan menghubungi kami jika anda membutuhkan informasi atau bantuan lebih lanjut.</p>
                        <p>&nbsp;</p>
                        <p><strong>SELALU DALAM KENDALI</strong></p>
                        <p>Sementara mayoritas dari pelanggan kami bermain dalam kemampuan mereka namun untuk beberapa orang akan lebih sulit membatasi diri. Untuk mengendalikan kebiasaan bermain anda kami meminta anda untuk mengingat beberapa poin di bawah ini:</p>
                        <ol>
                            <li>Hindari mengejar kekalahan.</li>
                            <li>Hanya bertaruh jika anda yakin dapat menutup kekalahan anda.</li>
                            <li>Selalu perhatikan waktu dan mengamati penggunaan dana anda.</li>
                        </ol>
                        <p>Jika anda ingin berbicara dengan seseorang yang berkaitan dengan taruhan anda, silahkan hubungi satu dari organisasi yang tertera di bawah ini.</p>
                        <p>&nbsp;</p>
                        <p><strong>APAKAH ANDA MEMILIKI MASALAH?</strong></p>
                        <p>Jika taruhan anda berdampak buruk terhadap hidup anda atau hidup orang lain, maka pertanyaan berikut mungkin dapat membantu anda untuk mengenali.&nbsp;</p>
                        <ul>
                            <li>Apakah bertaruh mencegah anda untuk menghadiri pekerjaan atau kampus anda?</li>
                            <li>Apakah anda bertaruh untuk menghabiskan waktu atau untuk lari dari kebosanan?</li>
                            <li>Apakah anda bertaruh sendiri untuk periode waktu yang lama?</li>
                            <li>Pernahkah orang lain mengkritik anda sehubungan dengan kebiasaan taruhan anda?</li>
                            <li>Apakah anda kehilangan minat kepada keluarga, teman atau hobi anda karena taruhan?</li>
                            <li>Apakah anda pernah berbohong untuk menutupi jumlah dana atau waktu yang anda habiskan untuk bertaruh?</li>
                            <li>Apakah anda berbohong, mencuri atau meminjam untuk memelihara kebiasaan bertaruh anda?</li>
                            <li>Apakah anda enggan untuk menggunakan dana taruhan anda untuk sesuatu yang lainnya?</li>
                            <li>Apakah anda bertaruh sampai anda kehilangan semua uang anda?</li>
                            <li>Setelah kalah, apakah anda merasa bahwa anda harus mencoba dan memenangkan kembali kekalahan anda secepatnya?</li>
                            <li>Jika anda kehabisan uang ketika bertaruh, apakah anda merasa kalah dan dalam keputusasaan dan merasa butuh untuk bertaruh lagi secepatnya?</li>
                            <li>Apakah argumentasi, frustasi atau kekecewaan membuat anda menginginkan untuk bertaruh lagi?</li>
                            <li>Apakah bertaruh membuat anda merasa depresi atau bahkan ingin bunuh diri?</li>
                        </ul>
                        <p>Semakin banyak pertanyaan yang anda jawab 'ya', semakin menunjukkan bahwa anda memiliki permasalahan dengan perjudian anda. Untuk berbicara dengan seseorang yang bisa memberikan nasihat dan dukungan, silahkan hubungi salah satu dari organisasi yang tertera di bawah ini.</p>
                        <p>Gamblers Anonymous adalah kumpulan dari pria dan wanita yang bergabung bersama untuk membantu masalah perjudian mereka masing-masing dan sesama pemain berat lainnya. Terdapat perkumpulan regional di seluruh dunia. Situs Pelayanan internasional Gamblers Anonymous ada di: <a href="http://www.gamblersanonymous.org/">www.gamblersanonymous.org&nbsp;</a></p>
                        <p>Gambling Therapy menyediakan bantuan dan bimbingan untuk siapapun yang terdampak buruk oleh perjudian. Situs ini bisa diakses di: <a href="http://www.gamblingtherapy.org/">www.gamblingtherapy.org</a></p>
                        <p>&nbsp;</p>
                        <p><strong>BERJUDI DIBAWAH UMUR</strong></p>
                        <p>Bertaruh dibawah batas umur 18 tahun merupakan tindakan ilegal di <strong><?= getenv('APP_NAME') ?></strong>. <strong><?= getenv('APP_NAME') ?> </strong>memiliki tanggung jawab yang serius untuk masalah ini. <strong><?= getenv('APP_NAME') ?> </strong>mempunyai hak untuk meminta bukti umur dari pelanggan manapun dan untuk melakukan pengecekan untuk memverifikasi informasi yang disediakan. Akun pelanggan mungkin akan ditutup untuk sementara dan dana akan ditahan sampai tersedia bukti yang memadai mengenai umur anda.</p>
                        <p>&nbsp;</p>
                        <p><strong>BATAS TARUHAN</strong></p>
                        <p>Di <strong><?= getenv('APP_NAME') ?></strong>, kami mengakui pentingnya penerapan batas taruhan secara efektif untuk mengurangi masalah perjudian. Dengan demikian, kami menyediakan pengguna dengan sistem untuk mengontrol jumlah uang yang dapat anda gunakan untuk berjudi. Anda dapat mengatur batas taruhan anda untuk permainan olahraga/ sportsbook dan dapat disesuaikan pada setiap kesempatan yang anda inginkan. Cukup pilih Batas Taruhan dari halaman pengaturan akun. Setelah itu masukkan jumlah maksimum Batas Taruhan lalu klik update. Setelah pengaturan Batas Taruhan, aanda akan menerima pemberitahuan melalui email dimana anda perlu mengkonfirmasikan sebelum permintaan anda diproses. Perlu diketahui bahwa jumlah yang telah anda masukkan akan menjadi jumlah maksimum taruhan yang dapat anda pasang pada periode tujuh (7) hari berturut-turut.</p>
                        <p>Untuk membantu Anda dalam pengaturan batas yang tepat, cobalah untuk merenungkan beberapa hal :</p>
                        <ul>
                            <li>Menetapkan apa yang merupakan kerugian yang diterima sebelum mulai untuk berjudi.</li>
                            <li>Perhatikan seberapa sering Anda bermain.</li>
                            <li>Perhatikan berapa lama Anda bermain dalam setiap permainan.<br><br></li>
                        </ul>
                        <p><strong>PENGECUALIAN DIRI</strong></p>
                        <p>Untuk pelanggan kami yang menginginkan untuk membatasi dirinya dari berjudi, kami menyediakan fasilitas pengecualian diri yang memungkinkan pelanggan untuk menutup akunnya untuk minimum waktu 6 bulan sampai 5 tahun sesuai dengan permintaan. Silahkan hubungi Petugas Layanan Pelanggan melalui “Live Chat” yang ada di situs untuk informasi lebih lanjut.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>