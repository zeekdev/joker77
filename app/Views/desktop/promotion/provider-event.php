<?= $this->extend('layouts/desktop/promotion') ?>

<?= $this->section('content') ?>
<div class="site-content-container" data-container-background="promotions">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="standard-container-with-sidebar">
                    <div class="promotion-side-menu" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/promotions/category-sprite.png?v=20231129-1);">
                        <h5> Category </h5>
                        <div class="top-tab-container promotion"> <a href="/desktop/promotion" data-active="false"> Semua Promosi </a> <a href="/desktop/provider-event" data-active="true"> Semua Event Provider </a> </div> <a href="/desktop/provider-event" data-active="true"> <i data-icon="all"></i> Semua Event Provider </a> <a href="/desktop/provider-event/event" data-active="false"> <i data-icon="event"></i> Event </a>
                    </div>
                    <div class="promotion-list">
                        <div class="promotion-item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_bd646d1d-3ffb-4a50-9e5e-11983b995a1b_1701065916793.png">
                            <div class="promotion-footer">
                                <div class="promotion-label">
                                    <h2>Daily Wins - MEGA GACOR LEVEL 6 TOURNAMENT [LIVE CASINO]</h2>
                                    <h3>Tanggal akhir: <span>-</span></h3>
                                </div> <button type="button" class="click-for-more-info-button" data-toggle="modal" data-target="#promotion_bd646d1d-3ffb-4a50-9e5e-11983b995a1b"> Klik Untuk Info Lebih Lanjut </button>
                            </div>
                            <div id="promotion_bd646d1d-3ffb-4a50-9e5e-11983b995a1b" class="modal" role="dialog" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                                            <h4 class="modal-title"> Daily Wins - MEGA GACOR LEVEL 6 TOURNAMENT [LIVE CASINO] </h4>
                                        </div>
                                        <div class="modal-body">
                                            <h3>Tanggal akhir: <span>-</span></h3>
                                            <p><strong>Daily Wins - MEGA GACOR Level 6 TOURNAMENT - Live Casino</strong></p>
                                            <p><strong><br></strong><strong>Event:&nbsp;<br></strong><strong>Mulai: Senin, 27 November 2023, 17:00 (WIB)<br></strong><strong>Sampai: Senin, 8 Januari 2024, 16:59 (WIB)</strong></p>
                                            <p>&nbsp;</p>
                                            <p><strong>Total Hadiah Semua Event (Slot&amp;Live Casino): IDR 30.000.000.000</strong></p>
                                            <p><strong>Total Hadiah Turnamen&amp;Cashdrop Live Casino: IDR 150.300.000 / Minggu</strong></p>
                                            <p>&nbsp;</p>
                                            <p><strong>SYARAT &amp; KETENTUAN TURNAMEN MEGA GACOR Level 6 - Live Casino:</strong></p>
                                            <p>1. 9,000 pemenang akan memenangkan hadiah uang tunai senilai Rp 901,800,000 dari 27 November 2023, 17:00 WIB hingga 8 Januari 2024, 16:59 WIB.</p>
                                            <p>2. Turnamen akan berlangsung tiga kali (3x) seminggu dari Senin ke Senin diantara 27 November 2023 17:00 WIB hingga 8 Januari 2024 16:59 WIB. Setiap turnamen akan memilih 500 pemenang. Ada 18 turnamen selama periode promo.</p>
                                            <p><strong>Jadwal Promosi:</strong><br>Senin 17:00 – Rabu 16:59<br>Rabu 17:00 – Jumat 16:59<br>Jumat 17:00 – Senin 16:59</p>
                                            <p>3. Tidak ada batas taruhan minimum dalam promosi ini.</p>
                                            <p>4. Peringkat akan berdasarkan Skor Tertinggi di setiap turnamen mingguan.</p>
                                            <p>5. Poin akan diperoleh berdasarkan Kemenangan (multiplier).</p>
                                            <p>6. Multiplier akan dihitung dari total kemenangan dibagi dengan total taruhan pada setiap putaran dan sama atau lebih&nbsp;besar dari nilai multiplier pada tabel di bawah ini<br>Multiplier 2x --&gt; Poin 10<br>Multiplier 5x --&gt; Poin 15<br>Multiplier 10x --&gt; Poin 20<br>Multiplier 15x --&gt; Poin 25<br>Multiplier 20x --&gt; Poin 50<br>Multiplier 50x --&gt; Poin 100<br>Multiplier 100x --&gt; Poin 200<br>Multiplier 200x --&gt; Poin 500<br>Multiplier 500x &amp; lebih --&gt;Poin 1000</p>
                                            <p><strong>Contoh: ​</strong><br>Jumlah taruhan Rp. 5.000 ​<br>Kemenangan: Rp. 40.000​Multiplier: 8 (40,000 / 5.000 = 8)​<br>Poin yang diperoleh: 15</p>
                                            <p>7. Game Kualifikasi: Mega Wheel dan Sweet Bonanza CandyLand.</p>
                                            <p>8. Hanya 1,200 taruhan pertama setiap harinya yang terkualifikasi untuk mengikuti promosi ini. Semua taruhan yang dipasang setelah mencapai limit taruhan tidak dihitung dalam perhitungan promosi.</p>
                                            <p>9. Jika ada dua atau lebih pemain dengan skor yang sama di papan peringkat turnamen, pemain yang mencetak skor pertama akan mendapatkan posisi yang lebih tinggi di papan peringkat</p>
                                            <p>10. Papan peringkat turnamen akan diperbaharui di kualifikasi game secara langsung.​</p>
                                            <p>11. Pragmatic Play berhak untuk mengubah, menangguhkan atau membatalkan promosi kapan saja dan tanpa pemberitahuan sebelumnya.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="promotion-item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_e58e6428-f0cf-451f-82f7-b43565020628_1701063681187.png">
                            <div class="promotion-footer">
                                <div class="promotion-label">
                                    <h2>Daily Wins - MEGA GACOR LEVEL 6 TOURNAMENT [SLOTS]</h2>
                                    <h3>Tanggal akhir: <span>-</span></h3>
                                </div> <button type="button" class="click-for-more-info-button" data-toggle="modal" data-target="#promotion_e58e6428-f0cf-451f-82f7-b43565020628"> Klik Untuk Info Lebih Lanjut </button>
                            </div>
                            <div id="promotion_e58e6428-f0cf-451f-82f7-b43565020628" class="modal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                                            <h4 class="modal-title"> Daily Wins - MEGA GACOR LEVEL 6 TOURNAMENT [SLOTS] </h4>
                                        </div>
                                        <div class="modal-body">
                                            <h3>Tanggal akhir: <span>-</span></h3>
                                            <p><strong>PP Daily Wins - MEGA GACOR Level 6 Tournament - Slot</strong></p>
                                            <p>&nbsp;</p>
                                            <p><strong>Event:&nbsp;<br>Mulai: Senin, 27 November 2023, 17:00 (WIB)<br>Sampai: Senin, 8 Januari 2024, 16:59 (WIB)</strong></p>
                                            <p>&nbsp;</p>
                                            <p><strong>Total Hadiah Semua Event (Slot&amp;Live Casino): IDR 30.000.000.000</strong></p>
                                            <p><strong>Total Hadiah Turnamen Slot: IDR 110.000.000 / Minggu</strong></p>
                                            <p><strong>Total Hadiah Cashdrop Slot: IDR 450.000.000 / Minggu</strong></p>
                                            <p>&nbsp;</p>
                                            <p><strong>SYARAT &amp; KETENTUAN TURNAMEN MEGA GACOR Level 6:</strong></p>
                                            <p>1. 630,000 pemenang akan memenangkan uang tunai senilai Rp 4,620,000,000 dari 27 November 2023, 17:00 WIB&nbsp;hingga 8 January 2024, 16:59 WIB.</p>
                                            <p>2. Turnamen harian akan berlangsung dari 17:00 WIB hingga 16:59 WIB, dengan 15,000 hadiah harian senilai Rp 110,000,000. Akan ada 42 turnamen selama periode promosi.</p>
                                            <p>3. Pemenang Turnamen berdasarkan pada Jumlah Kemenangan Single Spin Tertinggi (disesuaikan dengan nilai taruhan).</p>
                                            <p>4. Skor = (jumlah kemenangan (dalam USD) selama turnamen) / (nilai taruhan (dalam USD) selama turnamen) * 100.</p>
                                            <p><strong>Contoh perhitungan:<br></strong>Pemain 1: bertaruh sebesar $1 -&gt; menang $50 (skor = $50 : $1 x 100 = 5,000)<br>Pemain 2: bertaruh sebesar $1 -&gt; menang $5 (skor = $5 : $1 x 100 = 500)</p>
                                            <p>5. Tidak ada batas taruhan minimum dalam promosi ini.</p>
                                            <p>6. Pemain akan memenangkan hadiah berupa multiplier dari jumlah taruhan seperti yang tertera di Tabel hadiah.</p>
                                            <p>7. Jika putaran kemenangan melebihi Rp 80,000, maka hadiah akan dibayarkan berdasarkan taruhan sebesar Rp 80,000&nbsp;X multiplier yang diterima.</p>
                                            <p>8. Game yang terkualifikasi: Semua game Pragmatic Play kecuali Money Roll, Irish Charms, 888 Gold, Diamonds Forever 3 lines, Table Games, Video Poker, dan Scratch Cards.</p>
                                            <p>9. Jika ada dua atau lebih pemain dengan skor yg sama di papan peringkat turnamen, pemain yang mencetak skor pertama akan mendapatkan posisi yang lebih tinggi di papan peringkat.​</p>
                                            <p>10. Papan peringkat turnamen akan diperbaharui di kualifikasi game secara langsung.</p>
                                            <p>11. Pragmatic Play berhak untuk mengubah, menangguhkan atau membatalkan promosi kapan saja dan tanpa pemberitahuan sebelumnya.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="promotion-item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_b3cbd597-a680-4c7e-b3b9-bc08bc029869_1698724489140.gif">
                            <div class="promotion-footer">
                                <div class="promotion-label">
                                    <h2>MICROGAMING - Nexus Community Jackpot</h2>
                                    <h3>Tanggal akhir: <span>-</span></h3>
                                </div> <button type="button" class="click-for-more-info-button" data-toggle="modal" data-target="#promotion_b3cbd597-a680-4c7e-b3b9-bc08bc029869"> Klik Untuk Info Lebih Lanjut </button>
                            </div>
                            <div id="promotion_b3cbd597-a680-4c7e-b3b9-bc08bc029869" class="modal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                                            <h4 class="modal-title"> MICROGAMING - Nexus Community Jackpot </h4>
                                        </div>
                                        <div class="modal-body">
                                            <h3>Tanggal akhir: <span>-</span></h3>
                                            <p><strong>MG Cashdrop Apresiasi Pemain</strong></p>
                                            <p><strong>Total Hadiah Rp 750,000,000</strong></p>
                                            <p>&nbsp;</p>
                                            <p>&nbsp;</p>
                                            <p><strong>Event Date: 27 Oktober 2023 00:00 – 23 November 2023 23:59 (GMT+7)</strong></p>
                                            <p>&nbsp;</p>
                                            <p>&nbsp;</p>
                                            <p><strong>Permainan Terpilih:</strong></p>
                                            <p><strong>Legendary Treasures, Pong Pong Mahjong, Lara Croft: Tomb of the Sun</strong></p>
                                            <p>&nbsp;</p>
                                            <p><strong>Tipe Hadiah:</strong></p>
                                            <p><strong>Hadiah Tunai</strong></p>
                                            <p><strong>Pemain yang memainkan game yang berpartisipasi selama periode promosi memiliki peluang untuk memenangkan hadiah uang tunai secara acak. Hadiah utama hingga Rp 87,500.</strong></p>
                                            <p>&nbsp;</p>
                                            <p><strong>SYARAT &amp; KETENTUAN UMUM :</strong></p>
                                            <p>1. Tanpa taruhan minimum.</p>
                                            <p>2. Kesempatan untuk memenangkan hadiah tunai dengan memainkan game slot MG yang&nbsp;berpartisipasi.</p>
                                            <p>3. Daftar pemenang kejutan tunai harian akan diumumkan sekitar pukul 22.00 (GMT+7) pada setiap jadwal undian acara kejutan tunai.</p>
                                            <p>4. Jika pemain berhasil memenangkan hadiah pada acara undian kejutan tunai harian,maka mereka akan tetap bisa memenangkan hadiah pada jadwal undian kejutan tunai berikutnya.&nbsp;</p>
                                            <p>5. Hadiah uang tunai akan dikreditkan ke akun pemenang paling lambat 2 jam setelah pengumuman pemenang.</p>
                                            <p>6. Pemain yang memenangkan giveaway akan diberi tahu melalui pesan dalam game.</p>
                                            <p>7. MG berhak membuat keputusan akhir jika terjadi perselisihan.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="promotion-item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_a1f609f9-acd1-42ee-a2e6-5547363dd831_1699847732130.png">
                            <div class="promotion-footer">
                                <div class="promotion-label">
                                    <h2>LIVE22 - GRAND BATTLE ROYALE</h2>
                                    <h3>Tanggal akhir: <span>-</span></h3>
                                </div> <button type="button" class="click-for-more-info-button" data-toggle="modal" data-target="#promotion_a1f609f9-acd1-42ee-a2e6-5547363dd831"> Klik Untuk Info Lebih Lanjut </button>
                            </div>
                            <div id="promotion_a1f609f9-acd1-42ee-a2e6-5547363dd831" class="modal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                                            <h4 class="modal-title"> LIVE22 - GRAND BATTLE ROYALE </h4>
                                        </div>
                                        <div class="modal-body">
                                            <h3>Tanggal akhir: <span>-</span></h3>
                                            <section class="section">
                                                <div class="container">
                                                    <div class="columns">
                                                        <div class="column is-offset-2 is-8">
                                                            <div class="box">
                                                                <div class="content">
                                                                    <p><strong>LIVE22 PVP GRAND BATTLE ROYAL</strong></p>
                                                                    <p><strong>Total hadiah IDR 4,169,000,000!<br>Bersaing untuk meraih hadiah berlipat ganda!</strong></p>
                                                                    <p><strong>Periode Promosi Kampanye: 08/11/2023 00:00 – 05/12/2023 23:59 (GMT +8)</strong></p>
                                                                    <p>&nbsp;</p>
                                                                    <p><strong>Syarat dan Ketentuan:</strong></p>
                                                                    <p>1. Jumlah total hadiah = IDR 4,169,000,000</p>
                                                                    <p>2. Periode kampanye 08/11/2023 00:00 – 05/12/2023 23:59 (GMT +8)</p>
                                                                    <p>3. Selama Periode Promosi Kampanye, Live22 akan membagikan IDR 148,897,500.00 setiap hari dari kumpulan hadiah kepada 500 pemenang teratas yang berhasil memenangi putaran terbanyak dalam PVT Bonus Battle.</p>
                                                                    <p>4. Permainan yang Didukung PVP Bonus Battle: Crypto Coins, Apes Squad</p>
                                                                    <p>5. Pembagian hadiah harian:</p>
                                                                    <p>&nbsp; &nbsp; Tingkatan 1 - Jumlah Pemain 1 : Jumlah Hadiah IDR 9,904,500<br>&nbsp; &nbsp; Tingkatan 2 - Jumlah Pemain 1 : Jumlah Hadiah IDR 6,603,000<br>&nbsp; &nbsp; Tingkatan 3 - Jumlah Pemain 1 : Jumlah Hadiah IDR 3,301,500<br>&nbsp; &nbsp; Tingkatan 4 - Jumlah Pemain 1 : Jumlah Hadiah IDR 2,476,000<br>&nbsp; &nbsp; Tingkatan 5 - Jumlah Pemain 1 : Jumlah Hadiah IDR 1,650,500<br>&nbsp; &nbsp; Tingkatan 6 - Jumlah Pemain 5 : Jumlah Hadiah IDR 825,000<br>&nbsp; &nbsp; Tingkatan 7 - Jumlah Pemain 10&nbsp; &nbsp; &nbsp;: Jumlah Hadiah IDR 660,000<br>&nbsp; &nbsp; Tingkatan 8 - Jumlah Pemain 15&nbsp; &nbsp; &nbsp;: Jumlah Hadiah IDR 578,000<br>&nbsp; &nbsp; Tingkatan 9 - Jumlah Pemain 20&nbsp; &nbsp; &nbsp;: Jumlah Hadiah IDR 495,000<br>&nbsp; &nbsp; Tingkatan 10 - Jumlah Pemain 35&nbsp; &nbsp;: Jumlah Hadiah IDR 413,000<br>&nbsp; &nbsp; Tingkatan 11 - Jumlah Pemain 50&nbsp; &nbsp;: Jumlah Hadiah IDR 330,000<br>&nbsp; &nbsp; Tingkatan 12 - Jumlah Pemain 80&nbsp; &nbsp;: Jumlah Hadiah IDR 264,000<br>&nbsp; &nbsp; Tingkatan 13 - Jumlah Pemain 100 : Jumlah Hadiah IDR 198,000</p>
                                                                    <p>&nbsp; &nbsp; Tingkatan 14 - Jumlah Pemain 180 : Jumlah Hadiah IDR 132,000</p>
                                                                    <p>6. Live22 akan memasukkan hadiah ke saldo pemain yang berhak pada hari berikutnya, antara pukul 09:00~12:00 (GMT +8).</p>
                                                                    <p>&nbsp;</p>
                                                                    <p>7. Live22 berhak untuk mengubah, menangguhkan, atau membatalkan promosi kapan saja dan tanpa pemberitahuan sebelumnya.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <footer class="footer">
                                                <div class="container">
                                                    <div class="content">
                                                        <div class="columns">
                                                            <div class="column is-3">&nbsp;</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </footer>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="promotion-item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_e27a0a37-5e56-4873-b666-46c1feaa7a38_1700021884093.jpeg">
                            <div class="promotion-footer">
                                <div class="promotion-label">
                                    <h2>PLAYSTAR - PEMBERIAN AMPLOP MERAH</h2>
                                    <h3>Tanggal akhir: <span>-</span></h3>
                                </div> <button type="button" class="click-for-more-info-button" data-toggle="modal" data-target="#promotion_e27a0a37-5e56-4873-b666-46c1feaa7a38"> Klik Untuk Info Lebih Lanjut </button>
                            </div>
                            <div id="promotion_e27a0a37-5e56-4873-b666-46c1feaa7a38" class="modal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                                            <h4 class="modal-title"> PLAYSTAR - PEMBERIAN AMPLOP MERAH </h4>
                                        </div>
                                        <div class="modal-body">
                                            <h3>Tanggal akhir: <span>-</span></h3>
                                            <p><strong>Playstar Pemberian Amplop Merah</strong></p>
                                            <p><strong><br></strong><strong>Event Date:&nbsp;<br></strong><strong>15 November 2023 - 30 November 2023</strong></p>
                                            <p>&nbsp;</p>
                                            <p><strong>T &amp; C:</strong></p>
                                            <p>1. Menggambar 1 sampul merah secara acak setiap 200 permainan selama periode ini.</p>
                                            <p>2. Permainan Berbeda → Permainan akan dihitung secara individu.</p>
                                            <p>3. 1 Putaran = 1 permainan.</p>
                                            <p>4. 1 Lucky Strike = Meningkatkan peluang untuk memenangkan sampul merah.</p>
                                            <p>5. Peluang untuk menang bukanlah 100%.</p>
                                            <p>6. Dalam keadaan tertentu, PS berhak untuk memodifikasi, mengubah, atau menangguhkan kegiatan ini.</p>
                                            <p>7. PS memiliki hak interpretasi final untuk semua objeksi yang relevan yang timbul dari kegiatan ini</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="promotion-item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_3335e0ae-a916-498a-a3fd-bbd963972354_1700107593607.jpeg">
                            <div class="promotion-footer">
                                <div class="promotion-label">
                                    <h2>WORLDMATCH - HADIAH LEDAKAN NAGA</h2>
                                    <h3>Tanggal akhir: <span>-</span></h3>
                                </div> <button type="button" class="click-for-more-info-button" data-toggle="modal" data-target="#promotion_3335e0ae-a916-498a-a3fd-bbd963972354"> Klik Untuk Info Lebih Lanjut </button>
                            </div>
                            <div id="promotion_3335e0ae-a916-498a-a3fd-bbd963972354" class="modal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                                            <h4 class="modal-title"> WORLDMATCH - HADIAH LEDAKAN NAGA </h4>
                                        </div>
                                        <div class="modal-body">
                                            <h3>Tanggal akhir: <span>-</span></h3>
                                            <p style="font-weight: 400;"><strong><span style="font-style: inherit;">WM Hadiah Ledakan Naga</span></strong></p>
                                            <p style="font-weight: 400;"><strong><span style="font-style: inherit;"><br>Periode Kampanye</span></strong></p>
                                            <p style="font-weight: 400;"><span style="font-style: inherit; font-weight: inherit;">16-Nov-2023</span><span style="font-style: inherit; font-weight: inherit;">&nbsp;(Kamis</span><span style="font-style: inherit; font-weight: inherit;">) 12:00 pm</span><span style="font-style: inherit; font-weight: inherit;">～</span><span style="font-style: inherit; font-weight: inherit;">30-Nov-2023&nbsp;</span><span style="font-style: inherit; font-weight: inherit;">(Kamis)</span><span style="font-style: inherit; font-weight: inherit;">&nbsp;</span><span style="font-style: inherit; font-weight: inherit;">11:59 am</span><span style="font-style: inherit; font-weight: inherit;">&nbsp;</span><span style="font-style: inherit; font-weight: inherit;">(</span><span style="font-style: inherit; font-weight: inherit;">GMT+8</span><span style="font-style: inherit; font-weight: inherit;">)<br><br></span></p>
                                            <p style="font-weight: 400;"><strong><span style="font-style: inherit;">Deskripsi Kampanye</span></strong></p>
                                            <p style="font-weight: 400;"><span style="font-style: inherit; font-weight: inherit;">Selama periode kampanye, selama Anda bertaruh pada 6 pertandingan terbatas di 【WorldMatch】, itu akan dihitung sebagai jumlah taruhan kampanye, dan Anda dapat berpartisipasi dalam 【WM&nbsp;</span><span style="font-style: inherit; font-weight: inherit;">Hadiah Ledakan Naga</span><span style="font-style: inherit; font-weight: inherit;">】! Mereka yang telah mencapai 300 teratas dalam jumlah taruhan kampanye kumulatif dalam seminggu akan mendapatkan hadiah kampanye, total hingga 100,000k IDR! Mari kita rayakan peluncuran baru game 【</span><span style="font-style: inherit; font-weight: inherit;">The Tyragnez</span><span style="font-style: inherit; font-weight: inherit;">】&amp;【</span><span style="font-style: inherit; font-weight: inherit;">Fruit Punch</span><span style="font-style: inherit; font-weight: inherit;">】, hadiahnya akan dikirim setiap minggu!</span></p>
                                            <p style="font-weight: 400;"><span style="font-style: inherit; font-weight: inherit;">&nbsp;</span></p>
                                            <p style="font-weight: 400;"><span style="font-style: inherit; font-weight: inherit;"><strong>Melihat</strong></span></p>
                                            <p style="font-weight: 400;"><span style="font-style: inherit; font-weight: inherit;">1.Selama periode kampanye, jumlah taruhan pada 6 pertandingan terbatas akan dihitung sebagai jumlah taruhan kampanye.</span></p>
                                            <p style="font-weight: 400;"><span style="font-style: inherit; font-weight: inherit;">2.Peringkat kampanye diberi peringkat berdasarkan jumlah taruhan kampanye. Jumlah taruhan kampanye non-CNY akan dikonversi menjadi CNY sesuai dengan nilai tukar sistem untuk peringkat.</span></p>
                                            <p style="font-weight: 400;"><span style="font-style: inherit; font-weight: inherit;">Misalnya: Jika nilai tukar sistem WM saat ini adalah 4,92 THB=1 CNY, itu berarti pemain baht Thailand bertaruh 4,92 THB, dan bisa mendapatkan jumlah taruhan kampanye yang setara dengan 1 CNY.</span></p>
                                            <p style="font-weight: 400;"><span style="font-style: inherit; font-weight: inherit;">3.Pemain dengan jumlah taruhan kampanye yang sama akan diberi peringkat berdasarkan urutan kronologis.</span></p>
                                            <p style="font-weight: 400;"><span style="font-style: inherit; font-weight: inherit;">4.Hadiah akan dikonversi ke mata uang pemain yang menang sesuai dengan nilai tukar sistem untuk distribusi.</span></p>
                                            <p style="font-weight: 400;"><span style="font-style: inherit; font-weight: inherit;">5.Hadiah akan otomatis dikreditkan ke akun pemain sebelum pukul 18:00 (GMT+8) pada tanggal 23 November (Kamis) dan 30 November (Kamis).</span></p>
                                            <p style="font-weight: 400;"><span style="font-style: inherit; font-weight: inherit;">6.Dalam keadaan khusus, WorldMatch berhak memodifikasi, mengubah, atau menangguhkan aktivitas ini.</span></p>
                                            <p style="font-weight: 400;"><span style="font-style: inherit; font-weight: inherit;">7.WorldMatch berhak interpretasi akhir atas segala keberatan dan pertanyaan terkait yang timbul dari aktivitas ini.</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>