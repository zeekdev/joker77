<?= $this->extend('layouts/desktop/arcade') ?>

<?= $this->section('content') ?>

<div data-container-background="arcade" style="background-image: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/backgrounds/arcade.jpg?v=20231115);">
    <div class="arcade-banner-container">
        <picture>
            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/banners/arcade/banner.webp?v=20231115" type="image/webp" />
            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/banners/arcade/banner.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/banners/arcade/banner.png?v=20231115" />
        </picture>
        <picture>
            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-1.webp?v=20231115" type="image/webp" />
            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-1.png?v=20231115" type="image/png" /><img class="arcade-coin-1 float-effect-1s" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-1.png?v=20231115" />
        </picture>
        <picture>
            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-2.webp?v=20231115" type="image/webp" />
            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-2.png?v=20231115" type="image/png" /><img class="arcade-coin-2 float-effect-1s" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-2.png?v=20231115" />
        </picture>
        <picture>
            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-3.webp?v=20231115" type="image/webp" />
            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-3.png?v=20231115" type="image/png" /><img class="arcade-coin-3 float-effect-2s-d" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-3.png?v=20231115" />
        </picture>
    </div>
    <div class="container arcade-container">
        <div class="row">
            <div class="col-md-12">
                <div class="games-list-container">
                    <div class="provider-outer-container">
                        <?= $this->include('components/provider-arcade') ?>
                        <div class="vendor-name"> MicroGaming </div>
                        <div class="filter-section">
                            <div class="category-filter" id="filter_categories">
                                <div class="category-filter-link active" data-category=""> Semua permainan </div>
                            </div> <input type="text" id="filter_input" placeholder="Cari Permainan">
                        </div>
                    </div>
                    <div class="game-list" id="game_list" style="--star-on-icon: url(//nx-cdn.trgwl.com/Images/icons/star-on.svg?v=20231115); --star-off-icon: url(//nx-cdn.trgwl.com/Images/icons/star-off.svg?v=20231115);"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection() ?>

<?= $this->section('script') ?>
<script>
    window.addEventListener('DOMContentLoaded', () => {
        initializeArcadeGames({
            directoryPath: '//nx-cdn.trgwl.com/Images/providers/',
            provider: 'MICROGAMING',
            translations: {
                playNow: 'MAIN',
                demo: 'COBA'
            }
        });
    });
</script>
<?= $this->endSection() ?>    