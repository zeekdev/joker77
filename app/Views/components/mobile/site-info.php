<div class="footer-links-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="footer-links">
                    <li> <a href="/mobile/about-us"> Tentang <?= getenv('APP_NAME') ?> </a> </li>
                    <li> <a href="/mobile/responsible-gaming"> Responsible Gambling </a> </li>
                    <li> <a href="/mobile/faq"> Pusat Bantuan </a> </li>
                    <li> <a href="/mobile/terms-of-use"> Syarat dan Ketentuan </a> </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="site-info">
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-sm-12"> ©2023 <?= getenv('APP_NAME') ?>. All rights reserved | 18+ </div>
            </div>
        </div>
    </div>
</div>