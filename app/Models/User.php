<?php

namespace App\Models;

use CodeIgniter\Model;

class User extends Model
{
    protected $table            = 'users';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $allowedFields    = ['email', 'username', 'password', 'created_at'];

    public function getUser($id)
    {
        return $this->db->table('users')
            ->where('users.id', $id)
            ->join('banks', 'banks.id = users.bank_id')
            ->get()
            ->getResult()[0];
    }
}
