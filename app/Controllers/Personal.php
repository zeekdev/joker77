<?php

namespace App\Controllers;

use App\Models\User;
use App\Models\Bank;
use App\Models\DepositModel;
use App\Controllers\BaseController;

class Personal extends BaseController
{
    public function __construct()
    {
        $this->user = new User();
        $this->bank = new Bank();
        $this->deposit_model = new DepositModel();
    }

    public function getBanks()
    {
        return $this->bank->where('user_id', session()->get('id'))->findAll();
    }

    public function index()
    {
        $id = session()->get('id');
        $user = $this->user->find($id);
        $bank = $this->getBanks()[0];
        $deposits = $this->deposit_model
            ->where('user_id', session()->get('id'))
            ->orderBy('id', 'desc')
            ->findAll();

        if($this->isMobile()) {
            return view('mobile/user/account/account', compact('user', 'deposits', 'bank'));
        }

        return view('desktop/user/personal/account', compact('user', 'deposits', 'bank'));
    }

    public function bank_account()
    {
        $banks = $this->getBanks();
        return view('desktop/user/personal/bank-account', compact('banks'));
    }

    public function bank_account2()
    {
        $banks = $this->getBanks();
        return view('mobile/user/account/bank', compact('banks'));
    }

    public function save_bank()
    {
        $bank = $this->request->getVar('Bank');
        $norek = $this->request->getVar('AccountNo');
        $nama_rekening = $this->request->getVar('AccountName');
        $user_id = session()->get('id');

        $this->bank->insert(compact('bank', 'norek', 'nama_rekening', 'user_id'));

        return redirect()->back()->with('msg', 'Akun berhasil disimpan.');
    }

    public function new_message()
    {
        if($this->isMobile()) {
            return redirect()->to('mobile/messages/inbox');
        }

        return redirect()->to('desktop/messages/inbox');
    }

    public function password()
    {
        if($this->isMobile()) {
            return redirect()->to('mobile/password')->with('msg', 'ok');
        }
        
        return redirect()->to('desktop/password')->with('msg', 'ok');
    }
}
