<?= $this->extend('layouts/mobile/auth', ['type' => 'login']) ?>

<?= $this->section('content') ?>
<div class="standard-form-container login-outer-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <form action="/Account/Login" method="post"><input name="__RequestVerificationToken" type="hidden" value="-iZKHrdgL473aVQTsS4tCLUvKn8OxJ67wR6jD0cfwGIyR6QlG3wLu1TFrpuQrskv6kkiYktQqrGPkHGprINnQIrfyTHnUJgQsMGQldmHiSY1">
                    <div class="login-panel">
                        <div class="form-group"> <label> Nama Pengguna </label> <input type="text" name="Username" class="form-control" placeholder="Nama Pengguna"> </div>
                        <div class="form-group standard-password-field"> <label> Kata Sandi </label> <input type="password" name="Password" class="form-control" placeholder="Kata Sandi" id="password_input"> <i class="glyphicon glyphicon-eye-close" id="password_input_trigger"></i> </div>
                        <div class="standard-button-group"> <a href="/mobile/forgot-password" class="forgot-password-link"> Lupa kata sandi Anda? </a> </div>
                        <div class="standard-button-group"> <input type="submit" class="standard-button" value="Masuk"> </div>
                        <div class="standard-button-group register-field"> Belum punya akun? <a href="/mobile/register" class="register-button"> Daftar Sekarang </a> </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>