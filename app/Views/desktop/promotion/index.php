<?= $this->extend('layouts/desktop/promotion') ?>

<?= $this->section('content') ?>
<div class="site-content-container" data-container-background="promotions">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="standard-container-with-sidebar">
                    <div class="promotion-side-menu" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/promotions/category-sprite.png?v=20231115);">
                        <h5> Category </h5>
                        <div class="top-tab-container promotion"> <a href="/desktop/promotion" data-active="true"> Semua Promosi </a> <a href="/desktop/provider-event" data-active="false"> Semua Event Provider </a> </div> <a href="/desktop/promotion" data-active="true"> <i data-icon="all"></i> Semua Promosi </a> <a href="/desktop/promotion/new-member" data-active="false"> <i data-icon="new-member"></i> Member Baru </a> <a href="/desktop/promotion/special" data-active="false"> <i data-icon="special"></i> Spesial </a>
                    </div>
                    <div class="promotion-list">
                        <div class="promotion-item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_75287e9f-fc07-44fe-82a0-12e507d46721_1698341365933.png" />
                            <div class="promotion-footer">
                                <div class="promotion-label">
                                    <h2>HADIAH UANG TUNAI 350 JUTA</h2>
                                    <h3>Tanggal akhir: <span>-</span></h3>
                                </div> <button type="button" class="click-for-more-info-button" data-toggle="modal" data-target="#promotion_75287e9f-fc07-44fe-82a0-12e507d46721"> Klik Untuk Info Lebih Lanjut </button>
                            </div>
                            <div id="promotion_75287e9f-fc07-44fe-82a0-12e507d46721" class="modal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                                            <h4 class="modal-title"> HADIAH UANG TUNAI 350 JUTA </h4>
                                        </div>
                                        <div class="modal-body">
                                            <h3>Tanggal akhir: <span>-</span></h3>
                                            <p>* SPECIAL BONUS EVENT GRAND PRIZE UNTUK SEMUA MEMBER AKTIF<br />* AMBIL HADIAH UANG TUNAI SEKARANG JUGA<br />* HADIAH YANG TUNAI LANGSUNG TANPA DI UNDI</p>
                                            <p>SYARAT MUDAH MENDAPATKAN HADIAH GRATIS DARI <strong><span style="color: #ff0000;">PRAGMATIC218</span></strong><br />NILAI TURNOVER YANG WAJIB DICAPAI DAN HADIAH YANG DIDAPAT DALAM 1 BULAN :</p>
                                            <p>Player Yang Mencapai TURNOVER 50M - Mendapatkan Uang Tunai Rp 150.000.000<br />Player Yang Mencapai TURNOVER 30M - Mendapatkan Uang Tunai Rp 90.000.000<br />Player Yang Mencapai TURNOVER 10M - Mendapatkan Uang Tunai Rp 50.000.000<br />Player Yang Mencapai TURNOVER 5M - Mendapatkan Uang Tunai Rp 22.500.000<br />Player Yang Mencapai TURNOVER 3M - Mendapatkan Uang Tunai Rp 10.000.000<br />Player Yang Mencapai TURNOVER 1M - Mendapatkan Uang Tunai Rp 3.500.000<br />Player Yang Mencapai TURNOVER 500JT - Mendapatkan Uang Tunai Rp 1.500.000<br />Player Yang Mencapai TURNOVER 200JT - Mendapatkan Uang Tunai Rp 600.000<br />Player Yang Mencapai TURNOVER 100JT - Mendapatkan Uang Tunai Rp 200.000</p>
                                            <p>SYARAT DAN KETENTUAN :<br />1. PROMO BERLANGSUNG DISETIAP BULAN.<br />2. PEMENANG AKAN DIUMUMKAN SETIAP AWAL BULAN TANGGAL 2 SAMPAI TANGGAL 5.<br />3. MEMBER HARUS MENCAPAI TURNOVER YANG TELAH DI TETAPKAN <strong><span style="color: #ff0000;">PRAGMATIC218</span></strong>.<br />4. TOTAL TURNOVER DIHITUNG SELAMA 1 BULAN PENUH HANYA KHUSUS PERMAINAN SLOT GAMES (SEMUA PROVIDER) HADIAH LANGSUNG DIBAGIKAN TANPA DIUNDI.<br />5. TURNOVER TERHITUNG DARI TANGGAL 1 SAMPAI TANGGAL 30 ATAU TANGGAL 31 (AKHIR BULAN).<br />5. MEMBER TETAP MENDAPATKAN BONUS ROLLINGAN MINGGUAN 0.5% YANG DIBAGIKAN SETIAP HARI SENIN.<br />6. UANG TUNAI AKAN DI BAGIKAN LANGSUNG KE USER ID ATAU BISA DI WITHDRAW LANGSUNG TANPA TURNOVER.<br />7. JIKA TERINDIKASI MELAKUKAN KECURANGAN, MAKA KAMI AKAN MEMBLOKIR ID ANDA DAN PROMO KAMI ANGGAP HANGUS TANPA PEMBERITAHUAN.<br />8. HATI - HATI TERHADAP PENIPUAN YANG MENGATASNAMAKAN WEBSITE <strong><span style="color: #ff0000;">PRAGMATIC218</span></strong>, KAMI TIDAK MEMINTA TRANSFER UANG ATAUPUN LAINNYA.<br />9. SEMUA KEPUTUSAN PIHAK <strong><span style="color: #ff0000;">PRAGMATIC218</span></strong> MUTLAK DAN TIDAK DAPAT DIGANGGU GUGAT !!!</p>
                                            <p>&nbsp;</p>
                                            <p>&nbsp;</p>
                                            <p>Main &amp; menang terus dan dapatkan UANG TUNAI LANGSUNG GRATIS, Buruan ajak teman bergabung di <span style="color: #ff0000;"><strong>PRAGMATIC218</strong></span> jangan sampai ketinggalan guys !!!</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="promotion-item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_59908d36-0c1f-48a9-a9d0-1a4be0665bb0_1698327402080.png" />
                            <div class="promotion-footer">
                                <div class="promotion-label">
                                    <h2>Bonus Depan Deposit Harian Slot Game 10% Tiap Harinya</h2>
                                    <h3>Tanggal akhir: <span>2052-12-02</span></h3>
                                </div> <button type="button" class="click-for-more-info-button" data-toggle="modal" data-target="#promotion_59908d36-0c1f-48a9-a9d0-1a4be0665bb0"> Klik Untuk Info Lebih Lanjut </button>
                            </div>
                            <div id="promotion_59908d36-0c1f-48a9-a9d0-1a4be0665bb0" class="modal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                                            <h4 class="modal-title"> Bonus Depan Deposit Harian Slot Game 10% Tiap Harinya </h4>
                                        </div>
                                        <div class="modal-body">
                                            <h3>Tanggal akhir: <span>2052-12-02</span></h3>
                                            <ul>
                                                <li><strong>Syarat &amp; Ketentuan :</strong></li>
                                            </ul>
                                            <p>* Promo Bonus Deposit hanya Berlaku Untuk Permainan SLOT,Hubungi CS untuk Informasi Lebih Lengkapnya.</p>
                                            <p>* Minimal deposit adalah 10 ribu.</p>
                                            <p>* Maksimal Bonus Yang Diberikan adalah : IDR 500.000,-</p>
                                            <p>* Turn Over yang harus di capai 3x.</p>
                                            <p>* 1 hari bisa claim sebanyak 3x.</p>
                                            <p>* Jika saldo lock habis dan ingin claim bonus lagi mohon hubungi COSTUMER SERVICE untuk bukakan locknya.</p>
                                            <p>* Cara ambil: Login ke akun, klik tanda kurang (menu) di pojok kanan atas lalu pilih "bonus" lalu klik "ambil promosi ini".</p>
                                            <p>* Bonus akan kami tarik kembali jika terjadi indikasi kecurangan seperti stuck spin dan sejenisnya.</p>
                                            <p>* promo bonus tidak dapat di gabungkan dengan promosi lainnya</p>
                                            <p>* Syarat dan ketentuan Umum Pragmatic218 berlaku.</p>
                                            <p>* Promo ini sewaktu waktu dapat berubah tanpa permberitahuan sebelum nya,Terima Kasih.</p>
                                            <div class="expiration-countdown-container">
                                                <div class="expiration-countdown expiration_countdown" data-expiration-date="02-Dec-2052 12:00:00 AM">
                                                    <div data-section="days"> HARI <div data-value="days">10609</div>
                                                    </div>
                                                    <div data-section="hours"> JAM <div data-value="hours">4</div>
                                                    </div>
                                                    <div data-section="minutes"> MENIT <div data-value="minutes">17</div>
                                                    </div>
                                                    <div data-section="seconds"> DETIK <div data-value="seconds">4</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="promotion-item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_ac9ca828-06f5-4ba1-84b8-4fea9b52f5d5_1698327440910.png" />
                            <div class="promotion-footer">
                                <div class="promotion-label">
                                    <h2>deposit pulsa tanpa potongan</h2>
                                    <h3>Tanggal akhir: <span>-</span></h3>
                                </div> <button type="button" class="click-for-more-info-button" data-toggle="modal" data-target="#promotion_ac9ca828-06f5-4ba1-84b8-4fea9b52f5d5"> Klik Untuk Info Lebih Lanjut </button>
                            </div>
                            <div id="promotion_ac9ca828-06f5-4ba1-84b8-4fea9b52f5d5" class="modal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                                            <h4 class="modal-title"> deposit pulsa tanpa potongan </h4>
                                        </div>
                                        <div class="modal-body">
                                            <h3>Tanggal akhir: <span>-</span></h3>
                                            <p><em><strong>DEPOSIT PULSA TANPA POTONGAN</strong></em></p>
                                            <p>✅ Maxsimal deposit pulsa tanpa potongan perhari adalah IDR 800,000.<br />✅ DEPO pulsa tanpa potongan tidak dapat bermain TOGEL dan POKER.<br />✅ Tidak diperbolehkan mengambil bonus apapun di dalam akun.<br />✅ Jika sudah depo dan tidak bermain langsung melakukan wd maka di potong 35%.<br />✅ Syarat penarikan WD TO 3X contoh: depo IDR 20.000 x 3 TO = IDR 60.000.<br />✅ Berlaku untuk provider TELKOMSEL dan XL.</p>
                                            <p>⛔️ Pihak PRAGMATIC218 berhak membatalkan promo jika terdapat indikasi kecurangan.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="promotion-item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_54740598-1654-43d9-9fee-a79812587b10_1698327467457.png" />
                            <div class="promotion-footer">
                                <div class="promotion-label">
                                    <h2>BONUS WELCOME 100% UNTUK SLOT GAME</h2>
                                    <h3>Tanggal akhir: <span>2026-01-24</span></h3>
                                </div> <button type="button" class="click-for-more-info-button" data-toggle="modal" data-target="#promotion_54740598-1654-43d9-9fee-a79812587b10"> Klik Untuk Info Lebih Lanjut </button>
                            </div>
                            <div id="promotion_54740598-1654-43d9-9fee-a79812587b10" class="modal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                                            <h4 class="modal-title"> BONUS WELCOME 100% UNTUK SLOT GAME </h4>
                                        </div>
                                        <div class="modal-body">
                                            <h3>Tanggal akhir: <span>2026-01-24</span></h3>
                                            <p><span style="color: #ff0000;"><strong>BONUS NEW MEMBER SLOT 100%</strong></span></p>
                                            <p><span style="color: #ff0000;"><strong>Syarat dan Ketentuan :</strong></span></p>
                                            <ul>
                                                <li><span style="color: #ff0000;">Hanya berlaku untuk Member Baru Untuk Semua Permainan SLOT GAME<br /></span></li>
                                                <li><span style="color: #ff0000;">Minimal Deposit Pertama Rp 50.000&nbsp; (Lima puluh Ribu Rupiah).</span></li>
                                                <li><span style="color: #ff0000;">Maximum Bonus Adalah Sebesar Rp 1.000.000 (Satu Juta Rupiah).</span></li>
                                                <li><span style="color: #ff0000;">syarat Turnover/ Nilai Taruhan untuk mendapatkan bonus ini adalah TO 20x ( Nilai Deposit + Bonus 100% ) dalam 1 hari</span><br /><span style="color: #ff0000;">Contoh : Deposit Rp 100.000 + Bonus 100% x TO 20X.</span><br /><span style="color: #ff0000;">100.000 + 100.000 x 20 = 4.000.000 <br /></span></li>
                                                <li><span style="color: #ff0000;">Masuk ke AKUN - KLAIM BONUS - AMBIL PROMOSI (di pilih promosi yang sesuai) dan masukan jumlah yang ingin di ikuti.</span></li>
                                                <li><span style="color: #ff0000;">Jika sudah capai syarat anda Harus masuk ke dalam ID anda klik AKUN - KLAIM BONUS.</span></li>
                                                <li><span style="color: #ff0000;">promo bonus tidak dapat di gabungkan dengan promosi lainnya</span></li>
                                                <li><span style="color: #ff0000;">Yang Tidak Diperbolehkan Adalah Sebagai Berikut : Bonus Hunter,Kesamaan IP dsb.</span></li>
                                                <li><span style="color: #ff0000;">Apabila ada kesalahan dan kesamaan data member, kesamaan alamat IP, menggunakan IP Luar (Selain Indonesia) / Hidden IP address / IP Proxy , atau melakukan taruhan menggunakan mesin, Perusahaan berhak membatalkan Bonus Dan Kemenangan tersebut.</span></li>
                                                <li><span style="color: #ff0000;">Pihak <strong>Pragmatic218 </strong>Berhak menarik semua bonus dan jumlah kemenangan akan disita Jika Kedapatan Melakukan Kecurangan.</span></li>
                                            </ul>
                                            <div class="expiration-countdown-container">
                                                <div class="expiration-countdown expiration_countdown" data-expiration-date="24-Jan-2026 12:00:00 AM">
                                                    <div data-section="days"> HARI <div data-value="days">800</div>
                                                    </div>
                                                    <div data-section="hours"> JAM <div data-value="hours">4</div>
                                                    </div>
                                                    <div data-section="minutes"> MENIT <div data-value="minutes">17</div>
                                                    </div>
                                                    <div data-section="seconds"> DETIK <div data-value="seconds">4</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="promotion-item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_50ed0933-0fb4-4c3a-85be-11ea04bd75ea_1698327491690.png" />
                            <div class="promotion-footer">
                                <div class="promotion-label">
                                    <h2>CASHBACK SPORTSBOOK 10% SETIAP MINGGUNYA</h2>
                                    <h3>Tanggal akhir: <span>2029-06-01</span></h3>
                                </div> <button type="button" class="click-for-more-info-button" data-toggle="modal" data-target="#promotion_50ed0933-0fb4-4c3a-85be-11ea04bd75ea"> Klik Untuk Info Lebih Lanjut </button>
                            </div>
                            <div id="promotion_50ed0933-0fb4-4c3a-85be-11ea04bd75ea" class="modal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                                            <h4 class="modal-title"> CASHBACK SPORTSBOOK 10% SETIAP MINGGUNYA </h4>
                                        </div>
                                        <div class="modal-body">
                                            <h3>Tanggal akhir: <span>2029-06-01</span></h3>
                                            <p><span style="color: #ff0000;"><strong>CASHBACK SPORTS 10% SETIAP MINGGUNYA</strong></span></p>
                                            <p><span style="color: #ff0000;">Syarat &amp; Ketentuan:</span></p>
                                            <ul>
                                                <li><span style="color: #ff0000;">Minimal Cashback adalah Rp. 50.000 (Lima Puluh Ribu Rupiah)</span></li>
                                                <li><span style="color: #ff0000;">Maksimal Cashback adalah Rp. 25.000.000 (Dua Puluh Lima Juta Rupiah).</span></li>
                                                <li><span style="color: #ff0000;">Setiap id akan otomatis dapat mengikuti promo <strong>CASHBACK SPORTS 10% SETIAP MINGGUNYA</strong>.<br /></span></li>
                                                <li><span style="color: #ff0000;">Winlose akan dihitung setiap hari <strong>Senin hingga hari Minggu</strong> setiap minggunya, dan akan <strong>dikreditkan setiap hari SELASA</strong> pada minggu berikutnya.</span></li>
                                                <li><span style="color: #ff0000;">Minimal melakukan bettingan di 3 tim yang berbeda untuk mengikuti promo bonus <strong>CASHBACK SPORTS 10% SETIAP MINGGUNYA</strong>.</span></li>
                                                <li><span style="color: #ff0000;">Bonus dan Taruhan menang ditarik kembali apabila melakukan taruhan : di dua sisi ( SAFETY BET/ OPPOSITE BETTING ), OTHER SPORTS.</span></li>
                                                <li><span style="color: #ff0000;">Bonus &amp; Kemenangan di batalkan apabila ada kesamaan IP dan kesalahan data member.<br /></span></li>
                                                <li><span style="color: #ff0000;">Pihak <strong>Pragmatic218 </strong>Berhak menarik semua bonus dan jumlah kemenangan akan disita Jika Kedapatan Melakukan Kecurangan.</span></li>
                                                <li><span style="color: #ff0000;"><strong>Pragmatic218 </strong>memiliki kewenangan penuh untuk merubah, menghentikan dan membatalkan promosi tanpa pemberitahuan sebelumnya.</span></li>
                                            </ul>
                                            <div class="expiration-countdown-container">
                                                <div class="expiration-countdown expiration_countdown" data-expiration-date="01-Jun-2029 12:00:00 AM">
                                                    <div data-section="days"> HARI <div data-value="days">2024</div>
                                                    </div>
                                                    <div data-section="hours"> JAM <div data-value="hours">4</div>
                                                    </div>
                                                    <div data-section="minutes"> MENIT <div data-value="minutes">17</div>
                                                    </div>
                                                    <div data-section="seconds"> DETIK <div data-value="seconds">4</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>