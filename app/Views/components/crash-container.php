<div data-container-background="crash-game" style="background-image: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/backgrounds/crash-game.jpg?v=20231115);">
    <div class="crash-game-banner-container">
        <picture>
            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/banners/crash-game/banner.webp?v=20231115" type="image/webp" />
            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/banners/crash-game/banner.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/banners/crash-game/banner.png?v=20231115" />
        </picture>
        <picture>
            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-1.webp?v=20231115" type="image/webp" />
            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-1.png?v=20231115" type="image/png" /><img class="crash-game-coin-1 float-effect-1s" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-1.png?v=20231115" />
        </picture>
        <picture>
            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-2.webp?v=20231115" type="image/webp" />
            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-2.png?v=20231115" type="image/png" /><img class="crash-game-coin-2 float-effect-1s" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-2.png?v=20231115" />
        </picture>
        <picture>
            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-3.webp?v=20231115" type="image/webp" />
            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-3.png?v=20231115" type="image/png" /><img class="crash-game-coin-3 float-effect-2s-d" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-3.png?v=20231115" />
        </picture>
    </div>
    <div class="container crash-game-container">
        <div class="row">
            <div class="col-md-12">
                <div class="games-list-container">
                    <div class="provider-outer-container">
                        <div class="provider-slide" id="crash_game_providers"> <i class="glyphicon glyphicon-chevron-left left_trigger"></i>
                            <main> <a href="/desktop/crash-game/pragmatic" class="slide-item">
                                    <picture>
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/PPLIVECASINO.webp?v=20231115" type="image/webp" />
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/PPLIVECASINO.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/PPLIVECASINO.png?v=20231115" />
                                    </picture>
                                    <picture>
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/PPLIVECASINO-active.webp?v=20231115" type="image/webp" />
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/PPLIVECASINO-active.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/PPLIVECASINO-active.png?v=20231115" />
                                    </picture>
                                </a> <a href="/desktop/crash-game/spribe" class="slide-item">
                                    <picture>
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/SPRIBE.webp?v=20231115" type="image/webp" />
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/SPRIBE.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/SPRIBE.png?v=20231115" />
                                    </picture>
                                    <picture>
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/SPRIBE-active.webp?v=20231115" type="image/webp" />
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/SPRIBE-active.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/SPRIBE-active.png?v=20231115" />
                                    </picture>
                                </a> <a href="/desktop/crash-game/microgaming" class="slide-item">
                                    <picture>
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/MICROGAMING.webp?v=20231115" type="image/webp" />
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/MICROGAMING.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/MICROGAMING.png?v=20231115" />
                                    </picture>
                                    <picture>
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/MICROGAMING-active.webp?v=20231115" type="image/webp" />
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/MICROGAMING-active.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/MICROGAMING-active.png?v=20231115" />
                                    </picture>
                                </a> <a href="/desktop/crash-game/spinix" class="slide-item">
                                    <picture>
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/SPINIX.webp?v=20231115" type="image/webp" />
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/SPINIX.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/SPINIX.png?v=20231115" />
                                    </picture>
                                    <picture>
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/SPINIX-active.webp?v=20231115" type="image/webp" />
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/SPINIX-active.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/SPINIX-active.png?v=20231115" />
                                    </picture>
                                </a> <a href="/desktop/crash-game/advantplay-mini-game" class="slide-item">
                                    <picture>
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/ADVANTPLAYMINIGAME.webp?v=20231115" type="image/webp" />
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/ADVANTPLAYMINIGAME.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/ADVANTPLAYMINIGAME.png?v=20231115" />
                                    </picture>
                                    <picture>
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/ADVANTPLAYMINIGAME-active.webp?v=20231115" type="image/webp" />
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/ADVANTPLAYMINIGAME-active.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/ADVANTPLAYMINIGAME-active.png?v=20231115" />
                                    </picture>
                                </a> <a href="/desktop/crash-game/joker" class="slide-item">
                                    <picture>
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/JOKER.webp?v=20231115" type="image/webp" />
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/JOKER.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/JOKER.png?v=20231115" />
                                    </picture>
                                    <picture>
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/JOKER-active.webp?v=20231115" type="image/webp" />
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/JOKER-active.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/JOKER-active.png?v=20231115" />
                                    </picture>
                                </a> <a href="/desktop/crash-game/dragoonsoft" class="slide-item">
                                    <picture>
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/DRAGOONSOFT.webp?v=20231115" type="image/webp" />
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/DRAGOONSOFT.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/DRAGOONSOFT.png?v=20231115" />
                                    </picture>
                                    <picture>
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/DRAGOONSOFT-active.webp?v=20231115" type="image/webp" />
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/DRAGOONSOFT-active.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/DRAGOONSOFT-active.png?v=20231115" />
                                    </picture>
                                </a> <a href="/desktop/crash-game/funky-games" class="slide-item">
                                    <picture>
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/SBOFUNKYGAME.webp?v=20231115" type="image/webp" />
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/SBOFUNKYGAME.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/SBOFUNKYGAME.png?v=20231115" />
                                    </picture>
                                    <picture>
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/SBOFUNKYGAME-active.webp?v=20231115" type="image/webp" />
                                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/SBOFUNKYGAME-active.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/crash-game/SBOFUNKYGAME-active.png?v=20231115" />
                                    </picture>
                                </a> </main> <i class="glyphicon glyphicon-chevron-right right_trigger"></i>
                        </div>
                        <div class="vendor-name"> <?= $title ?> </div>
                        <div class="filter-section">
                            <div class="category-filter" id="filter_categories">
                                <div class="category-filter-link active" data-category=""> Semua permainan </div>
                            </div> <input type="text" id="filter_input" placeholder="Cari Permainan">
                        </div>
                    </div>
                    <div class="game-list" id="game_list" style="--star-on-icon: url(//nx-cdn.trgwl.com/Images/icons/star-on.svg?v=20231115); --star-off-icon: url(//nx-cdn.trgwl.com/Images/icons/star-off.svg?v=20231115);"></div>
                </div>
            </div>
        </div>
    </div>
</div>