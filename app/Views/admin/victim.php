<?= $this->extend('layouts/admin') ?>

<?= $this->section('content') ?>
<div class="container">
	<div class="row py-4">
		<div class="col-md-3 mb-4">
			<ul class="list-group" data-bs-theme="dark">
			  <li class="list-group-item">
			  	<a href="/cp/home">Home</a>
			  </li>
			  <li class="list-group-item bg-danger">
			  	<a href="/cp/victims">Victim</a>
			  </li>
			  <li class="list-group-item">
			  	<a href="/cp/deposit">Deposit</a>
			  </li>
			</ul>
		</div>
		<div class="col-md-9">
			<div class="row">
				<div class="col-md-12">
					<div class="d-flex justify-content-between mb-2">
						<h5 class="fw-bold">Victims</h5>
					</div>
					<table class="table table-bordered border-danger text-center" data-bs-theme="dark">
						<thead>
							<tr>
								<th class="bg-danger">ID</th>
								<th class="bg-danger">Tgl Masuk</th>
								<th class="bg-danger">Username</th>
								<th class="bg-danger">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($users as $row): ?>
							<tr>
								<td><?= $row['id'] ?></td>
								<td><?= date_format(date_create($row['created_at']), 'd/m/Y H:i:s A') ?></td>
								<td><?= $row['username'] ?></td>
								<td>
									<a href="/cp/victims/delete?id=<?= $row['id'] ?>" title="Banned" class="btn btn-sm btn-danger"><i class="bi bi-trash"></i></a>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
					<h6 class="fw-bold">Total: <span class="text-danger"><?= $total_user ?></span></h6>
				</div>
			</div>
		</div>
	</div>	
</div>
<?= $this->endSection() ?>