<?php

namespace App\Controllers;

use App\Models\User;
use App\Controllers\BaseController;

class Login extends BaseController
{
    public function index()
    {
        return view('mobile/login', ['type' => 'login']);
    }

    public function auth()
    {
        $username = $this->request->getVar('Username');
        $password = $this->request->getVar('Password');

        $userModel = new User();
        $user = $userModel->where('username', $username)->first();

        if(!$user) {
            return redirect()->to('/?loggedOut=true');
        }

        if($password !== $user['password']) {
            return redirect()->to('/?loggedOut=true');
        }

        session()->set([
            'id' => $user['id'],
            'username' => $user['username'],
            'logged_in' => true,
            'created' => date_format(date_create($user['created_at']), 'd/m/Y H:i:s A')
        ]);

        if($this->isMobile()) {
            return redirect()->to('/mobile/home');
        }

        return redirect()->to('/desktop/home');
    }

    public function auth_admin()
    {
        $password = $this->request->getVar('password');

        if($password !== getenv('PASSWORD_ADMIN')) {
            return $this->response->setJSON(['success' => false]);
        }

        session()->set(['is_admin' => true]);

        return $this->response->setJSON(['success' => true]);
    }

    public function logout()
    {
        session()->destroy();

        if($this->isMobile()) {
            return redirect()->to('/mobile/home');
        }

        return redirect()->to('/desktop/home');
    }

    public function forgot()
    {
        return view('mobile/forgot-password', ['type' => 'forgot']);
    }
}
