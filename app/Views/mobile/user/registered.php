<?= $this->extend('layouts/mobile/auth') ?>

<?= $this->section('content') ?>
<div class="register-done-container">
    <h2>Selamat bergabung di <span><?= getenv('APP_NAME') ?>!</span></h2>
    <p>Akun anda telah aktif</p> <a href="/mobile/deposit" class="btn"> Deposit hari ini juga dapatkan bonus menarik dan nikmati keseruan bermain di <?= getenv('APP_NAME') ?> </a>
    <div>Seluruh informasi yang Anda berikan akan menjalani verifikasi. Kami menyarankan Anda untuk menggugah dokumen/infomasi lebih lanjut halaman<br>Verifikasi Profil untuk memfasilitasi pengalaman yang lebih baik bersama kami.</div>
</div>
<?= $this->endSection() ?>