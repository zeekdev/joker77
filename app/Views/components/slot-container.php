<div class="slots-banner-container">
    <picture>
        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/banners/slots/banner.webp?v=20231108-2" type="image/webp" />
        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/banners/slots/banner.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/banners/slots/banner.png?v=20231108-2" />
    </picture>
    <picture>
        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-4.webp?v=20231108-2" type="image/webp" />
        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-4.png?v=20231108-2" type="image/png" /><img class="slots-coin-1 float-effect-1s" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-4.png?v=20231108-2" />
    </picture>
    <picture>
        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-5.webp?v=20231108-2" type="image/webp" />
        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-5.png?v=20231108-2" type="image/png" /><img class="slots-coin-2 float-effect-1s" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-5.png?v=20231108-2" />
    </picture>
    <picture>
        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-6.webp?v=20231108-2" type="image/webp" />
        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-6.png?v=20231108-2" type="image/png" /><img class="slots-coin-3 float-effect-2s-d" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/decorations/coins/coin-6.png?v=20231108-2" />
    </picture>
</div>
<div class="container slots-container">
    <div class="row">
        <div class="col-md-12">
            <div class="progressive-jackpot" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/jackpot/background.png?v=20231108-2);"> <span id="progressive_jackpot" data-progressive-jackpot-url="https://jp-api.nexuswlb.com"></span>
                <div class="jackpot-container"> <span class="jackpot-currency jackpot_currency"></span> <span class="counter-container" id="progressive_jackpot_counter"> <span data-digit="-"></span> <span data-digit="-"></span> <span data-digit="-"></span> <span data-digit="-"></span> <span data-digit="-"></span> <span data-digit="-"></span> <span data-digit="-"></span> <span data-digit="-"></span> <span data-digit="-"></span> <span data-digit="-"></span> <span data-digit="-"></span> </span> </div>
            </div>
            <div class="games-list-container">
                <div class="provider-outer-container">
                    <div class="provider-slide" id="slots_providers"> <i class="glyphicon glyphicon-chevron-left left_trigger"></i>
                        <main> <a href="/desktop/slots/pragmatic" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PP.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PP.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PP.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PP-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PP-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PP-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/microgaming" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/MICROGAMING.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/MICROGAMING.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/MICROGAMING.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/MICROGAMING-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/MICROGAMING-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/MICROGAMING-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/pgsoft" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PGSOFT.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PGSOFT.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PGSOFT.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PGSOFT-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PGSOFT-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PGSOFT-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/reel-kingdom" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/REELKINGDOM.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/REELKINGDOM.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/REELKINGDOM.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/REELKINGDOM-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/REELKINGDOM-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/REELKINGDOM-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/advantplay" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/ADVANTPLAY.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/ADVANTPLAY.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/ADVANTPLAY.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/ADVANTPLAY-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/ADVANTPLAY-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/ADVANTPLAY-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/no-limit-city" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/NOLIMITCITY.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/NOLIMITCITY.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/NOLIMITCITY.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/NOLIMITCITY-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/NOLIMITCITY-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/NOLIMITCITY-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/habanero" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/HABANERO.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/HABANERO.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/HABANERO.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/HABANERO-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/HABANERO-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/HABANERO-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/joker" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/JOKER.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/JOKER.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/JOKER.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/JOKER-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/JOKER-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/JOKER-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/jili" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/JILI.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/JILI.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/JILI.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/JILI-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/JILI-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/JILI-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/spinix" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SPINIX.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SPINIX.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SPINIX.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SPINIX-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SPINIX-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SPINIX-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/crowd-play" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/CROWDPLAY.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/CROWDPLAY.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/CROWDPLAY.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/CROWDPLAY-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/CROWDPLAY-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/CROWDPLAY-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/live22" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/LIVE22.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/LIVE22.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/LIVE22.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/LIVE22-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/LIVE22-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/LIVE22-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/playstar" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PLAYSTAR.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PLAYSTAR.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PLAYSTAR.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PLAYSTAR-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PLAYSTAR-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PLAYSTAR-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/vpower" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/VPOWER.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/VPOWER.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/VPOWER.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/VPOWER-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/VPOWER-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/VPOWER-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/worldmatch" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/WORLDMATCH.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/WORLDMATCH.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/WORLDMATCH.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/WORLDMATCH-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/WORLDMATCH-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/WORLDMATCH-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/fachai" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/FACHAI.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/FACHAI.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/FACHAI.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/FACHAI-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/FACHAI-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/FACHAI-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/slot88" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SLOT88.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SLOT88.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SLOT88.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SLOT88-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SLOT88-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SLOT88-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/ion-slot" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PGS.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PGS.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PGS.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PGS-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PGS-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PGS-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/amb-slot" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/AMB.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/AMB.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/AMB.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/AMB-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/AMB-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/AMB-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/mario-club" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/MARIOCLUB.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/MARIOCLUB.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/MARIOCLUB.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/MARIOCLUB-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/MARIOCLUB-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/MARIOCLUB-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/dragoonsoft" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/DRAGOONSOFT.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/DRAGOONSOFT.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/DRAGOONSOFT.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/DRAGOONSOFT-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/DRAGOONSOFT-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/DRAGOONSOFT-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/spade-gaming" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SPADEGAMING.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SPADEGAMING.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SPADEGAMING.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SPADEGAMING-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SPADEGAMING-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SPADEGAMING-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/fun-gaming" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/FUNGAMING.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/FUNGAMING.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/FUNGAMING.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/FUNGAMING-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/FUNGAMING-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/FUNGAMING-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/naga-games" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/NAGAGAMES.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/NAGAGAMES.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/NAGAGAMES.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/NAGAGAMES-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/NAGAGAMES-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/NAGAGAMES-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/jdb" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/JDB.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/JDB.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/JDB.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/JDB-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/JDB-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/JDB-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/cq9" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SBOCQ9.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SBOCQ9.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SBOCQ9.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SBOCQ9-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SBOCQ9-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SBOCQ9-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/ttg" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/TTG.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/TTG.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/TTG.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/TTG-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/TTG-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/TTG-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/netent" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/NETENT.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/NETENT.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/NETENT.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/NETENT-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/NETENT-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/NETENT-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/big-time-gaming" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/BIGTIMEGAMING.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/BIGTIMEGAMING.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/BIGTIMEGAMING.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/BIGTIMEGAMING-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/BIGTIMEGAMING-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/BIGTIMEGAMING-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/red-tiger" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/REDTIGER.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/REDTIGER.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/REDTIGER.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/REDTIGER-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/REDTIGER-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/REDTIGER-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/skywind" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SKYWIND.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SKYWIND.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SKYWIND.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SKYWIND-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SKYWIND-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SKYWIND-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/playtech" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PLAYTECH.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PLAYTECH.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PLAYTECH.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PLAYTECH-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PLAYTECH-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PLAYTECH-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/yggdrasil" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/YGGDRASIL.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/YGGDRASIL.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/YGGDRASIL.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/YGGDRASIL-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/YGGDRASIL-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/YGGDRASIL-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/playngo" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PLAYNGO.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PLAYNGO.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PLAYNGO.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PLAYNGO-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PLAYNGO-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/PLAYNGO-active.png?v=20231108-2" />
                                </picture>
                            </a> <a href="/desktop/slots/real-time-gaming" class="slide-item">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SBOREALTIMEGAMING.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SBOREALTIMEGAMING.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SBOREALTIMEGAMING.png?v=20231108-2" />
                                </picture>
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SBOREALTIMEGAMING-active.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SBOREALTIMEGAMING-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/slots/SBOREALTIMEGAMING-active.png?v=20231108-2" />
                                </picture>
                            </a> </main> <i class="glyphicon glyphicon-chevron-right right_trigger"></i>
                    </div>
                    <div class="vendor-name"> <?= $vendor ?> </div>
                    <div class="filter-section">
                        <div class="category-filter" id="filter_categories">
                            <div class="category-filter-link active" data-category=""> Semua permainan </div>
                        </div> <input type="text" id="filter_input" placeholder="Cari Permainan">
                    </div>
                </div>
                <div class="game-list" id="game_list" style="--star-on-icon: url(//nx-cdn.trgwl.com/Images/icons/star-on.svg?v=20231108-2); --star-off-icon: url(//nx-cdn.trgwl.com/Images/icons/star-off.svg?v=20231108-2);"></div>
            </div>
        </div>
    </div>
</div>