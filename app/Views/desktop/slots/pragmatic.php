<?= $this->extend('layouts/desktop/slot') ?>

<?= $this->section('content') ?>
<div data-container-background="slots" style="background-image: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/backgrounds/slots.jpg?v=20231108-2);">
    <?= view('components/slot-container', ['vendor' => 'Pragmatic Play']) ?>
</div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
<script>
    window.addEventListener('DOMContentLoaded', () => {
        initializeSlotGames({
            directoryPath: '//nx-cdn.trgwl.com/Images/providers/',
            provider: 'PP',
            onDemoLinkClicked: (game) => {
                alert('Anda akan bermain versi demo, ini berarti menang/kalah dalam permainan tidak akan mempengaruhi dana/saldo anda.');
    
                openPopup(`http://demogamesfree-asia.pragmaticplay.net/gs2c/openGame.do?gameSymbol=${game}&lang=en&cur=IDR&lobbyUrl=js://window.close()`, 'Slots');
            },
            translations: {
                playNow: 'MAIN',
                demo: 'COBA',
            }
        });
    });
</script>
<?= $this->endSection() ?>    