<?= $this->extend('layouts/mobile/history') ?>

<?= $this->section('content', ['h' => 1]) ?>
<div class="reporting-form-container">
    <div class="reporting-nav-bar"> <a href="/mobile/history/deposit" data-active="false"> DEPOSIT </a> <a href="/mobile/history/withdrawal" data-active="false"> PENARIKAN </a> <a href="/mobile/statement/consolidate" data-active="true"> RIWAYAT TARUHAN </a> </div>
    <form action="/mobile/statement/consolidate" method="post">
        <div class="reporting-control-group">
            <div class="form-group"> <label>Tipe</label> <select class="form-control" id="available_game_types" name="GameType">
                    <option value="Unknown"> Semua </option>
                    <option value="Arcade"> Arcade </option>
                    <option value="Casino"> Live Casino </option>
                    <option value="CrashGame"> Crash Game </option>
                    <option value="ESports"> E-Sports </option>
                    <option value="Lottery"> Togel </option>
                    <option value="Multi"> Poker </option>
                    <option value="Slots"> Slots </option>
                    <option value="Sports"> Olahraga </option>
                </select> </div>
            <div class="form-group"> <label>Provider</label> <select class="form-control" id="available_games" name="Game" disabled="">
                    <option value="Unknown">Semua</option>
                </select> </div> <label>Date Range</label> <input type="text" class="form-control" id="date_range_picker" data-picker="date-range" data-separator=" - "> <input type="hidden" name="StartingDate" id="starting_date" value="2023-12-09"> <input type="hidden" name="EndingDate" id="ending_date" value="2023-12-09">
        </div>
        <div class="standard-button-group"> <button type="submit" class="standard-secondary-button"> Search </button> </div>
        <div class="reporting-scroll-container">
            <table class="table grid_table">
                <thead>
                    <tr>
                        <th scope="col"> </th>
                        <th scope="col"> Tanggal(GMT+7) </th>
                        <th scope="col"> Nomor </th>
                        <th scope="col"> Total Taruhan </th>
                        <th scope="col"> Total Turnover </th>
                        <th scope="col"> Total Win/Lose </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
<script src='/js/deposit/consol.js' defer></script>


<script>
    window.addEventListener('DOMContentLoaded', () => {
        initialConsolidateStatement({
            availabelGames:{"Unknown":{},"Arcade":{"SPRIBE":"Spribe","ARCADIA":"Arcadia","G8TANGKAS":"MM Tangkas"},"Casino":{"TRG":"ION Casino","PPLIVECASINO":"PP Casino","PPBLACKJACK":"PP Black Jack","MGLIVECASINO":"MG Live","EVOGAMING":"Evo Gaming","SBOSEXYBACCARAT":"Sexy Baccarat","PRETTYGAMING":"Pretty Gaming","AG":"Asia Gaming","ALLBET":"AllBet","PGSLIVE":"PGS Live","SAGAMING":"SA Gaming","EBET":"Ebet","DREAMGAMING":"Dream Gaming","SBOCASINO":"568Win Casino","HKB":"HKB","SV388":"SV388"},"CrashGame":{},"ESports":{"IMONE":"IM Esports","PINNACLEESPORTS":"Pinnacle E-Sports","TFGAMING":"TF Gaming"},"Lottery":{"BALAK4D":"Nex4D","BALAK4DOPERATOR":"Nex4D Operator","BALAK4DGLOBAL":"Nex4D Global"},"Multi":{"G8POKER":"Balak Play","ONEPOKER":"9Gaming"},"Slots":{"NINEGAMINGSLOTS":"9Gaming Slots","PP":"Pragmatic Play","MICROGAMING":"MicroGaming","PGSOFT":"PG Slots","REELKINGDOM":"Reel Kingdom by Pragmatic","HABANERO":"Habanero","NOLIMITCITY":"No Limit City","ADVANTPLAY":"AdvantPlay","JOKER":"Joker","JILI":"Jili","SPINIX":"Spinix","CROWDPLAY":"Crowd Play","LIVE22":"Live22","PLAYSTAR":"Playstar","VPOWER":"VPower","WORLDMATCH":"Worldmatch","FACHAI":"Fachai","SLOT88":"Slot88","PGS":"ION Slot","AMB":"AMB Slot","MARIOCLUB":"Mario Club","DRAGOONSOFT":"Dragoonsoft","SPADEGAMING":"Spade Gaming","FUNGAMING":"Fun Gaming","NAGAGAMES":"Naga Games","JDB":"JDB","SBOCQ9":"CQ9","TTG":"Top Trend Gaming","NETENT":"Netent","BIGTIMEGAMING":"Big Time Gaming","REDTIGER":"Red Tiger","SKYWIND":"Skywind","PLAYTECH":"Playtech","YGGDRASIL":"Yggdrasil","PLAYNGO":"Play'n Go","SBOREALTIMEGAMING":"Real Time Gaming"},"Sports":{"SBO":"SBO Sportsbook","IBCSPORTS":"Saba Sportsbook","OPUS":"Opus","WBET":"WBet","IMSPORTSBOOK":"IM Sportsbook","PINNACLE":"Pinnacle","CMD":"CMD","SBOVIRTUALGAMES":"SBO Virtual Sports","PPVIRTUALGAMES":"PP Virtual Sports"}},
            selectedGameType: 'Unknown',
            selectedGame: 'Unknown',
            unknownGameType: 'Unknown',
            translations: {
                all: 'Semua',
            }
        });
    });
</script>
<?= $this->endSection() ?>