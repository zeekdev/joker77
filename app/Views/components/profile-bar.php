<div class="standard-profile-bar">
    <section class="user-field">
        <picture>
            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/avatar.webp?v=20231129-1" type="image/webp">
            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/avatar.png?v=20231129-1" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/avatar.png?v=20231129-1">
        </picture>
        <div>
            <div class="username-field"> <span class="username"><?= session()->get('username') ?></span> Selamat Datang! </div>
            <div class="last-login-field"> Terakhir Masuk: <?= session()->get('created') ?> </div>
        </div>
    </section>
    <section class="balance-field"> Saldo Wallet <div class="currency"> IDR <span class="balance total_balance">0.00</span>
            <div class="locked-balance locked_balance_container" hidden=""> <i data-icon="locked-balance" class="glyphicon glyphicon-lock"></i> <span class="total_locked_balance"></span> </div>
        </div>
    </section>
</div>