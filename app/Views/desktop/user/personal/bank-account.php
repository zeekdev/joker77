<?= $this->extend('layouts/desktop/personal') ?>

<?= $this->section('content') ?>
<div class="standard-form-content">
    <div class="tab-menu-container" data-style="vertical"> <a href="/desktop/deposit"> <i data-icon="deposit" style="background-image: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/tabs/deposit.svg?v=20231205-1);"></i> Deposit </a> <a href="/desktop/withdrawal"> <i data-icon="withdrawal" style="background-image: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/tabs/withdrawal.svg?v=20231205-1);"></i> Penarikan </a> </div>
    <?php if(session()->has('msg')): ?>
    <div class="alert alert-success"><?= session()->get('msg') ?></div>
    <?php endif; ?>
    <div class="standard-form-title"> Informasi Detail Perbankan </div>
    <form action="/Profile/BankAccount" id="form0" method="post" novalidate="novalidate"><input name="__RequestVerificationToken" type="hidden" value="sqUS94DMS3ZAUJIfgBwNGY85YBkX6yuxoUi1s4bkhOv09PERn9_5HQMwI1-HPewOdarbqJUJy9L61yxnqsmvL1sczI-KhWfKkVrmfV9pTKQ1">
        <div class="standard-inline-form-group"> <label for="PaymentType">Tipe Pembayaran</label>
            <div data-section="asterisk">*</div>
            <div data-section="input"> <select class="form-control" data-val="true" data-val-required="The PaymentType field is required." id="payment_method_select" name="PaymentType">
                    <option selected="selected" value="BANK">Bank</option>
                </select> <span class="standard-required-message">Mohon pilih jenis pembayaran.</span> </div>
        </div>
        <div class="standard-inline-form-group"> <label for="Bank">Akun</label>
            <div data-section="asterisk">*</div>
            <div data-section="input"> <select class="form-control" data-val="true" data-val-required="The Bank field is required." id="Bank" name="Bank">
                    <option value="">-- Pilih Akun --</option>
                    <option value="BSI">BANK BSI</option>
                    <option value="BCA">BCA</option>
                    <option value="BNI">BNI</option>
                    <option value="BRI">BRI</option>
                    <option value="CIMB">CIMB</option>
                    <option value="DANA">DANA</option>
                    <option value="GOPAY">GOPAY</option>
                    <option value="MANDIRI">MANDIRI</option>
                    <option value="MAYBANK">MAYBANK</option>
                    <option value="OVO">OVO</option>
                </select> <span class="standard-required-message">Silahkan pilih bank!</span> </div>
        </div>
        <div class="standard-inline-form-group"> <label for="Branch">Cabang</label>
            <div data-section="input"> <input class="form-control" id="Branch" name="Branch" placeholder="Cabang bank anda sesuai dengan buku tabungan" type="text" value=""> </div>
        </div>
        <div class="standard-inline-form-group"> <label for="AccountName">Nama Rekening</label>
            <div data-section="asterisk">*</div>
            <div data-section="input"> <input class="form-control" data-val="true" data-val-regex="The field AccountName must match the regular expression '^[0-9a-zA-Z ]*$'." data-val-regex-pattern="^[0-9a-zA-Z ]*$" data-val-required="The AccountName field is required." id="AccountName" name="AccountName" placeholder="Nama lengkap anda sesuai dengan buku tabungan" type="text" value="<?= $banks[0]['nama_rekening'] ?>"> <span class="standard-required-message">Nama rekening harus diisi.</span> </div>
        </div>
        <div class="standard-inline-form-group"> <label for="AccountNo">Nomor Rekening</label>
            <div data-section="asterisk">*</div>
            <div data-section="input"> <input maxlength="20" autocomplete="off" class="form-control" data-val="true" data-val-regex="The field AccountNo must match the regular expression '^[0-9]+$'." data-val-regex-pattern="^[0-9]+$" data-val-required="The AccountNo field is required." id="AccountNo" name="AccountNo" placeholder="Nomor rekening anda" type="text" value=""> <span class="standard-required-message">Rekening bank harus diisi. (Hanya nomor)</span> </div>
        </div>
        <div class="standard-inline-form-group"> <label for="VerificationCode">Kode Verifikasi</label>
            <div data-section="asterisk">*</div>
            <div data-section="input" class="captcha-input"> <input autocomplete="off" class="form-control" data-val="true" data-val-required="The VerificationCode field is required." id="VerificationCode" name="VerificationCode" placeholder="Validasi" type="text" value=""> <span class="standard-required-message">Silakan masukkan captcha!</span>
                <div class="captcha-container"> <i class="glyphicon glyphicon-refresh refresh-captcha-button" id="refresh_captcha_button"></i> <img id="captcha_image" src="/captcha"> </div>
            </div>
        </div>
        <div class="standard-button-group"> <input type="submit" class="btn btn-primary" value="Tambah Akun"> </div>
    </form>
    <table class="table">
        <thead>
            <tr>
                <th scope="col"> Akun </th>
                <th scope="col"> Nama Akun </th>
                <th scope="col"> Nomor Akun </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($banks as $row): ?>
            <tr>
                <td><?= $row['bank'] ?></td>
                <td><?= $row['nama_rekening'] ?></td>
                <td>******<?= substr($row['norek'], 5) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
<script src="/js/personal/bank-account.js"></script>
<?= $this->endSection() ?>