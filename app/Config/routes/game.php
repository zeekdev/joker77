<?php

#--------------------------------------------------------------------
# MENU SLOT
#--------------------------------------------------------------------

// Desktop
$routes->get('/desktop/slots/games/(:any)', 'Slot::list/$1');
$routes->get('/desktop/slots/(:any)', 'Slot::index/$1');

// Mobile
$routes->view('/mobile/hot-games', 'mobile/hot-games');
$routes->get('/mobile/slots/games/(:any)', 'Slot::list/$1');
$routes->view('/mobile/slots', 'mobile/slots/index');
$routes->get('/mobile/slots/(:any)', 'Slot::mobile/$1');

#--------------------------------------------------------------------
# MENU CASINO
#--------------------------------------------------------------------

// Desktop
$routes->get('/desktop/casino/games/(:any)', 'Casino::list/$1');
$routes->get('/desktop/casino/(:any)', 'Casino::index/$1');

// Mobile
$routes->get('/mobile/casino/games/(:any)', 'Casino::list/$1');
$routes->view('/mobile/casino', 'mobile/casino/index');
$routes->get('/mobile/casino/(:any)', 'Casino::mobile/$1');

#--------------------------------------------------------------------
# MENU CRASH GAME
#--------------------------------------------------------------------

// Desktop
$routes->get('/desktop/crash-game/games/(:any)', 'CrashGame::list/$1');
$routes->get('/desktop/crash-game/(:any)', 'CrashGame::index/$1');

// Mobile
$routes->get('/mobile/crash-game/games/(:any)', 'CrashGame::list/$1');
$routes->view('/mobile/crash-game', 'mobile/crash/index');
$routes->get('/mobile/crash-game/(:any)', 'CrashGame::mobile/$1');

#--------------------------------------------------------------------
# MENU ARCADE
#--------------------------------------------------------------------

// Desktop
$routes->get('/desktop/arcade/games/(:any)', 'Arcade::list/$1');
$routes->get('/desktop/arcade/(:any)', 'Arcade::index/$1');

// Mobile
$routes->get('/mobile/arcade/games/(:any)', 'Arcade::list/$1');
$routes->view('/mobile/arcade', 'mobile/arcade/index');
$routes->get('/mobile/arcade/(:any)', 'Arcade::mobile/$1');

#--------------------------------------------------------------------
# MENU POKER
#--------------------------------------------------------------------

// Mobile
$routes->view('/mobile/poker', 'mobile/poker/index');
$routes->view('/mobile/poker/balak-play', 'mobile/poker/balak-play');
$routes->view('/mobile/poker/9gaming', 'mobile/poker/9gaming');

#--------------------------------------------------------------------
# MENU TOGEL
#--------------------------------------------------------------------

// Mobile
$routes->view('/mobile/others', 'mobile/togel/index');
$routes->view('/mobile/others/nex-4d', 'mobile/togel/nex4d');