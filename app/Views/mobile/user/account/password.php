<?= $this->extend('layouts/mobile/profile') ?>

<?= $this->section('content') ?>
<div class="standard-form-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="tab-menu-background-container">
                    <div class="tab-menu-container" data-style="vertical"> <a href="/mobile/account-summary" data-active="false"> <i data-icon="profile" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/profile.svg?v=20231205-1);--active-image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/profile-active.svg?v=20231205-1);"></i> Akun Saya </a> <a href="/mobile/password" data-active="true"> <i data-icon="password" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/password.svg?v=20231205-1);--active-image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/password-active.svg?v=20231205-1);"></i> Ubah Kata Sandi </a></div>
                </div>
                <div class="standard-form-title"> UBAH KATA SANDI </div>
                <div class="standard-form-note"> <span>Catatan:</span><br>*Password harus terdiri dari 8-20 karakter.<br>*Password harus mengandung setidaknya satu huruf besar, satu huruf kecil, dan satu angka. <br>*Password tidak boleh mengandung username. </div>
                <form action="/Profile/Password" method="post" novalidate="novalidate"><input name="__RequestVerificationToken" type="hidden" value="CwvpLy5e6v_h-jafansXfLieDh0bLtZevfulTC8BuWHokodqeFWNXWWb1KsuW3RllWRVUCynkse2RGh8DLg168qdjNWFj8MBCG9F12ODgOQ1">
                    <div class="form-group"> <label for="OldPassword">Kata Sandi Saat Ini</label> <input maxlength="20" class="form-control" data-val="true" data-val-required="The OldPassword field is required." id="OldPassword" name="OldPassword" placeholder="Kata Sandi Saat Ini" type="password"> <span class="standard-required-message">Kata sandi harus diisi.</span> </div>
                    <div class="form-group standard-password-field"> <label for="NewPassword">Kata Sandi Baru</label> <input maxlength="20" class="form-control" data-val="true" data-val-regex="The field NewPassword must match the regular expression '^(?=.{8,20}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9]).*$'." data-val-regex-pattern="^(?=.{8,20}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9]).*$" data-val-required="The NewPassword field is required." id="new_password_input" name="NewPassword" placeholder="Kata Sandi Baru" type="password"> <span class="standard-required-message">Password harus terdiri dari 8-20 karakter, dan harus mengandung setidaknya satu huruf besar, satu huruf kecil, dan satu angka.</span> <i class="glyphicon glyphicon-eye-open" id="new_password_input_trigger"></i> </div>
                    <div class="form-group standard-password-field"> <label for="ConfirmPassword">Ulangi Kata Sandi</label> <input maxlength="20" class="form-control" data-val="true" data-val-equalto="'ConfirmPassword' and 'NewPassword' do not match." data-val-equalto-other="*.NewPassword" data-val-required="The ConfirmPassword field is required." id="confirm_password_input" name="ConfirmPassword" placeholder="Ulangi Kata Sandi" type="password"> <span class="standard-required-message">* Kata sandi tidak cocok.</span> <i class="glyphicon glyphicon-eye-open" id="confirm_password_input_trigger"></i> </div>
                    <div class="form-group"> <label for="VerificationCode">Kode Verifikasi</label>
                        <div data-section="input" class="captcha-input"> <input autocomplete="off" class="form-control" data-val="true" data-val-required="The VerificationCode field is required." id="VerificationCode" name="VerificationCode" placeholder="Validasi" type="text" value=""> <span class="standard-required-message">Captcha salah.</span>
                            <div class="captcha-container"> <img id="captcha_image" src="/captcha"> <i class="glyphicon glyphicon-refresh refresh-captcha-button" id="refresh_captcha_button"></i> </div>
                        </div>
                    </div>
                    <div class="standard-button-group"> <input type="submit" class="standard-secondary-button" value="UBAH KATA SANDI"> </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>