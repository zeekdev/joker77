<?= $this->extend('layouts/mobile/slot') ?>

<?= $this->section('content') ?>
<div id="page_carousel" class="carousel slide page-carousel" data-ride="carousel" data-interval="5000">
    <ol class="carousel-indicators">
        <li class="active" data-target="#page_carousel" data-slide-to="0"></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/banners/slots/banner.jpg?v=20231108-2" /> </div>
    </div>
</div>

<?= $this->include('components/mobile/menu-outer') ?>

<div class="game-list">
    <ul>
        <li> <a href="/mobile/slots/pragmatic">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pp.webp?v=20231108-2" type="image/webp" />
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pp.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pp.png?v=20231108-2" />
                </picture>
            </a> Pragmatic Play </li>
        <li> <a href="/mobile/slots/microgaming">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/microgaming.webp?v=20231108-2" type="image/webp" />
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/microgaming.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/microgaming.png?v=20231108-2" />
                </picture>
            </a> MicroGaming </li>
        <li> <a href="/mobile/slots/pgsoft">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pgsoft.webp?v=20231108-2" type="image/webp" />
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pgsoft.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pgsoft.png?v=20231108-2" />
                </picture>
            </a> PG Slots </li>
        <li> <a href="/mobile/slots/reel-kingdom">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/reelkingdom.webp?v=20231108-2" type="image/webp" />
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/reelkingdom.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/reelkingdom.png?v=20231108-2" />
                </picture>
            </a> Reel Kingdom by Pragmatic </li>
        <li> <a href="/mobile/slots/advantplay">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/advantplay.webp?v=20231108-2" type="image/webp" />
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/advantplay.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/advantplay.png?v=20231108-2" />
                </picture>
            </a> AdvantPlay </li>
        <li> <a href="/mobile/slots/no-limit-city">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/nolimitcity.webp?v=20231108-2" type="image/webp" />
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/nolimitcity.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/nolimitcity.png?v=20231108-2" />
                </picture>
            </a> No Limit City </li>
        <li> <a href="/mobile/slots/habanero">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/habanero.webp?v=20231108-2" type="image/webp" />
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/habanero.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/habanero.png?v=20231108-2" />
                </picture>
            </a> Habanero </li>
        <li> <a href="/mobile/slots/joker">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/joker.webp?v=20231108-2" type="image/webp" />
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/joker.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/joker.png?v=20231108-2" />
                </picture>
            </a> Joker </li>
        <li> <a href="/mobile/slots/jili">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/jili.webp?v=20231108-2" type="image/webp" />
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/jili.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/jili.png?v=20231108-2" />
                </picture>
            </a> Jili </li>
        <li> <a href="/mobile/slots/spinix">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/spinix.webp?v=20231108-2" type="image/webp" />
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/spinix.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/spinix.png?v=20231108-2" />
                </picture>
            </a> Spinix </li>
        <li> <a href="/mobile/slots/crowd-play">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/crowdplay.webp?v=20231108-2" type="image/webp" />
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/crowdplay.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/crowdplay.png?v=20231108-2" />
                </picture>
            </a> Crowd Play </li>
        <li> <a href="/mobile/slots/live22">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/live22.webp?v=20231108-2" type="image/webp" />
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/live22.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/live22.png?v=20231108-2" />
                </picture>
            </a> Live22 </li>
        <li> <a href="/mobile/slots/playstar">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/playstar.webp?v=20231108-2" type="image/webp" />
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/playstar.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/playstar.png?v=20231108-2" />
                </picture>
            </a> Playstar </li>
        <li> <a href="/mobile/slots/vpower">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/vpower.webp?v=20231108-2" type="image/webp" />
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/vpower.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/vpower.png?v=20231108-2" />
                </picture>
            </a> VPower </li>
        
    </ul>
</div>

<?= $this->include('components/mobile/popular') ?>

<?= $this->endSection() ?>