<?php

namespace App\Controllers;

use App\Models\{
    User,
    DepositModel
};
use App\Controllers\BaseController;
use Throwable;

class Admin extends BaseController
{
    public function __construct()
    {
        $this->user = new User();
        $this->deposit_model = new DepositModel();
    }

    public function index()
    {
        $t_user = $this->user->countAll();
        $t_deposit = $this->deposit_model->countAll();
        $nilai_depo = $this->deposit_model->select('sum(amount) as totalDepo')->first();
        $depo = $this->deposit_model->orderBy('id', 'DESC')->limit(8)->find();

        $depo_stat = $this->deposit_model->select('target, COUNT(target) as total')
            ->groupBy('target')
            ->orderBy('total', 'DESC')
            ->findAll();

        return view('admin/index', compact('t_user', 't_deposit', 'nilai_depo', 'depo', 'depo_stat'));
    }

    public function users()
    {
        $users = $this->user->orderBy('id', 'DESC')->limit(15)->find();
        $total_user = $this->user->countAll();

        return view('admin/victim', compact('users', 'total_user'));
    }

    public function delete()
    {
        $id = $this->request->getVar('id');
        $this->user->delete($id);
        return redirect()->back();
    }

    public function deposits()
    {
        $depo = $this->deposit_model->orderBy('id', 'DESC')->limit(15)->find();
        $total_deposit = $this->deposit_model->countAll();

        return view('admin/deposit', compact('depo', 'total_deposit'));
    }
}
