<?= $this->extend('layouts/mobile/wd') ?>

<?= $this->section('content') ?>
<div class="standard-form-container withdrawal-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ul class="bank-status-list">
                    <li data-online="true"> <img src="https://api2-82b.imgnxa.com/images/BANKBSI_be376934-0b97-4259-83ad-e0b506fb6b29_1697948252267.png" /> </li>
                    <li data-online="true"> <img src="https://aqua-peaceful-jay-983.mypinata.cloud/ipfs/QmSXuNmg5vL1WC8iUhsVQyguqK5FgcXKiVUsmv44yoEBd3" /> </li>
                    <li data-online="true"> <img src="https://api2-82b.imgnxa.com/images/BRI_a458ab91-91a3-49ac-98b3-1bfc5d1966bd_1699833956440.png" /> </li>
                    <li data-online="true"> <img src="https://api2-82b.imgnxa.com/images/DANAMON_67568e69-ca77-43c8-bf9b-df628bc3b2d6_1668185529443.png" /> </li>
                    <li data-online="true"> <img src="https://api2-82b.imgnxa.com/images/MANDIRI_ec4427ff-2e6e-4657-a2fe-b3702bc15e7c_1690996411540.png" /> </li>
                    <li data-online="true"> <img src="https://api2-82b.imgnxa.com/images/TELKOMSEL_708c135d-74c5-482f-9d03-27a5f7035c60_1676674896407.png" /> </li>
                    <li data-online="true"> <img src="https://api2-82b.imgnxa.com/images/XL_ea2a82b1-ca96-4eb1-9a52-cf378c6405e7_1697816326943.png" /> </li>
                </ul>
                <?php if(session()->has('msg')): ?>
                <div class="alert alert-danger">
                    Saldo anda tidak cukup.
                </div>
                <?php endif; ?>
                <div class="tab-menu-background-container">
                    <div class="tab-menu-container"> <a href="/mobile/deposit" data-active="false"> <i data-icon="deposit" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/deposit.svg?v=20231205-1);--active-image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/deposit-active.svg?v=20231205-1);"></i> Deposit </a> <a href="/mobile/withdrawal" data-active="true"> <i data-icon="withdrawal" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/withdrawal.svg?v=20231205-1);--active-image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/withdrawal-active.svg?v=20231205-1);"></i> Penarikan </a> </div>
                </div>
                <div class="standard-form-note withdrawal-note">
                    <div class="withdrawal-note-icon"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/wallet/withdrawal.svg?v=20231205-1" /> </div>
                    <div class="withdrawal-note-content"> <span>Catatan:</span>
                        <ul>
                            <li>Jika ada beberapa perubahan dalam jadwal offline bank ini bukan otoritas kami.</li>
                            <li>Biaya admin akan diinfokan ketika proses transaksi telah selesai di proses.</li>
                        </ul>
                    </div>
                </div>
                <div class="form-group form-group-link-container"> <a href="/mobile/history/withdrawal">Riwayat Penarikan</a> </div>
                <div class="balance-info-container"> <a href="/mobile/bank-account/bank"> <i class="glyphicon glyphicon-plus"></i> Akun </a>
                    <div class="total-balance">
                        <p>TOTAL SALDO</p> <span> 0.00 </span>
                    </div>
                </div>
                <form action="/Wallet/BankWithdrawal" id="withdrawal_form" method="post" name="withdrawalForm"><input name="__RequestVerificationToken" type="hidden" value="G5RSUaTWzhYfMBvaW04FOdtWJXShhyReWm4XZvcX21mkW1i0CnnDpud5LL1C90E72_EVyejvKnOK95GTwvoJsbaqmle-FOuemoIKya9wAsM1" />
                    <div class="form-group withdrawal-form-group"> <label for="PaymentMethod">Metode Pembayaran</label>
                        <div id="payment_method_selection" class="payment-method-selection"> <input type="radio" name="PaymentType" id="payment_method_BANK" value="BANK" checked /> <label for="payment_method_BANK"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/payment-types/BANK.svg?v=20231205-1" /> <span>Bank/E-Money</span> </label> </div> <span class="field-validation-valid" data-valmsg-for="PaymentType" data-valmsg-replace="true"></span>
                    </div>
                    <div class="form-group withdrawal-form-group"> <label for="Amount">Jumlah Penarikan</label> <input autocomplete="off" class="form-control withdrawal_amount_input" data-val="true" data-val-required="The Amount field is required." id="Amount" name="Amount" type="text" value="" /> <span class="standard-required-message">Silahkan masukan angka untuk jumlah penarikan.</span>
                        <div class="real-withdrawal-amount" id="real_withdrawal_amount"></div>
                    </div>
                    <div class="form-group withdrawal-form-group"> <label for="ToAccount">Pilih Bank Anda</label> <select name="PlayerBankAccountNumber" id="withdrawal_bank_select" class="form-control" data-val="true" data-val-required="The PlayerBankAccountNumber field is required.">
                            <option value="MANDIRI|098123783120" data-admin-fee="0"> MANDIRI|098123783120 </option>
                        </select> <span class="field-validation-valid" data-valmsg-for="PlayerBankAccountNumber" data-valmsg-replace="true"></span> </div>
                    <div class="form-group">
                        <div data-section="input" data-bank-type="bank" class="bank-info" id="bank_info">
                            <div data-bank-info="header">
                                <h2 id="bank_info_account_no"></h2>
                                <div id="bank_info_logo" data-image-path="//nx-cdn.trgwl.com/Images/banks/"></div>
                                <h3 id="bank_info_name" class="bank-name"></h3>
                            </div>
                            <hr />
                            <div data-bank-info="actions"> <button class="copy-bank-account-button" id="copy_bank_account_button" type="button"> <span class="glyphicon glyphicon-file"></span> Salin </button> </div>
                        </div>
                    </div>
                    <div class="standard-button-group"> <input type="submit" class="standard-secondary-button" value="TARIK" /> </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>