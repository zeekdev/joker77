<?= $this->extend('layouts/mobile/slot') ?>

<?= $this->section('content') ?>
<?= $this->include('components/mobile/menu-outer') ?>

<div class="game-list">
    <ul>
        <li> <a href="/mobile/others/nex-4d">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/balak4d.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/balak4d.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/balak4d.png?v=20231115">
                </picture>
            </a> Nex4D </li>
    </ul>
</div>

<?= $this->include('components/mobile/popular') ?>

<?= $this->endSection() ?>