<?= $this->extend('layouts/mobile/slot') ?>

<?= $this->section('content') ?>
<div id="page_carousel" class="carousel slide page-carousel" data-ride="carousel" data-interval="5000">
    <ol class="carousel-indicators">
        <li class="active" data-target="#page_carousel" data-slide-to="0"></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/banners/arcade/banner.jpg?v=20231123"> </div>
    </div>
</div>
<?= $this->include('components/mobile/menu-outer') ?>

<div class="game-list">
    <ul>
        <li> <a href="/mobile/arcade/microgaming">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/microgamingfishing.webp?v=20231123" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/microgamingfishing.png?v=20231123" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/microgamingfishing.png?v=20231123">
                </picture>
            </a> MicroGaming </li>
        <li> <a href="/mobile/arcade/spinix">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/spinixfishing.webp?v=20231123" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/spinixfishing.png?v=20231123" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/spinixfishing.png?v=20231123">
                </picture>
            </a> Spinix </li>
        <li> <a href="/mobile/arcade/spribe">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/spribe.webp?v=20231123" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/spribe.png?v=20231123" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/spribe.png?v=20231123">
                </picture>
            </a> Spribe </li>
        <li> <a href="/mobile/arcade/joker">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/jokerfishing.webp?v=20231123" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/jokerfishing.png?v=20231123" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/jokerfishing.png?v=20231123">
                </picture>
            </a> Joker </li>
        <li> <a href="/mobile/arcade/fachai">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/fachaifishing.webp?v=20231123" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/fachaifishing.png?v=20231123" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/fachaifishing.png?v=20231123">
                </picture>
            </a> Fachai </li>
        <li> <a href="/mobile/arcade/jili">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/jilifishing.webp?v=20231123" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/jilifishing.png?v=20231123" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/jilifishing.png?v=20231123">
                </picture>
            </a> Jili </li>
        <li> <a href="/mobile/arcade/amb-slot">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/ambfishing.webp?v=20231123" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/ambfishing.png?v=20231123" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/ambfishing.png?v=20231123">
                </picture>
            </a> AMB Slot </li>
        <li> <a href="/mobile/arcade/crowd-play">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/crowdplayfishing.webp?v=20231123" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/crowdplayfishing.png?v=20231123" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/crowdplayfishing.png?v=20231123">
                </picture>
            </a> Crowd Play </li>
    </ul>
</div>

<?= $this->include('components/mobile/popular') ?>

<?= $this->endSection() ?>