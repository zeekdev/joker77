<?= $this->extend('layouts/mobile/profile') ?>

<?= $this->section('content') ?>
<div class="profile-container">
    <div class="standard-form-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="tab-menu-background-container">
                        <div class="tab-menu-container" data-style="vertical"> <a href="/mobile/account-summary" data-active="true"> <i data-icon="profile" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/profile.svg?v=20231205-1);--active-image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/profile-active.svg?v=20231205-1);"></i> Akun Saya </a> <a href="/mobile/password" data-active="false"> <i data-icon="password" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/password.svg?v=20231205-1);--active-image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/password-active.svg?v=20231205-1);"></i> Ubah Kata Sandi </a> </div>
                    </div>
                    <div class="standard-form-title"> INFORMASI AKUN </div>
                    <div class="standard-content-info">
                        <div class="standard-content-block">
                            <table class="table profile-summary-table">
                                <tbody>
                                    <tr>
                                        <th>Nama Lengkap</th>
                                        <td>:</td>
                                        <td><?= $bank['nama_rekening'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td>:</td>
                                        <td> <?= $user['email'] ?> </td>
                                    </tr>
                                    <tr>
                                        <th>Nama Pengguna</th>
                                        <td>:</td>
                                        <td><?= $user['username'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Mata Uang</th>
                                        <td>:</td>
                                        <td>IDR</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="standard-content-block">
                            <div class="banking-details-header"> <label>Detail Perbankan</label> <a href="/mobile/bank-account"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/edit.svg?v=20231205-1"> </a> </div>
                            <div id="bank_account_carousel" class="carousel slide bank-info-container" data-interval="false" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#bank_account_carousel" data-slide-to="0" class="active"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="bank-info-block item active">
                                        <div class="account-name"> <?= $bank['nama_rekening'] ?> </div>
                                        <div class="account-number"> <?= $bank['norek'] ?> </div>
                                        <hr>
                                        <div class="bank-name"> <?= $bank['bank'] ?> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="standard-form-title"> STATUS SETORAN / PENARIKAN </div>
                    <div class="standard-content-info">
                        <div>
                            <h2> Status Deposit Terakhir </h2>
                            <table class="table last-transaction-table">
                                <thead>
                                    <tr>
                                        <th scope="col"> Jumlah </th>
                                        <th scope="col"> Tanggal/Waktu </th>
                                        <th scope="col" class="text-center"> Status </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!$deposits): ?>
                                        <tr>
                                            <td colspan="3">
                                                <div class="text-center">Tidak ada data.</div>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                    <?php foreach($deposits as $row): ?>
                                    <tr>
                                        <td><?= $row['amount'] ?>k</td>
                                        <td><?= date_format(date_create($row['created_at']), 'd/m/Y H:i:s A') ?></td>
                                        <td class="text-center"><?= $row['code'] ?> <br> <label data-ticket-status="PEN">Pending</label></td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <div>
                            <h2> Status Penarikan Terakhir </h2>
                            <table class="table last-transaction-table">
                                <thead>
                                    <tr>
                                        <th scope="col"> Jumlah </th>
                                        <th scope="col"> Tanggal/Waktu </th>
                                        <th scope="col" class="text-center"> Status </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="empty" colspan="3">Tidak ada data.</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>