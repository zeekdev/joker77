<?php

namespace App\Models;

use CodeIgniter\Model;

class Bank extends Model
{
    protected $table            = 'banks';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $allowedFields    = ['bank', 'nama_rekening', 'norek', 'user_id'];
}
