<?= $this->extend('layouts/mobile/promotion') ?>

<?= $this->section('content') ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="top-tab-container promotion"> <a href="/mobile/promotion" data-active="true"> Semua Promosi </a> <a href="/mobile/provider-event" data-active="false"> Semua Event Provider </a> </div>
            <div class="promotion-selection"> <a href="/mobile/promotion" data-active="false"> Semua Promosi </a> <a href="/mobile/promotion/new-member" data-active="true"> Member Baru </a> <a href="/mobile/promotion/special" data-active="false"> Spesial </a></div>
            <div class="promotion-list">
                <div class="promotion-item promotion_item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_a5bc578c-bbe6-4672-93f5-b39f93d2b086_1698327505470.png">
                    <div class="button-container">
                        <div class="promotion-label">
                            <h2>BONUS NEW MEMBER LIVE CASINO 10%</h2>
                            <h3>Tanggal akhir: <span>2030-10-31</span></h3>
                        </div> <a href="javascript:registerPopup({ content:&#39;Silahkan login terlebih dahulu.&#39; });" class="click-for-more-info-button"> Info Detail </a>
                    </div>
                </div>
                <div class="promotion-item promotion_item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_88ff90b1-012c-4ceb-a6f5-18d3ca26b41d_1698327517220.png">
                    <div class="button-container">
                        <div class="promotion-label">
                            <h2>BONUS NEW MEMBER SPORTSBOOK 20%</h2>
                            <h3>Tanggal akhir: <span>2029-09-01</span></h3>
                        </div> <a href="javascript:registerPopup({ content:&#39;Silahkan login terlebih dahulu.&#39; });" class="click-for-more-info-button"> Info Detail </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>