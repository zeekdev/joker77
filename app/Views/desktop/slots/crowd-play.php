<?= $this->extend('layouts/desktop/slot') ?>

<?= $this->section('content') ?>
<div data-container-background="slots" style="background-image: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/backgrounds/slots.jpg?v=20231108-2);">
    <?= view('components/slot-container', ['vendor' => 'CROWD PLAY']) ?>
</div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
<script>
    window.addEventListener('DOMContentLoaded', () => {
        initializeSlotGames({
            directoryPath: '//nx-cdn.trgwl.com/Images/providers/',
            provider: 'CROWDPLAY',
            translations: {
                playNow: 'MAIN',
                demo: 'COBA'
            }
        });
    });
</script>
<?= $this->endSection() ?>    