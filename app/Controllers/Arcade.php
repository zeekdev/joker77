<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Arcade extends BaseController
{
    public function index($provider)
    {
        return view('desktop/arcade/'.$provider);
    }

    public function mobile($provider)
    {
        return view('mobile/arcade/'.$provider);
    }

    public function list($provider)
    {
        $json = APPPATH . 'Controllers/json/arcade/'.$provider.'.json';
        
        if($this->isAuth()) {
            $json = APPPATH . 'Controllers/json/arcade/auth/'.$provider.'.json';
        }

        $data = file_get_contents($json);
        return $this->response->setJSON($data);
    }
}
