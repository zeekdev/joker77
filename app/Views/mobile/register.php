<?= $this->extend('layouts/mobile/auth') ?>

<?= $this->section('content') ?>
<div class="standard-form-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <form action="/Register/MobileRegisterSubmit" enctype="multipart/form-data" method="post"><input name="__RequestVerificationToken" type="hidden" value="JP4znX9hJ_O-SSE8zahxr4ISKfVNMKdRI5vptghhNS1g6msHmHXfhCGCELEQK9VH2g-0vscAgFxGI-8TM4zYwB0y2MOLoOVKrpmbKJWevow1" />
                    <div class="standard-sub-section">
                        <div class="standard-form-title"> Informasi Pribadi </div>
                        <div class="standard-form-content form_subcategory">
                            <div class="form-group required-form-group"> <label for="UserName">Nama Pengguna</label> <input MaxLength="12" autocomplete="off" class="form-control lowercase" data-val="true" data-val-regex="The field UserName must match the regular expression &#39;^[0-9a-zA-Z]{3,12}$&#39;." data-val-regex-pattern="^[0-9a-zA-Z]{3,12}$" data-val-required="The UserName field is required." id="UserName" name="UserName" placeholder="Nama Pengguna Anda" type="text" value="" /> <span class="standard-required-message"> Harap masukkan antara 3 - 12 karakter dalam alfanumerik! <br /> Nama pengguna tidak boleh memiliki spasi! </span> </div>
                            <div class="form-group required-form-group standard-password-field"> <label for="Password">Kata Sandi</label> <input MaxLength="20" class="form-control" data-val="true" data-val-regex="The field Password must match the regular expression &#39;^(?=.{8,20}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9]).*$&#39;." data-val-regex-pattern="^(?=.{8,20}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9]).*$" data-val-required="The Password field is required." id="Password" name="Password" placeholder="Kata Sandi Anda" type="password" /> <span class="standard-required-message">Password harus terdiri dari 8-20 karakter, dan harus mengandung setidaknya satu huruf besar, satu huruf kecil, dan satu angka.</span> <i class="glyphicon glyphicon-eye-close password_input_trigger"></i> </div>
                            <div class="form-group required-form-group standard-password-field"> <label for="ConfirmedPassword">Ulangi Kata Sandi</label> <input MaxLength="20" class="form-control" data-val="true" data-val-equalto="&#39;ConfirmedPassword&#39; and &#39;Password&#39; do not match." data-val-equalto-other="*.Password" data-val-required="The ConfirmedPassword field is required." id="ConfirmedPassword" name="ConfirmedPassword" placeholder="Ulangi Kata Sandi Anda" type="password" /> <span class="standard-required-message">Konfirmasi kata sandi tidak cocok!</span> <i class="glyphicon glyphicon-eye-close password_input_trigger"></i> </div>
                            <div class="form-group register-form-note"> <span>Catatan:</span><br>*Password harus terdiri dari 8-20 karakter.<br>*Password harus mengandung setidaknya satu huruf besar, satu huruf kecil, dan satu angka. <br>*Password tidak boleh mengandung username. </div>
                            <div class="form-group "> <label for="Email">Email</label> <input MaxLength="100" autocomplete="off" class="form-control" data-val="true" data-val-email="The Email field is not a valid e-mail address." id="Email" name="Email" placeholder="Email@example.com" type="text" value="" /> <span class="standard-required-message">Email salah.</span> </div>
                            <div class="form-group register-form-note"> Silakan masukkan email yang aktif untuk tujuan reset kata sandi </div>
                            <div class="form-group required-form-group"> <label for="Phone">No. Kontak</label>
                                <div data-section="input" class="copy-input-button-field"> <input MaxLength="13" autocomplete="off" class="form-control" data-val="true" data-val-length="The field Phone must be a string with a minimum length of 10 and a maximum length of 13." data-val-length-max="13" data-val-length-min="10" data-val-regex="The field Phone must match the regular expression &#39;^[0-9]+$&#39;." data-val-regex-pattern="^[0-9]+$" id="Phone" name="Phone" placeholder="Nomor Telepon Anda" required="required" type="text" value="" /> <span class="standard-required-message">Nomor Kontak Harus diantara 10 dan 13 digit</span> <button class="copy-input-button" id="copy_phone_button" type="button"> <i class="glyphicon glyphicon-file"></i> </button> </div>
                            </div>
                        </div>
                    </div>
                    <div class="standard-sub-section" id="bank_account_section">
                        <div class="standard-form-title"> Informasi Pembayaran </div>
                        <div class="standard-form-content form_subcategory">
                            <div class="form-group"> <label for="SelectedBank">Metode Pembayaran</label>
                                <div data-section="input"> <label> <input type="radio" name="bank_section_radio_group" value="bank" checked> BANK </label> </div>
                            </div>
                            <div class="form-group required-form-group"> <label for="SelectedBank">Bank</label> <select class="form-control" data-val="true" data-val-required="Bank harus diisi." id="selected_bank_select" name="SelectedBank">
                                    <option value="">-- Pilih Bank --</option>
                                    <option value="BSI">BANK BSI</option>
                                    <option value="BCA">BCA</option>
                                    <option value="BNI">BNI</option>
                                    <option value="BRI">BRI</option>
                                    <option value="CIMB">CIMB</option>
                                    <option value="DANA">DANA</option>
                                    <option value="DANAMON">DANAMON</option>
                                    <option value="GOPAY">GOPAY</option>
                                    <option value="MANDIRI">MANDIRI</option>
                                    <option value="MAYBANK">MAYBANK</option>
                                    <option value="OVO">OVO</option>
                                </select> <span class="standard-required-message">Bank harus diisi.</span> </div>
                            <div class="form-group required-form-group"> <label for="BankAccountName">Nama Rekening</label> <input MaxLength="100" autocomplete="off" class="form-control" data-val="true" data-val-regex="The field BankAccountName must match the regular expression &#39;^[0-9a-zA-Z ]*$&#39;." data-val-regex-pattern="^[0-9a-zA-Z ]*$" data-val-required="Nama rekening bank harus diisi dan hanya boleh berisi karakter alfanumerik." id="bank_account_name_input" name="BankAccountName" placeholder="Nama lengkap anda sesuai dengan buku tabungan" type="text" value="" /> <span class="standard-required-message">Nama rekening bank harus diisi dan hanya boleh berisi karakter alfanumerik.</span> </div>
                            <div class="form-group required-form-group"> <label for="BankAccountNumber">Nomor Rekening</label> <input MaxLength="20" autocomplete="off" class="form-control" data-val="true" data-val-regex="The field BankAccountNumber must match the regular expression &#39;^[0-9]+$&#39;." data-val-regex-pattern="^[0-9]+$" data-val-required="Rekening bank harus diisi. (Hanya nomor)" id="bank_account_number_input" name="BankAccountNumber" placeholder="Nomor rekening anda" type="text" value="" /> <span class="standard-required-message">Rekening bank harus diisi. (Hanya nomor)</span> </div>
                        </div>
                    </div>
                    <div class="standard-sub-section">
                        <div class="standard-form-title"> Lainnya </div>
                        <div class="standard-form-content form_subcategory">
                            <div class="form-group required-form-group"> <label for="VerificationCode">Kode Verifikasi</label>
                                <div data-section="input" class="captcha-input"> <input autocomplete="off" class="form-control" data-val="true" data-val-required="The VerificationCode field is required." id="VerificationCode" name="VerificationCode" placeholder="Validasi" type="text" value="" /> <span class="standard-required-message">Silakan masukkan captcha!</span>
                                    <div class="captcha-container"> <img id="captcha_image" src="/captcha" /> <i class="glyphicon glyphicon-refresh refresh-captcha-button" id="refresh_captcha_button"></i> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="standard-button-group"> <input type="submit" class="standard-secondary-button" value="Daftar" /> </div>
                </form>
                <div class="register-page-reminder"> Dengan meng-klik tombol DAFTAR, saya menyatakan bahwa saya berumur diatas 18 tahun dan telah membaca dan menyetujui syarat &amp; ketentuan <?= getenv('APP_NAME') ?>. </div>
                <div class="register-page-link"> <a href="javascript:window.openNewTab('/mobile/terms-of-use','T&C-Page')"> SYARAT &amp; KETENTUAN </a> </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>