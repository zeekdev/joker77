<?= $this->extend('layouts/mobile/deposit') ?>

<?= $this->section('content') ?>
<div class="standard-form-container deposit-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ul class="bank-status-list">
                    <li data-online="true"> <img src="https://api2-82b.imgnxa.com/images/BANKBSI_be376934-0b97-4259-83ad-e0b506fb6b29_1697948252267.png" /> </li>
                    <li data-online="true"> <img src="https://aqua-peaceful-jay-983.mypinata.cloud/ipfs/QmSXuNmg5vL1WC8iUhsVQyguqK5FgcXKiVUsmv44yoEBd3" /> </li>
                    <li data-online="true"> <img src="https://api2-82b.imgnxa.com/images/BRI_a458ab91-91a3-49ac-98b3-1bfc5d1966bd_1699833956440.png" /> </li>
                    <li data-online="true"> <img src="https://api2-82b.imgnxa.com/images/DANAMON_67568e69-ca77-43c8-bf9b-df628bc3b2d6_1668185529443.png" /> </li>
                    <li data-online="true"> <img src="https://api2-82b.imgnxa.com/images/MANDIRI_ec4427ff-2e6e-4657-a2fe-b3702bc15e7c_1690996411540.png" /> </li>
                    <li data-online="true"> <img src="https://api2-82b.imgnxa.com/images/TELKOMSEL_708c135d-74c5-482f-9d03-27a5f7035c60_1676674896407.png" /> </li>
                    <li data-online="true"> <img src="https://api2-82b.imgnxa.com/images/XL_ea2a82b1-ca96-4eb1-9a52-cf378c6405e7_1697816326943.png" /> </li>
                </ul>
                <div class="tab-menu-background-container">
                    <div class="tab-menu-container"> <a href="/mobile/deposit" data-active="true"> <i data-icon="deposit" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/deposit.svg?v=20231205-1);--active-image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/deposit-active.svg?v=20231205-1);"></i> Deposit </a> <a href="/mobile/withdrawal" data-active="false"> <i data-icon="withdrawal" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/withdrawal.svg?v=20231205-1);--active-image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/withdrawal-active.svg?v=20231205-1);"></i> Penarikan </a> </div>
                </div>
                <?php if(session()->has('msg')): ?>
                <div class="alert alert-success">
                    Permintaan deposit berhasil dikirim.
                </div>
                <?php endif; ?>
                <div class="standard-form-note deposit-note">
                    <div class="deposit-note-icon"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/wallet/deposit.svg?v=20231205-1" /> </div>
                    <div class="deposit-note-content"> <span>Catatan:</span>
                        <ul>
                            <li>Untuk deposit pertama kali member harus menambah akun pembayaran terlebih dahulu.</li>
                            <li>Jika ingin deposit diluar nominal yang sudah ditentukan, harap pilih &#39;Akun Tujuan&#39; lain.</li>
                            <li>Biaya admin akan diinfokan ketika proses transaksi telah selesai di proses.</li>
                        </ul>
                    </div>
                </div>
                <div class="form-group form-group-link-container"> <a href="/mobile/history/deposit">Riwayat Deposit</a> </div>
                <div class="balance-info-container"> <a href="/mobile/bank-account/bank"> <i class="glyphicon glyphicon-plus"></i> Akun </a>
                    <div class="total-balance">
                        <p>TOTAL SALDO</p> <span> 0.00 </span>
                    </div>
                </div>
                <form action="/Wallet/BankDeposit" enctype="multipart/form-data" id="deposit_form" method="post" name="depositForm"><input name="__RequestVerificationToken" type="hidden" value="HrHLeSFxXJKY2GSIzyCAQ-HUYZnXubnoeBv4TTtiBGJnuzVFhzgQF_fG_ZxLhNKf_Y-3eiZPvylGXN--c4FocrBDv3_OKlk2FrHVciCL_Vc1" />
                    <div class="form-group deposit-form-group"> <label for="PaymentMethod">Metode Pembayaran</label>
                        <div id="payment_method_selection" class="payment-method-selection"> <input type="radio" name="PaymentType" id="payment_method_BANK" value="BANK" checked /> <label for="payment_method_BANK"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/payment-types/BANK.svg?v=20231205-1" /> <span>Bank</span> </label> <input type="radio" name="PaymentType" id="payment_method_PULSA" value="PULSA" /> <label for="payment_method_PULSA"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/payment-types/PULSA.svg?v=20231205-1" /> <span>Pulsa</span> </label> </div> <span class="field-validation-valid" data-valmsg-for="PaymentType" data-valmsg-replace="true"></span>
                    </div>
                    <div class="form-group deposit-form-group"> <label for="Amount">Jumlah</label>
                        <div class="deposit-amount-container">
                            <div data-section="depo-amount">
                                <div data-field="amount">
                                    <div id="predefined_deposit_amount_selection" class="predefined-deposit-amount-selection" style="display: none"> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_10" value="10" /> <label for="predefined_value_10">10</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_15" value="15" /> <label for="predefined_value_15">15</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_20" value="20" /> <label for="predefined_value_20">20</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_25" value="25" /> <label for="predefined_value_25">25</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_30" value="30" /> <label for="predefined_value_30">30</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_40" value="40" /> <label for="predefined_value_40">40</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_50" value="50" /> <label for="predefined_value_50">50</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_75" value="75" /> <label for="predefined_value_75">75</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_100" value="100" /> <label for="predefined_value_100">100</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_150" value="150" /> <label for="predefined_value_150">150</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_200" value="200" /> <label for="predefined_value_200">200</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_250" value="250" /> <label for="predefined_value_250">250</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_300" value="300" /> <label for="predefined_value_300">300</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_350" value="350" /> <label for="predefined_value_350">350</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_400" value="400" /> <label for="predefined_value_400">400</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_450" value="450" /> <label for="predefined_value_450">450</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_500" value="500" /> <label for="predefined_value_500">500</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_600" value="600" /> <label for="predefined_value_600">600</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_700" value="700" /> <label for="predefined_value_700">700</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_800" value="800" /> <label for="predefined_value_800">800</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_900" value="900" /> <label for="predefined_value_900">900</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_1000" value="1000" /> <label for="predefined_value_1000">1000</label> <input type="radio" name="PredefinedDepositAmount" id="predefined_value_2000" value="2000" /> <label for="predefined_value_2000">2000</label> </div> <input autocomplete="off" class="form-control deposit_amount_input" data-val="true" data-val-required="The Amount field is required." id="Amount" name="Amount" type="text" value="" /> <span class="standard-required-message">Silahkan masukan angka untuk jumlah deposit.</span>
                                </div> <span id="fast_deposit" data-field="reference-number" style="display:none;"> <input class="form-control" id="account_number_reference" readonly /> </span> <span id="fast_deposit_copy" data-field="copy" style="display:none;"> <button class="copy-bank-account-button  form-control" id="copy_bank_account_ref_button" type="button"> <span class="glyphicon glyphicon-file"></span> </button> </span>
                            </div>
                            <div class="deposit-amount-range"> <span id="deposit_amount_range_label"></span> </div>
                            <div class="real-deposit-amount" id="real_deposit_amount" data-title="Jumlah yang harus di transfer"></div>
                            <div class="fast-deposit-note" id="fast_deposit_note" style="display:none;">Transfer sesuai dengan nominal yang tertera pada jumlah yang harus di transfer</div>
                        </div>
                    </div>
                    <div class="deposit-form-group">
                        <div class="form-group"> <label for="FromAccount">Akun Asal</label> <select class="form-control" data-val="true" data-val-required="The FromAccountNumber field is required." id="from_bank_account_select" name="FromAccountNumber">
                                <?php foreach($banks as $val): ?>
                                    <option value="<?= $val['bank']."|".$val['norek'] ?>"><?= $val['bank']."|".$val['norek'] ?></option>
                                <?php endforeach; ?>
                            </select> <span class="standard-required-message">Pilih Akun Asal untuk disetor</span> </div>
                    </div>
                    <div class="deposit-form-group">
                        <?php $destination = $this->destination(); ?>
                        <div class="form-group">
                            <div class="to-account-label-container"> <label for="ToAccount">Akun Tujuan</label> <span id="view_all_available_banks">Lihat Semua</span> </div> <select name="CompanyBankId" id="deposit_bank_select" class="form-control" data-val="true" data-val-required="Pilih bank perusahaan untuk disetor">
                                <option value="MANDIRI" data-bank-name="MANDIRI" data-account-holder="<?= $destination['mandiri'][1] ?>" data-account-number="<?= $destination['mandiri'][0] ?>" data-supported-banks="BANK BSI;BCA;BRI;CIMB;DANAMON;MANDIRI;MAYBANK;GOPAY;BNI" data-is-auto-approve="false" data-conversion-rate="0.0" data-minimum-deposit-amount="10.000000" data-maximum-deposit-amount="20000.000000" data-deposit-amount-range="Min: 10.00 | Max: 20,000.00" data-high-priority="false" data-use-predefined-deposit-amounts="false" data-admin-fee="0" data-need-reveal-button="false" data-bank-id="72d98dcc-e960-46c1-93cd-60a9dd511378" data-payment-type="BANK" data-qr-code="" data-qr-code-format="" data-is-online="true" data-bank-logo="//nx-cdn.trgwl.com/Images/bank-thumbnails/MANDIRI.webp?v=20231129-1"> MANDIRI | <?= $destination['mandiri'][0] ?> </option>
                                <option value="DANA" data-bank-name="DANA" data-account-holder="<?= $destination['dana'][1] ?>" data-account-number="<?= $destination['dana'][0] ?>" data-supported-banks="BANK BSI;BCA;BRI;CIMB;DANAMON;MANDIRI;MAYBANK;GOPAY;BNI;SHOPEEPAY;DANA" data-is-auto-approve="false" data-conversion-rate="0.0" data-minimum-deposit-amount="10.000000" data-maximum-deposit-amount="9900.000000" data-deposit-amount-range="Min: 10.00 | Max: 9,900.00" data-high-priority="false" data-use-predefined-deposit-amounts="false" data-admin-fee="0" data-need-reveal-button="false" data-bank-id="77af1e67-b647-4d23-8172-8338903a0d3e" data-payment-type="BANK" data-qr-code="" data-qr-code-format=".png" data-is-online="true" data-bank-logo="//nx-cdn.trgwl.com/Images/bank-thumbnails/DANA.webp?v=20231129-1"> DANA | <?= $destination['dana'][0] ?> </option>
                                <option value="GOPAY" data-bank-name="GOPAY" data-account-holder="<?= $destination['gopay'][1] ?>" data-account-number="<?= $destination['gopay'][0] ?>" data-supported-banks="OVO;BANK BSI;BCA;BRI;CIMB;DANAMON;MANDIRI;MAYBANK;GOPAY;BNI;SHOPEEPAY;DANA" data-is-auto-approve="false" data-conversion-rate="0.0" data-minimum-deposit-amount="10.000000" data-maximum-deposit-amount="5000.000000" data-deposit-amount-range="Min: 10.00 | Max: 5,000.00" data-high-priority="false" data-use-predefined-deposit-amounts="false" data-admin-fee="0" data-need-reveal-button="false" data-bank-id="1cf68445-b45f-4579-addc-f7615657f2ef" data-payment-type="BANK" data-qr-code="" data-qr-code-format="" data-is-online="true" data-bank-logo="//nx-cdn.trgwl.com/Images/bank-thumbnails/GOPAY.webp?v=20231129-1"> GOPAY | <?= $destination['gopay'][0] ?> </option>
                                <option value="BCA" data-bank-name="BCA" data-account-holder="<?= $destination['bca'][1] ?>" data-account-number="<?= $destination['bca'][0] ?>" data-supported-banks="OVO;BANK BSI;BCA;BRI;CIMB;DANAMON;MANDIRI;MAYBANK;GOPAY;BNI;SHOPEEPAY;DANA" data-is-auto-approve="true" data-conversion-rate="0.0" data-minimum-deposit-amount="10.000000" data-maximum-deposit-amount="30000.000000" data-deposit-amount-range="Min: 10.00 | Max: 30,000.00" data-high-priority="false" data-use-predefined-deposit-amounts="false" data-admin-fee="0" data-need-reveal-button="false" data-bank-id="3111b902-7b2e-43e9-958c-7c27b88ab765" data-payment-type="BANK" data-qr-code="" data-qr-code-format="" data-is-online="true" data-bank-logo="//nx-cdn.trgwl.com/Images/bank-thumbnails/BCA.webp?v=20231129-1"> BCA | <?= $destination['bca'][0] ?> </option>
                                <option value="BNI" data-bank-name="BNI" data-account-holder="<?= $destination['bni'][1] ?>" data-account-number="<?= $destination['bni'][0] ?>" data-supported-banks="OVO;BANK BSI;BCA;BRI;CIMB;DANAMON;MANDIRI;MAYBANK;GOPAY;BNI;SHOPEEPAY;DANA" data-is-auto-approve="false" data-conversion-rate="0.0" data-minimum-deposit-amount="10.000000" data-maximum-deposit-amount="20000.000000" data-deposit-amount-range="Min: 10.00 | Max: 20,000.00" data-high-priority="false" data-use-predefined-deposit-amounts="false" data-admin-fee="0" data-need-reveal-button="false" data-bank-id="9cb1a54b-0812-4e7d-a407-b618b741a2ee" data-payment-type="BANK" data-qr-code="" data-qr-code-format="png" data-is-online="true" data-bank-logo="//nx-cdn.trgwl.com/Images/bank-thumbnails/BNI.webp?v=20231129-1"> BNI | <?= $destination['bni'][0] ?> </option>
                                <option value="BRI" data-bank-name="BRI" data-account-holder="<?= $destination['bri'][1] ?>" data-account-number="<?= $destination['bri'][0] ?>" data-supported-banks="OVO;BANK BSI;BCA;BRI;CIMB;DANAMON;MANDIRI;MAYBANK;GOPAY;BNI;SHOPEEPAY;DANA" data-is-auto-approve="false" data-conversion-rate="0.0" data-minimum-deposit-amount="10.000000" data-maximum-deposit-amount="25000.000000" data-deposit-amount-range="Min: 10.00 | Max: 25,000.00" data-high-priority="false" data-use-predefined-deposit-amounts="false" data-admin-fee="0" data-need-reveal-button="false" data-bank-id="53aa0119-2a6d-485d-be58-db376ac6ab73" data-payment-type="BANK" data-qr-code="" data-qr-code-format="" data-is-online="true" data-bank-logo="//nx-cdn.trgwl.com/Images/bank-thumbnails/BRI.webp?v=20231129-1"> BRI | <?= $destination['bri'][0] ?> </option>
                            </select> <span class="standard-required-message">Pilih bank perusahaan untuk disetor</span>
                        </div>
                        <div class="form-group">
                            <div data-section="input" data-bank-type="bank" class="bank-info" id="bank_info">
                                <div class="recommended-for-instant-process">Rekomendasi<span>(Proses Instan)</span></div>
                                <div data-bank-info="header">
                                    <h1 id="bank_info_account_name"></h1>
                                    <div id="bank_info_logo" data-image-path="//nx-cdn.trgwl.com/Images/banks/"></div>
                                    <h3 id="bank_info_name" class="bank-name"></h3>
                                </div>
                                <div data-bank-info="details">
                                    <h2 id="bank_info_account_no"></h2>
                                </div>
                                <div data-bank-info="qrcode" id="bank_qr_code"></div>
                                <h4 id="qr_code_note">Scan/Screenshot QR Code ini untuk Transfer ke Provider yang dituju</h4>
                                <hr />
                                <div data-bank-info="actions">
                                    <div class="admin-fee-container"> Biaya Admin: <div id="admin_fee_display" class="admin-fee">0</div>
                                    </div> <button class="btn btn-secondary reveal-bank-account-button" id="reveal_bank_account_button" type="button"> Tunjukkan Nomor Rekening </button> <button class="copy-bank-account-button" id="copy_bank_account_button" type="button"> <span class="glyphicon glyphicon-file"></span> Salin </button>
                                </div> <input id="bank_info_account_no_hidden_input" name="ToAccountNumber" type="hidden" value="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div id="available_banks_popup" class="available-banks-popup">
                                <div id="available_banks_container" class="available-banks-container" data-default-bank-icon="//nx-cdn.trgwl.com/Images/bank-thumbnails/default.webp?v=20231205-1"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group deposit-form-group"> <label for="ReferenceNumber">Nomor referensi</label> <input autocomplete="off" class="form-control " data-val="true" data-val-regex="The field ReferenceNumber must match the regular expression &#39;^[0-9a-zA-Z ]*$&#39;." data-val-regex-pattern="^[0-9a-zA-Z ]*$" id="ReferenceNumber" name="ReferenceNumber" type="text" value="" /> <span class="standard-required-message">Karakter khusus tidak diperbolehkan</span> </div>
                    <div class="standard-inline-form-group deposit-form-group"> <label for="TransactionReceipt">Tanda Terima Transaksi</label>
                        <div data-section="input"> <input Class="form-control" id="TransactionReceipt" name="TransactionReceipt" type="file" value="" /> </div>
                    </div> <input id="is_fast_deposit_hidden_input" name="IsFastDeposit" type="hidden" value="False" />
                    <div class="standard-button-group"> <input type="submit" class="standard-secondary-button" value="DEPOSIT" /> </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>