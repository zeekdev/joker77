<div class="topbar-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 topbar-inner-container">
                <div class="topbar-left-section">
                    <div class="topbar-item language-selector-container" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/flags.png?v=20231115);">
                        <div id="language_selector_trigger" data-toggle="dropdown" class="language-selector-trigger" data-language="id"> <i data-language="id"></i> </div>
                        <ul class="dropdown-menu language-selector">
                            <li class="language_selector" data-language="en"> <i data-language="en"></i>
                                <div class="language-name">
                                    <div>ENGLISH</div>
                                    <div>ENGLISH</div>
                                </div>
                            </li>
                            <li class="language_selector" data-language="id"> <i data-language="id"></i>
                                <div class="language-name">
                                    <div>BHS INDONESIA</div>
                                    <div>INDONESIAN</div>
                                </div>
                            </li>
                            <li class="language_selector" data-language="kr"> <i data-language="kr"></i>
                                <div class="language-name">
                                    <div>한국어</div>
                                    <div>KOREAN</div>
                                </div>
                            </li>
                            <li class="language_selector" data-language="cn"> <i data-language="cn"></i>
                                <div class="language-name">
                                    <div>中文</div>
                                    <div>CHINESE</div>
                                </div>
                            </li>
                            <li class="language_selector" data-language="jp"> <i data-language="jp"></i>
                                <div class="language-name">
                                    <div>日本語</div>
                                    <div>JAPANESE</div>
                                </div>
                            </li>
                            <li class="language_selector" data-language="th"> <i data-language="th"></i>
                                <div class="language-name">
                                    <div>ไทย</div>
                                    <div>THAI</div>
                                </div>
                            </li>
                            <li class="language_selector" data-language="my"> <i data-language="my"></i>
                                <div class="language-name">
                                    <div>မြန်မာစာ</div>
                                    <div>BURMESE</div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="topbar-item"> <a href="javascript:void(0)" class="js_live_chat_link"> <i data-icon="live-chat" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/live-chat.svg?v=20231115);"></i> Live Chat </a> </div>
                    <div class="topbar-item"> <a href="/mobile/home" rel="nofollow"> <i data-icon="mobile" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/mobile.svg?v=20231115);"></i> Versi Mobile </a> </div>
                </div>
                <div class="user-info">
                    <div class="user-info-item"> <button title="Refresh" id="refresh_balance" data-loading="false">
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/refresh.webp?v=20231115" type="image/webp">
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/refresh.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/refresh.png?v=20231115">
                            </picture>
                        </button> </div>
                    <div class="user-info-item wallet-container"> <?= session()->get('username') ?> <div class="balance"> <a href="#" data-toggle="dropdown"> IDR <span class="total_balance">0.00</span> <span class="locked-balance locked_balance_container" hidden=""> <i data-icon="locked-balance" class="glyphicon glyphicon-lock"></i> <span class="total_locked_balance"></span> </span> </a>
                            <div class="dropdown-menu vendor-balances-container">
                                <div class="vendor-balances-header">
                                    <div>SALDO KREDIT</div>
                                    <div>0.00</div>
                                </div>
                                <div class="vendor-balances-content">
                                    <div> <strong>Slots</strong>
                                        <div class="vendor-balance-item">
                                            <div>
                                                <div>Pragmatic Play</div>
                                                <div data-vendor-game-code="7">0.00</div>
                                            </div>
                                            <div>
                                                <div>MicroGaming</div>
                                                <div data-vendor-game-code="17">0.00</div>
                                            </div>
                                            <div>
                                                <div>PG Slots</div>
                                                <div data-vendor-game-code="9">0.00</div>
                                            </div>
                                            <div>
                                                <div>Reel Kingdom by Pragmatic</div>
                                                <div data-vendor-game-code="74">0.00</div>
                                            </div>
                                            <div>
                                                <div>Habanero</div>
                                                <div data-vendor-game-code="16">0.00</div>
                                            </div>
                                            <div>
                                                <div>No Limit City</div>
                                                <div data-vendor-game-code="92">0.00</div>
                                            </div>
                                            <div>
                                                <div>AdvantPlay</div>
                                                <div data-vendor-game-code="54">0.00</div>
                                            </div>
                                            <div>
                                                <div>Joker</div>
                                                <div data-vendor-game-code="6">0.00</div>
                                            </div>
                                            <div>
                                                <div>Jili</div>
                                                <div data-vendor-game-code="70">0.00</div>
                                            </div>
                                            <div>
                                                <div>Spinix</div>
                                                <div data-vendor-game-code="91">0.00</div>
                                            </div>
                                            <div>
                                                <div>Crowd Play</div>
                                                <div data-vendor-game-code="73">0.00</div>
                                            </div>
                                            <div>
                                                <div>Live22</div>
                                                <div data-vendor-game-code="45">0.00</div>
                                            </div>
                                            <div>
                                                <div>Playstar</div>
                                                <div data-vendor-game-code="65">0.00</div>
                                            </div>
                                            <div>
                                                <div>VPower</div>
                                                <div data-vendor-game-code="77">0.00</div>
                                            </div>
                                            <div>
                                                <div>Worldmatch</div>
                                                <div data-vendor-game-code="89">0.00</div>
                                            </div>
                                            <div>
                                                <div>Fachai</div>
                                                <div data-vendor-game-code="72">0.00</div>
                                            </div>
                                            <div>
                                                <div>Slot88</div>
                                                <div data-vendor-game-code="40">0.00</div>
                                            </div>
                                            <div>
                                                <div>ION Slot</div>
                                                <div data-vendor-game-code="50">0.00</div>
                                            </div>
                                            <div>
                                                <div>AMB Slot</div>
                                                <div data-vendor-game-code="61">0.00</div>
                                            </div>
                                            <div>
                                                <div>Mario Club</div>
                                                <div data-vendor-game-code="80">0.00</div>
                                            </div>
                                            <div>
                                                <div>Dragoonsoft</div>
                                                <div data-vendor-game-code="81">0.00</div>
                                            </div>
                                            <div>
                                                <div>Spade Gaming</div>
                                                <div data-vendor-game-code="29">0.00</div>
                                            </div>
                                            <div>
                                                <div>Fun Gaming</div>
                                                <div data-vendor-game-code="79">0.00</div>
                                            </div>
                                            <div>
                                                <div>Naga Games</div>
                                                <div data-vendor-game-code="87">0.00</div>
                                            </div>
                                            <div>
                                                <div>JDB</div>
                                                <div data-vendor-game-code="51">0.00</div>
                                            </div>
                                            <div>
                                                <div>CQ9</div>
                                                <div data-vendor-game-code="13">0.00</div>
                                            </div>
                                            <div>
                                                <div>Top Trend Gaming</div>
                                                <div data-vendor-game-code="67">0.00</div>
                                            </div>
                                            <div>
                                                <div>Netent</div>
                                                <div data-vendor-game-code="94">0.00</div>
                                            </div>
                                            <div>
                                                <div>Big Time Gaming</div>
                                                <div data-vendor-game-code="95">0.00</div>
                                            </div>
                                            <div>
                                                <div>Red Tiger</div>
                                                <div data-vendor-game-code="93">0.00</div>
                                            </div>
                                            <div>
                                                <div>Skywind</div>
                                                <div data-vendor-game-code="90">0.00</div>
                                            </div>
                                            <div>
                                                <div>Playtech</div>
                                                <div data-vendor-game-code="2">0.00</div>
                                            </div>
                                            <div>
                                                <div>Yggdrasil</div>
                                                <div data-vendor-game-code="42">0.00</div>
                                            </div>
                                            <div>
                                                <div>Play'n Go</div>
                                                <div data-vendor-game-code="18">0.00</div>
                                            </div>
                                            <div>
                                                <div>Real Time Gaming</div>
                                                <div data-vendor-game-code="28">0.00</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div> <strong>Live Casino</strong>
                                        <div class="vendor-balance-item">
                                            <div>
                                                <div>PP Casino</div>
                                                <div data-vendor-game-code="41">0.00</div>
                                            </div>
                                            <div>
                                                <div>MG Live</div>
                                                <div data-vendor-game-code="66">0.00</div>
                                            </div>
                                            <div>
                                                <div>Evo Gaming</div>
                                                <div data-vendor-game-code="38">0.00</div>
                                            </div>
                                            <div>
                                                <div>Sexy Baccarat</div>
                                                <div data-vendor-game-code="27">0.00</div>
                                            </div>
                                            <div>
                                                <div>Pretty Gaming</div>
                                                <div data-vendor-game-code="39">0.00</div>
                                            </div>
                                            <div>
                                                <div>Asia Gaming</div>
                                                <div data-vendor-game-code="14">0.00</div>
                                            </div>
                                            <div>
                                                <div>AllBet</div>
                                                <div data-vendor-game-code="44">0.00</div>
                                            </div>
                                            <div>
                                                <div>PGS Live</div>
                                                <div data-vendor-game-code="64">0.00</div>
                                            </div>
                                            <div>
                                                <div>SA Gaming</div>
                                                <div data-vendor-game-code="84">0.00</div>
                                            </div>
                                            <div>
                                                <div>Ebet</div>
                                                <div data-vendor-game-code="85">0.00</div>
                                            </div>
                                            <div>
                                                <div>Dream Gaming</div>
                                                <div data-vendor-game-code="43">0.00</div>
                                            </div>
                                            <div>
                                                <div>568Win Casino</div>
                                                <div data-vendor-game-code="10">0.00</div>
                                            </div>
                                            <div>
                                                <div>HKB</div>
                                                <div data-vendor-game-code="37">0.00</div>
                                            </div>
                                            <div>
                                                <div>SV388</div>
                                                <div data-vendor-game-code="57">0.00</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div> <strong>Togel</strong>
                                        <div class="vendor-balance-item">
                                            <div>
                                                <div>Nex4D</div>
                                                <div data-vendor-game-code="48">0.00</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div> <strong>Olahraga</strong>
                                        <div class="vendor-balance-item">
                                            <div>
                                                <div>SBO Sportsbook</div>
                                                <div data-vendor-game-code="5">0.00</div>
                                            </div>
                                            <div>
                                                <div>Saba Sportsbook</div>
                                                <div data-vendor-game-code="23">0.00</div>
                                            </div>
                                            <div>
                                                <div>Opus</div>
                                                <div data-vendor-game-code="71">0.00</div>
                                            </div>
                                            <div>
                                                <div>WBet</div>
                                                <div data-vendor-game-code="69">0.00</div>
                                            </div>
                                            <div>
                                                <div>IM Sportsbook</div>
                                                <div data-vendor-game-code="86">0.00</div>
                                            </div>
                                            <div>
                                                <div>Pinnacle</div>
                                                <div data-vendor-game-code="59">0.00</div>
                                            </div>
                                            <div>
                                                <div>CMD</div>
                                                <div data-vendor-game-code="83">0.00</div>
                                            </div>
                                            <div>
                                                <div>SBO Virtual Sports</div>
                                                <div data-vendor-game-code="11">0.00</div>
                                            </div>
                                            <div>
                                                <div>PP Virtual Sports</div>
                                                <div data-vendor-game-code="55">0.00</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div> <strong>Crash Game</strong>
                                        <div class="vendor-balance-item">
                                            <div>
                                                <div>PP Casino</div>
                                                <div data-vendor-game-code="41">0.00</div>
                                            </div>
                                            <div>
                                                <div>Spribe</div>
                                                <div data-vendor-game-code="82">0.00</div>
                                            </div>
                                            <div>
                                                <div>MicroGaming</div>
                                                <div data-vendor-game-code="17">0.00</div>
                                            </div>
                                            <div>
                                                <div>Spinix</div>
                                                <div data-vendor-game-code="91">0.00</div>
                                            </div>
                                            <div>
                                                <div>AdvantPlay Mini Game</div>
                                                <div data-vendor-game-code="62">0.00</div>
                                            </div>
                                            <div>
                                                <div>Joker</div>
                                                <div data-vendor-game-code="6">0.00</div>
                                            </div>
                                            <div>
                                                <div>Dragoonsoft</div>
                                                <div data-vendor-game-code="81">0.00</div>
                                            </div>
                                            <div>
                                                <div>Funky Games</div>
                                                <div data-vendor-game-code="35">0.00</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div> <strong>Arcade</strong>
                                        <div class="vendor-balance-item">
                                            <div>
                                                <div>MicroGaming</div>
                                                <div data-vendor-game-code="17">0.00</div>
                                            </div>
                                            <div>
                                                <div>Spinix</div>
                                                <div data-vendor-game-code="91">0.00</div>
                                            </div>
                                            <div>
                                                <div>Spribe</div>
                                                <div data-vendor-game-code="82">0.00</div>
                                            </div>
                                            <div>
                                                <div>Joker</div>
                                                <div data-vendor-game-code="6">0.00</div>
                                            </div>
                                            <div>
                                                <div>Fachai</div>
                                                <div data-vendor-game-code="72">0.00</div>
                                            </div>
                                            <div>
                                                <div>Jili</div>
                                                <div data-vendor-game-code="70">0.00</div>
                                            </div>
                                            <div>
                                                <div>AMB Slot</div>
                                                <div data-vendor-game-code="61">0.00</div>
                                            </div>
                                            <div>
                                                <div>Crowd Play</div>
                                                <div data-vendor-game-code="73">0.00</div>
                                            </div>
                                            <div>
                                                <div>VPower</div>
                                                <div data-vendor-game-code="77">0.00</div>
                                            </div>
                                            <div>
                                                <div>Worldmatch</div>
                                                <div data-vendor-game-code="89">0.00</div>
                                            </div>
                                            <div>
                                                <div>Mario Club</div>
                                                <div data-vendor-game-code="80">0.00</div>
                                            </div>
                                            <div>
                                                <div>Dragoonsoft</div>
                                                <div data-vendor-game-code="81">0.00</div>
                                            </div>
                                            <div>
                                                <div>Live22</div>
                                                <div data-vendor-game-code="45">0.00</div>
                                            </div>
                                            <div>
                                                <div>CQ9</div>
                                                <div data-vendor-game-code="13">0.00</div>
                                            </div>
                                            <div>
                                                <div>Spade Gaming</div>
                                                <div data-vendor-game-code="29">0.00</div>
                                            </div>
                                            <div>
                                                <div>Fun Gaming</div>
                                                <div data-vendor-game-code="79">0.00</div>
                                            </div>
                                            <div>
                                                <div>Arcadia</div>
                                                <div data-vendor-game-code="63">0.00</div>
                                            </div>
                                            <div>
                                                <div>MM Tangkas</div>
                                                <div data-vendor-game-code="96">0.00</div>
                                            </div>
                                            <div>
                                                <div>Skywind</div>
                                                <div data-vendor-game-code="90">0.00</div>
                                            </div>
                                            <div>
                                                <div>Playstar</div>
                                                <div data-vendor-game-code="65">0.00</div>
                                            </div>
                                            <div>
                                                <div>JDB</div>
                                                <div data-vendor-game-code="51">0.00</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div> <strong>Poker</strong>
                                        <div class="vendor-balance-item">
                                            <div>
                                                <div>Balak Play</div>
                                                <div data-vendor-game-code="24">0.00</div>
                                            </div>
                                            <div>
                                                <div>9Gaming</div>
                                                <div data-vendor-game-code="32">0.00</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div> <strong>E-Sports</strong>
                                        <div class="vendor-balance-item">
                                            <div>
                                                <div>IM Esports</div>
                                                <div data-vendor-game-code="78">0.00</div>
                                            </div>
                                            <div>
                                                <div>Pinnacle E-Sports</div>
                                                <div data-vendor-game-code="60">0.00</div>
                                            </div>
                                            <div>
                                                <div>TF Gaming</div>
                                                <div data-vendor-game-code="58">0.00</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="user-info-item"> <a href="/desktop/deposit">
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/wallet.webp?v=20231115" type="image/webp">
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/wallet.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/wallet.png?v=20231115">
                            </picture>
                        </a> </div>
                    <div class="user-info-item"> <a href="/desktop/messages/inbox">
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/inbox.webp?v=20231115" type="image/webp">
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/inbox.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/inbox.png?v=20231115">
                            </picture>
                        </a> </div>
                    <div class="user-info-item"> <a href="/desktop/account-summary">
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/profile.webp?v=20231115" type="image/webp">
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/profile.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/profile.png?v=20231115">
                            </picture>
                        </a> </div>
                    <div class="user-info-item"> <a href="#" onclick="window.closeWindows(); document.querySelector('#logout-form').submit()">
                            <form action="/Account/Logout" id="logout-form" method="post">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/logout.webp?v=20231115" type="image/webp">
                                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/logout.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/logout.png?v=20231115">
                                </picture>
                            </form>
                        </a> </div>
                </div>
            </div>
        </div>
    </div>
</div>