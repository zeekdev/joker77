<?= $this->extend('layouts/mobile/slot') ?>

<?= $this->section('content') ?>
<div id="page_carousel" class="carousel slide page-carousel" data-ride="carousel" data-interval="5000">
    <ol class="carousel-indicators">
        <li class="active" data-target="#page_carousel" data-slide-to="0"></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/banners/poker/banner.jpg?v=20231123"> </div>
    </div>
</div>
<?= $this->include('components/mobile/menu-outer') ?>

<div class="game-list">
    <ul>
        <li> <a href="/mobile/poker/balak-play">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/g8poker.webp?v=20231123" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/g8poker.png?v=20231123" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/g8poker.png?v=20231123">
                </picture>
            </a> Balak Play </li>
        <li> <a href="/mobile/poker/9gaming">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/onepoker.webp?v=20231123" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/onepoker.png?v=20231123" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/onepoker.png?v=20231123">
                </picture>
            </a> 9Gaming </li>
    </ul>
</div>

<?= $this->include('components/mobile/popular') ?>

<?= $this->endSection() ?>