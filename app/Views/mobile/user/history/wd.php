<?= $this->extend('layouts/mobile/history') ?>

<?= $this->section('content') ?>
<div class="reporting-form-container">
    <div class="reporting-nav-bar"> <a href="/mobile/history/deposit" data-active="false"> DEPOSIT </a> <a href="/mobile/history/withdrawal" data-active="true"> PENARIKAN </a> <a href="/mobile/statement/consolidate" data-active="false"> RIWAYAT TARUHAN </a> </div>
    <form action="/mobile/history/withdrawal" method="post">
        <div class="reporting-control-group"> <label>Date Range</label> <input type="text" class="form-control" id="date_range_picker" data-picker="date-range" data-separator=" - "> <input type="hidden" name="StartingDate" id="starting_date" value="2023-12-09"> <input type="hidden" name="EndingDate" id="ending_date" value="2023-12-09"> </div>
        <div class="standard-button-group"> <button type="submit" class="standard-secondary-button"> Search </button> </div>
        <div class="reporting-scroll-container">
            <table class="table grid_table">
                <thead>
                    <tr>
                        <th scope="col"> Nomor Tiket </th>
                        <th scope="col"> Jumlah </th>
                        <th scope="col"> Tanggal/Waktu(GMT+7) </th>
                        <th scope="col"> Status </th>
                        <th scope="col"> Tanda Terima Transaksi </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</div>
<?= $this->endSection() ?>