<?php

namespace App\Controllers;

use App\Models\Bank;
use App\Models\DepositModel;
use App\Controllers\BaseController;

class Deposit extends BaseController
{
    public function __construct()
    {   
        $this->deposit_model = new DepositModel();
        $this->bank = new Bank(); 
    }

    public function getBanks()
    {
        return $this->bank->where('user_id', session()->get('id'))->findAll();
    }

    public function index()
    {
        return view('desktop/user/deposit/bank', [
            'banks' => $this->getBanks()
        ]);
    }

    public function mobile()
    {
        return view('mobile/user/wallet/bank', [
            'banks' => $this->getBanks()
        ]);
    }

    public function pulsa()
    {
        return view('desktop/user/deposit/pulsa');
    }

    public function pulsa_mobile()
    {
        return view('mobile/user/wallet/pulsa');
    }

    public function store()
    {
        $data = $this->request->getPost();
        
        $this->deposit_model->insert([
            'code' => 'D'.rand(10000, 99999),
            'from' => $data['FromAccountNumber'] ?? $data['TelephoneNumber'],
            'target' => $data['CompanyBankId'],
            'amount' => $data['Amount'],
            'user_id' => session()->get('id'),
            'created_at' => $this->currentDate()
        ]);

        return redirect()->back()->with('msg', 'ok');
    }
}
