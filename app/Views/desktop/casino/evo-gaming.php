<?= $this->extend('layouts/desktop/casino') ?>

<?= $this->section('content') ?>
<div class="pp-live-casino-container" data-container-background="casino">
    <div class="container casino-container">
        <div class="row">
            <div class="col-md-12">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/banners/casino/pp-live-casino.webp?v=20231115" type="image/webp" />
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/banners/casino/pp-live-casino.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/banners/casino/pp-live-casino.png?v=20231115" />
                </picture>
                <div class="games-list-container">
                    <div class="provider-outer-container">
                        <div class="vendor-name"> EVO GAMING </div>
                        <div class="filter-section">
                            <div class="category-filter" id="filter_categories">
                                <div class="category-filter-link active" data-category=""> Semua permainan </div>
                            </div> <input type="text" id="filter_input" placeholder="Cari Permainan">
                        </div>
                    </div>
                    <div class="game-list" id="game_list" style="--star-on-icon: url(//nx-cdn.trgwl.com/Images/icons/star-on.svg?v=20231115); --star-off-icon: url(//nx-cdn.trgwl.com/Images/icons/star-off.svg?v=20231115);"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
<script>
        window.addEventListener('DOMContentLoaded', () => {
            initializeCasinoGames({
                directoryPath: '//nx-cdn.trgwl.com/Images/providers/',
                provider: 'EVOGAMING',
                translations: {
                    playNow: 'MAIN',
                    demo: 'COBA',
                }
            });
        });
</script>
<?= $this->endSection() ?>    