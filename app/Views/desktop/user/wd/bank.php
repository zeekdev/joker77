<?= $this->extend('layouts/desktop/wd') ?>

<?= $this->section('content') ?>
<div class="standard-form-content withdrawal-container">
    <div class="standard-form-title">PENARIKAN</div>
    <?php if(session()->getFlashData('msg')): ?>
    <div class="form-group">
        <div class="alert-danger">Saldo tidak cukup untuk melakukan penarikan.</div>
    </div>
    <?php endif; ?>
    <div class="standard-form-note withdrawal-note">
        <div class="withdrawal-note-icon"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/wallet/withdrawal.svg?v=20231205-1" /> </div>
        <div class="withdrawal-note-content"> <span>Catatan:</span>
            <ul>
                <li>Jika ada beberapa perubahan dalam jadwal offline bank ini bukan otoritas kami.</li>
                <li>Biaya admin akan diinfokan ketika proses transaksi telah selesai di proses.</li>
            </ul>
        </div>
    </div>
    <div class="form-group withdrawal-form-group balance-info-container"> <label for="Balance">Total Saldo</label>
        <div data-section="input"> 0.00 <span class="formatted-balance"> (0.00) </span> </div>
    </div>
    <div class="tab-menu-container"> <a href="/desktop/history/withdrawal"> <i data-icon="withdrawal-history" style="background-image: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/tabs/withdrawal-history.svg?v=20231205-1);"></i> Riwayat Penarikan </a> <a href="/desktop/bank-account"> <i data-icon="bank-account" style="background-image: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/tabs/bank-account.svg?v=20231205-1);"></i> Tambah Akun </a> </div>
    <form action="/Wallet/BankWithdrawal" id="withdrawal_form" method="post" name="withdrawalForm"><input name="__RequestVerificationToken" type="hidden" value="HIft2K0D_5NISj7exx9vHUIz33qV8SuUtBlaRNWsEpaLbvOal2YUP_ciFCMwGkwrkhhPr_gCg2A-XnnLOxwNo7TiRQhn6b0DrKN1xjS2YlE1" />
        <div class="form-group withdrawal-form-group"> <label for="UserName">Nama Pengguna</label>
            <div data-section="input"> fikriroy </div>
        </div>
        <div class="form-group withdrawal-form-group"> <label for="PaymentMethod"> Metode Pembayaran <span data-section="asterisk">*</span> </label>
            <div data-section="input">
                <div id="payment_method_selection" class="payment-method-selection"> <input type="radio" name="PaymentType" id="payment_method_BANK" value="BANK" checked /> <label for="payment_method_BANK"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/payment-types/BANK.svg?v=20231205-1" /> <span>Bank</span> </label> 
            </div>
        </div>
        <div class="form-group withdrawal-form-group"> <label for="Amount"> Jumlah Penarikan <span data-section="asterisk">*</span> </label>
            <div data-section="input"> <input autocomplete="off" class="form-control withdrawal_amount_input" data-val="true" data-val-required="The Amount field is required." id="Amount" name="Amount" type="text" value="" /> <span class="standard-required-message">Silahkan masukan angka untuk jumlah penarikan.</span> </div>
            <div class="real-withdrawal-amount" id="real_withdrawal_amount"></div>
        </div>
        <div class="form-group withdrawal-form-group"> <label for="ToAccount"> Pilih Bank Anda <span data-section="asterisk">*</span> </label>
            <div data-section="input"> <select name="PlayerBankAccountNumber" id="withdrawal_bank_select" class="form-control" data-val="true" data-val-required="The PlayerBankAccountNumber field is required.">
                <?php foreach($banks as $val): ?>
                    <option value="<?= $val['norek']." (".$val['bank'].")" ?>" data-admin-fee="0"> <?= $val['bank']." | ".$val['norek'] ?></option>
                <?php endforeach; ?>
                </select> <span class="field-validation-valid" data-valmsg-for="PlayerBankAccountNumber" data-valmsg-replace="true"></span> </div>
        </div>
        <div class="form-group">
            <div data-section="input" data-bank-type="bank" class="bank-info" id="bank_info">
                <div data-bank-info="header">
                    <h2 id="bank_info_account_no"></h2>
                    <div id="bank_info_logo" data-image-path="//nx-cdn.trgwl.com/Images/banks/"></div>
                    <h3 id="bank_info_name"></h3>
                </div>
                <hr />
                <div data-bank-info="actions"> <button class="copy-bank-account-button" id="copy_bank_account_button" type="button"> <span class="glyphicon glyphicon-file"></span> Salin </button> </div>
            </div>
        </div>
        <div class="standard-button-group"> <input type="submit" class="btn btn-primary" value="TARIK" /> </div>
    </form>
</div>
<?= $this->endSection() ?>