<?= $this->extend('layouts/desktop/personal') ?>

<?= $this->section('content') ?>
<div class="standard-form-content">
    <div class="standard-form-title"> UBAH KATA SANDI </div>
    <div class="standard-form-note"> <span>Catatan:</span><br>*Password harus terdiri dari 8-20 karakter.<br>*Password harus mengandung setidaknya satu huruf besar, satu huruf kecil, dan satu angka. <br>*Password tidak boleh mengandung username. </div>
    <?php if(session()->getFlashData('msg')): ?>
    <div class="form-group">
        <div class="alert-success">Password berhasil diubah!</div>
    </div>
    <?php endif; ?>
    <form action="/Profile/Password" method="post" novalidate="novalidate"><input name="__RequestVerificationToken" type="hidden" value="jNw2ykM9YmJtDASadr4-G6nTDBXaIQQsE6j3yJx_fPPQlnRcziJ-PB8eM7zTW15SEHiooBcEzqWzHfLGA_xSJ3zF74UVXvyPODoGw9CcKUU1">
        <div class="standard-inline-form-group"> <label for="OldPassword">Kata Sandi Saat Ini</label>
            <div data-section="asterisk">*</div>
            <div data-section="input"> <input class="form-control" data-val="true" data-val-required="The OldPassword field is required." id="OldPassword" name="OldPassword" placeholder="Kata Sandi Saat Ini" type="password"> <span class="standard-required-message">Kata sandi harus diisi.</span> </div>
        </div>
        <div class="standard-inline-form-group"> <label for="NewPassword">Kata Sandi Baru</label>
            <div data-section="asterisk">*</div>
            <div data-section="input"> <input maxlength="20" class="form-control" data-val="true" data-val-regex="The field NewPassword must match the regular expression '^(?=.{8,20}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9]).*$'." data-val-regex-pattern="^(?=.{8,20}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9]).*$" data-val-required="The NewPassword field is required." id="NewPassword" name="NewPassword" placeholder="Kata Sandi Baru" type="password"> <span class="standard-required-message">Password harus terdiri dari 8-20 karakter, dan harus mengandung setidaknya satu huruf besar, satu huruf kecil, dan satu angka.</span> </div>
        </div>
        <div class="standard-inline-form-group"> <label for="ConfirmPassword">Ulangi Kata Sandi</label>
            <div data-section="asterisk">*</div>
            <div data-section="input"> <input maxlength="20" class="form-control" data-val="true" data-val-equalto="'ConfirmPassword' and 'NewPassword' do not match." data-val-equalto-other="*.NewPassword" data-val-required="The ConfirmPassword field is required." id="ConfirmPassword" name="ConfirmPassword" placeholder="Ulangi Kata Sandi" type="password"> <span class="standard-required-message">* Kata sandi tidak cocok.</span> </div>
        </div>
        <div class="standard-inline-form-group"> <label for="VerificationCode">Kode Verifikasi</label>
            <div data-section="asterisk">*</div>
            <div data-section="input" class="captcha-input"> <input autocomplete="off" class="form-control" data-val="true" data-val-required="The VerificationCode field is required." id="VerificationCode" name="VerificationCode" placeholder="Validasi" type="text" value=""> <span class="standard-required-message">Captcha salah.</span>
                <div class="captcha-container"> <i class="glyphicon glyphicon-refresh refresh-captcha-button" id="refresh_captcha_button"></i> <img id="captcha_image" src="/captcha"> </div>
            </div>
        </div>
        <div class="standard-button-group"> <input type="submit" class="btn btn-primary" value="UBAH KATA SANDI"> </div>
    </form>
</div>
<?= $this->endSection() ?>