<div class="provider-slide" id="arcade_providers"> <i class="glyphicon glyphicon-chevron-left left_trigger"></i>
    <main> <a href="/desktop/arcade/microgaming" class="slide-item">
            <picture>
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/MICROGAMING.webp?v=20231115" type="image/webp" />
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/MICROGAMING.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/MICROGAMING.png?v=20231115" />
            </picture>
            <picture>
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/MICROGAMING-active.webp?v=20231115" type="image/webp" />
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/MICROGAMING-active.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/MICROGAMING-active.png?v=20231115" />
            </picture>
        </a> <a href="/desktop/arcade/spinix" class="slide-item">
            <picture>
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/SPINIX.webp?v=20231115" type="image/webp" />
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/SPINIX.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/SPINIX.png?v=20231115" />
            </picture>
            <picture>
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/SPINIX-active.webp?v=20231115" type="image/webp" />
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/SPINIX-active.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/SPINIX-active.png?v=20231115" />
            </picture>
        </a> <a href="/desktop/arcade/spribe" class="slide-item">
            <picture>
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/SPRIBE.webp?v=20231115" type="image/webp" />
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/SPRIBE.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/SPRIBE.png?v=20231115" />
            </picture>
            <picture>
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/SPRIBE-active.webp?v=20231115" type="image/webp" />
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/SPRIBE-active.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/SPRIBE-active.png?v=20231115" />
            </picture>
        </a> <a href="/desktop/arcade/joker" class="slide-item">
            <picture>
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/JOKER.webp?v=20231115" type="image/webp" />
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/JOKER.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/JOKER.png?v=20231115" />
            </picture>
            <picture>
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/JOKER-active.webp?v=20231115" type="image/webp" />
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/JOKER-active.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/JOKER-active.png?v=20231115" />
            </picture>
        </a> <a href="/desktop/arcade/fachai" class="slide-item">
            <picture>
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/FACHAI.webp?v=20231115" type="image/webp" />
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/FACHAI.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/FACHAI.png?v=20231115" />
            </picture>
            <picture>
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/FACHAI-active.webp?v=20231115" type="image/webp" />
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/FACHAI-active.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/FACHAI-active.png?v=20231115" />
            </picture>
        </a> <a href="/desktop/arcade/jili" class="slide-item">
            <picture>
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/JILI.webp?v=20231115" type="image/webp" />
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/JILI.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/JILI.png?v=20231115" />
            </picture>
            <picture>
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/JILI-active.webp?v=20231115" type="image/webp" />
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/JILI-active.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/JILI-active.png?v=20231115" />
            </picture>
        </a> <a href="/desktop/arcade/amb-slot" class="slide-item">
            <picture>
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/AMB.webp?v=20231115" type="image/webp" />
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/AMB.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/AMB.png?v=20231115" />
            </picture>
            <picture>
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/AMB-active.webp?v=20231115" type="image/webp" />
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/AMB-active.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/AMB-active.png?v=20231115" />
            </picture>
        </a> <a href="/desktop/arcade/crowd-play" class="slide-item">
            <picture>
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/CROWDPLAY.webp?v=20231115" type="image/webp" />
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/CROWDPLAY.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/CROWDPLAY.png?v=20231115" />
            </picture>
            <picture>
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/CROWDPLAY-active.webp?v=20231115" type="image/webp" />
                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/CROWDPLAY-active.png?v=20231115" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/tabs/arcade/CROWDPLAY-active.png?v=20231115" />
            </picture>
        </a></main> <i class="glyphicon glyphicon-chevron-right right_trigger"></i>
</div>