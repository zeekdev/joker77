<?= $this->extend('layouts/mobile/slot') ?>

<?= $this->section('content') ?>
<div id="page_carousel" class="carousel slide page-carousel" data-ride="carousel" data-interval="5000">
    <ol class="carousel-indicators">
        <li class="active" data-target="#page_carousel" data-slide-to="0"></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/banners/casino/banner.jpg?v=20231115"> </div>
    </div>
</div>
<?= $this->include('components/mobile/menu-outer') ?>
<div class="game-list">
    <ul>
        <li> <a href="/mobile/casino/ion-casino">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/trg.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/trg.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/trg.png?v=20231115">
                </picture>
            </a> ION Casino </li>
        <li> <a href="/mobile/casino/pragmatic">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pplivecasino.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pplivecasino.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pplivecasino.png?v=20231115">
                </picture>
            </a> PP Casino </li>
        <li> <a href="/mobile/casino/mg-live">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/mglivecasino.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/mglivecasino.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/mglivecasino.png?v=20231115">
                </picture>
            </a> MG Live </li>
        <li> <a href="/mobile/casino/evo-gaming">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/evogaming.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/evogaming.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/evogaming.png?v=20231115">
                </picture>
            </a> Evo Gaming </li>
        <li> <a href="/mobile/casino/sexy-baccarat">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/sbosexybaccarat.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/sbosexybaccarat.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/sbosexybaccarat.png?v=20231115">
                </picture>
            </a> Sexy Baccarat </li>
        <li> <a href="/mobile/casino/pretty-gaming">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/prettygaming.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/prettygaming.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/prettygaming.png?v=20231115">
                </picture>
            </a> Pretty Gaming </li>
        <li> <a href="/mobile/casino/asia-gaming">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/ag.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/ag.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/ag.png?v=20231115">
                </picture>
            </a> Asia Gaming </li>
        <li> <a href="/mobile/casino/allbet">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/allbet.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/allbet.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/allbet.png?v=20231115">
                </picture>
            </a> AllBet </li>
        <li> <a href="/mobile/casino/pgs-live">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pgslive.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pgslive.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pgslive.png?v=20231115">
                </picture>
            </a> PGS Live </li>
        <li> <a href="/mobile/casino/sa-gaming">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/sagaming.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/sagaming.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/sagaming.png?v=20231115">
                </picture>
            </a> SA Gaming </li>
        
        
    </ul>
</div>

<?= $this->include('components/mobile/popular') ?>

<?= $this->endSection() ?>