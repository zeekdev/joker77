<?= $this->extend('layouts/mobile/promotion') ?>

<?= $this->section('content') ?>
<div data-container-background="general">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="info-center-container about-us-container standard-form-content">
                    <h2 class="info-center-content-title mb-4">Tentang <?= getenv('APP_NAME') ?></h2>
                    <h4>TENTANG KAMI</h4>
                    <div><strong><?= getenv('APP_NAME') ?></strong>
                        <p>Selamat datang di <strong><?= getenv('APP_NAME') ?></strong>, situs judi slot online terkemuka di Asia, yang menyediakan beragam produk permainan terbaik di Asia.</p>
                        <p><strong><?= getenv('APP_NAME') ?> </strong>adalah situs permainan taruhan olahraga hingga kasino terkemuka dan terpercaya di Asia, <strong><?= getenv('APP_NAME') ?></strong> menawarkan pengalaman judi online terbaik dengan berbagai variasi permainan kasino &amp; sportsbook yang dapat dipilih dengan odds paling kompetitif dalam dunia judi sepak bola. Kami menawarkan rata-rata 10.000 permainan Olahraga yang berbeda setiap bulan dan berbagai kompetisi di seluruh dunia dalam <strong><?= getenv('APP_NAME') ?> Sportsbook, sementara total lebih dari 100 permainan kasino dari variasi bakarat, slot, roulette dan permainan kasino lainnya dapat dimainkan di <strong><?= getenv('APP_NAME') ?> </strong>Casino.</strong></p>
                        <p>&nbsp;</p>
                        <p><strong>KEAMANAN</strong></p>
                        <p>Domain aman dan privat serta integritas produk kami adalah poros fundamental dari pengalaman taruhan online di <strong><?= getenv('APP_NAME') ?></strong>. Kami selalu mengutamakan keamanan tercanggih dan memperbaharui semua permainan serta proses-proses kami secara berkala, demi memastikan pengalaman permainan online Anda 100% aman dan adil. Kami selalu mengutamakan menjaga kerahasiaan informasi Anda, dan kami tidak akan pernah membagikannya ataupun menjualnya ke pihak ketiga, kecuali diharuskan sesuai dengan yang disebutkan di Kebijakan Privasi kami.</p>
                        <p>&nbsp;</p>
                        <p><strong>PELAYANAN PELANGGAN</strong></p>
                        <p>Didukung layanan pelanggan 24 jam, yang tersedia 7 hari seminggu, staf kami yang ramah dan profesional akan memastikan bahwa semua masalah yang Anda hadapi akan ditangani dengan cepat, efisien, dan secara ramah. Kami memberikan prioritas tinggi untuk memastikan sistem pembayaran yang aman dan memberikan kerahasiaan informasi pribadi.</p>
                        <p>Misi utama kami adalah selalu memberikan pengalaman taruhan online terbaik bagi pemain yang bertanggung jawab. Silahkan hubungi kami melalui Livechat dan Whatsapp, dengan saran dan komentar Anda.</p>
                        <p>Kami memilki beberapa metode pembayaran yang mudah dan aman, demi kenyamanan Anda. Kami mengikuti kebijakan kenali pelanggan Anda (KYC) dan anti-pencucian uang (AML), dan kami bekerja sama dengan badan keuangan serta badan pengatur demi memastikan ketaatan berstandar tertinggi pada hukum dan peraturan.</p>
                        <p>&nbsp;</p>
                        <p><strong>PROGRAM &amp; HIBURAN</strong></p>
                        <p>Di <strong><?= getenv('APP_NAME') ?></strong>, kami berusaha untuk memberikan yang terbaik dalam permainan dan layanan online untuk menawarkan pengalaman unik &amp; terbaik bagi pelanggan kami. Kami memiliki beberapa program berhadiah, yang memberikan pelanggan kami berbagai hadiah yang benar-benar layak untuk para pemain. Bermain di <strong><?= getenv('APP_NAME') ?> </strong>tidak hanya menyenangkan dan menghibur, tetapi juga sangat menguntungkan!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>