<div class="site-contacts">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="contact-list">
                    <li> <a href="https://api.whatsapp.com/send?phone=<?= getenv('NO_WA') ?>" target="_blank" rel="noopener nofollow"> <i> <img alt="Contact" loading="lazy" src="//nx-cdn.trgwl.com/Images/communications/whatsapp.svg?v=20231108-2" /> </i> +<?= getenv('NO_WA') ?> </a> </li>
                    <li> <a href="<?= getenv('LINK_TELEGRAM') ?>" target="_blank" rel="noopener nofollow"> <i> <img alt="Contact" loading="lazy" src="//nx-cdn.trgwl.com/Images/communications/telegram.svg?v=20231108-2" /> </i> CS<?= getenv('APP_NAME') ?> </a> </li>
                </ul>
            </div>
        </div>
    </div>
</div>