<?= $this->extend('layouts/mobile/promotion') ?>

<?= $this->section('content') ?>
<div class="site-content-container" data-container-background="general">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="info-center-container faq-container standard-form-content">
                    <h2 class="info-center-content-title">Pusat Bantuan</h2>
                    <h4>FAQs</h4>
                    <div class="blue_box">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="accordion" class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><a href="#collapseOne" data-toggle="collapse" data-parent="#accordion">1. Bagaimana Cara Membuat Akun Di <?= getenv('APP_NAME') ?>?</a></div>
                                        <div id="collapseOne" class="panel-collapse collapse ">
                                            <div class="panel-body">Cara membuat akun di <strong><?= getenv('APP_NAME') ?></strong> sangat mudah, silahkan klik tombol "<a href="../../desktop/register.aspx" target="_blank" rel="noopener">Daftar</a>" yang terletak di bagian kanan atas pada halaman utama <strong><?= getenv('APP_NAME') ?></strong>. Lalu isi semua informasi yang dibutuhkan secara tepat dan benar. Anda harus mengkonfirmasi bahwa anda setidaknya berusia 18 tahun untuk melengkapi pendaftaran anda.</div>
                                            <div class="panel-body">Kolom nama belakang anda tidak dapat dikosongkan. Jika anda tidak memiliki nama belakang (nama keluarga), maka anda dapat mengulang penulisan nama depan anda. <em>Cotoh: Andi Andi.</em></div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><a href="#collapseTwo" data-toggle="collapse" data-parent="#accordion">2. Mata Uang Apa Saja Yang Diterima Oleh 828BET?</a></div>
                                        <div id="collapseTwo" class="panel-collapse collapse">
                                            <div class="panel-body">Dibawah ini adalah daftar semua mata uang yang diterima oleh <strong><?= getenv('APP_NAME') ?></strong>:</div>
                                            <ol>
                                                <li>Rupiah (IDR).*</li>
                                            </ol>
                                            <div class="panel-body">
                                                <p><strong>Catatan:</strong></p>
                                                <p>1 unit Rupiah (IDR) di <strong><?= getenv('APP_NAME') ?></strong> akan mewakili Rp. 1.000.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><a href="#collapseThree" data-toggle="collapse" data-parent="#accordion">3. Bagaimana Cara Menyetor Dana Ke Akun <?= getenv('APP_NAME') ?> Saya?</a></div>
                                        <div id="collapseThree" class="panel-collapse collapse">
                                            <div class="panel-body">Sebelum anda dapat bertaruh, anda perlu melakukan deposit dana ke akun anda menggunakan salah satu opsi deposit berikut ini:</div>
                                            <ol>
                                                <li>Bank Lokal.</li>
                                                <li>Online Deposit.</li>
                                            </ol>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><a href="#collapseFour" data-toggle="collapse" data-parent="#accordion">4. Adakah batasan minimal dalam melakukan betting?</a></div>
                                        <div id="collapseFour" class="panel-collapse collapse">
                                            <div class="panel-body">Setiap permainan yang disediakan oleh <strong><?= getenv('APP_NAME') ?></strong> memiliki taruhan minimal sesuai permainan yang dipilih. Semua informasi tersedia ketika member sudah masuk ke dalam permainan.</div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><a href="#collapseFive" data-toggle="collapse" data-parent="#accordion">5. Apakah situs kami terpercaya?</a></div>
                                        <div id="collapseFive" class="panel-collapse collapse">
                                            <div class="panel-body">Situs kami merupakan situs resmi yang berkantor di Manila, Filipina dimana sebelum kami beroperasi kami sudah mendapatkan lisensi PACGOR sebagai tanda bahwa kami web terpercaya. Seluruh transaksi data keuangan dan privasi anda menjadi salah satu prioritas kami dan tidak ada satupun member kami yang tidak dibayar kemenangannya.</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>