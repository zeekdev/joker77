<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateUserTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'email' => [
                'type'       => 'VARCHAR',
                'constraint' => '100'
            ],
            'username' => [
                'type'       => 'VARCHAR',
                'constraint' => '100'
            ],
            'password' => [
                'type'       => 'VARCHAR',
                'constraint' => '100'
            ],
            'created_at' => [
                'type'       => 'DATETIME'
            ]
        ]);
        $this->forge->addKey('id', true, true);
        $this->forge->createTable('users');
    }

    public function down()
    {
        $this->forge->dropTable('users');
    }
}
