<div class="navbar navbar-fixed-top">
    <?php if(session()->has('logged_in') && session()->get('logged_in')): ?>
    <?= $this->include('components/topbar-logged'); ?>
    <?php else: ?>
    <?= $this->include('components/topbar'); ?>
    <?php endif; ?>
    <div class="site-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="site-header-inner-container"> <a href="/desktop/home" class="logo"> <img loading="lazy" src="<?= getenv('APP_LOGO') ?>" /> </a>
                        <ul class="top-menu">
                            <li data-active="false"> <a href="/desktop/hot-games"> Hot Games </a>
                                <div class="game-list-container">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="games-container">
                                                    <li> <a href="/desktop/slots/pragmatic">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pp.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pp.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pp.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="javascript:registerPopup({ content:&#39;<?= session()->has('logged_in') ? 'Saldo anda tidak cukup' : 'Silahkan login terlebih dahulu' ?>.&#39; });">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/balak4d.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/balak4d.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/balak4d.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/slots/microgaming">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/microgaming.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/microgaming.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/microgaming.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/slots/habanero">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/habanero.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/habanero.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/habanero.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/slots/pgsoft">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pgsoft.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pgsoft.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pgsoft.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/slots/jili">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/jili.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/jili.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/jili.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/slots/reel-kingdom">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/reelkingdom.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/reelkingdom.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/reelkingdom.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="javascript:registerPopup({ content:&#39;<?= session()->has('logged_in') ? 'Saldo anda tidak cukup' : 'Silahkan login terlebih dahulu' ?>.&#39; });">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/trg.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/trg.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/trg.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="javascript:registerPopup({ content:&#39;<?= session()->has('logged_in') ? 'Saldo anda tidak cukup' : 'Silahkan login terlebih dahulu' ?>.&#39; });">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/g8poker.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/g8poker.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/g8poker.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li data-active="false"> <a href="/desktop/slots"> Slots <i class="glyphicon glyphicon-chevron-down"></i> </a>
                                <div class="game-list-container">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="games-container">
                                                    <li> <a href="/desktop/slots/pragmatic">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pp.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pp.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pp.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/slots/microgaming">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/microgaming.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/microgaming.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/microgaming.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/slots/pgsoft">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pgsoft.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pgsoft.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pgsoft.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/slots/reel-kingdom">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/reelkingdom.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/reelkingdom.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/reelkingdom.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/slots/advantplay">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/advantplay.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/advantplay.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/advantplay.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/slots/no-limit-city">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/nolimitcity.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/nolimitcity.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/nolimitcity.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/slots/habanero">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/habanero.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/habanero.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/habanero.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/slots/joker">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/joker.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/joker.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/joker.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/slots/jili">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/jili.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/jili.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/jili.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/slots/spinix">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/spinix.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/spinix.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/spinix.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/slots/crowd-play">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/crowdplay.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/crowdplay.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/crowdplay.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/slots/live22">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/live22.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/live22.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/live22.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/slots/playstar">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/playstar.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/playstar.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/playstar.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/slots/vpower">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/vpower.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/vpower.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/vpower.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li data-active="false"> <a href="/desktop/casino"> Live Casino <i class="glyphicon glyphicon-chevron-down"></i> </a>
                                <div class="game-list-container">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="games-container">
                                                    <li> <a href="javascript:registerPopup({ content:&#39;<?= session()->has('logged_in') ? 'Saldo anda tidak cukup' : 'Silahkan login terlebih dahulu' ?>.&#39; });">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/trg.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/trg.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/trg.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/casino/pragmatic">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pplivecasino.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pplivecasino.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pplivecasino.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="javascript:registerPopup({ content:&#39;<?= session()->has('logged_in') ? 'Saldo anda tidak cukup' : 'Silahkan login terlebih dahulu' ?>.&#39; });">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/mglivecasino.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/mglivecasino.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/mglivecasino.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/casino/evo-gaming">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/evogaming.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/evogaming.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/evogaming.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="javascript:registerPopup({ content:&#39;<?= session()->has('logged_in') ? 'Saldo anda tidak cukup' : 'Silahkan login terlebih dahulu' ?>.&#39; });">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/sbosexybaccarat.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/sbosexybaccarat.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/sbosexybaccarat.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="javascript:registerPopup({ content:&#39;<?= session()->has('logged_in') ? 'Saldo anda tidak cukup' : 'Silahkan login terlebih dahulu' ?>.&#39; });">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/prettygaming.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/prettygaming.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/prettygaming.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="javascript:registerPopup({ content:&#39;<?= session()->has('logged_in') ? 'Saldo anda tidak cukup' : 'Silahkan login terlebih dahulu' ?>.&#39; });">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/ag.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/ag.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/ag.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="javascript:registerPopup({ content:&#39;<?= session()->has('logged_in') ? 'Saldo anda tidak cukup' : 'Silahkan login terlebih dahulu' ?>.&#39; });">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/allbet.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/allbet.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/allbet.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="javascript:registerPopup({ content:&#39;<?= session()->has('logged_in') ? 'Saldo anda tidak cukup' : 'Silahkan login terlebih dahulu' ?>.&#39; });">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pgslive.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pgslive.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pgslive.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="javascript:registerPopup({ content:&#39;<?= session()->has('logged_in') ? 'Saldo anda tidak cukup' : 'Silahkan login terlebih dahulu' ?>.&#39; });">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/sagaming.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/sagaming.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/sagaming.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li data-active="false"> <a href="/desktop/others"> Togel <i class="glyphicon glyphicon-chevron-down"></i> </a>
                                <div class="game-list-container">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="games-container">
                                                    <li> <a href="javascript:registerPopup({ content:&#39;<?= session()->has('logged_in') ? 'Saldo anda tidak cukup' : 'Silahkan login terlebih dahulu' ?>.&#39; });">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/balak4d.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/balak4d.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/balak4d.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li data-active="false"> <a href="/desktop/crash-game"> Crash Game <i class="glyphicon glyphicon-chevron-down"></i> </a>
                                <div class="game-list-container">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="games-container">
                                                    <li> <a href="/desktop/crash-game/pragmatic">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pplivecasinocrash.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pplivecasinocrash.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/pplivecasinocrash.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/crash-game/spribe">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/spribecrash.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/spribecrash.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/spribecrash.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/crash-game/microgaming">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/microgamingcrash.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/microgamingcrash.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/microgamingcrash.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/crash-game/spinix">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/spinixcrash.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/spinixcrash.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/spinixcrash.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/crash-game/advantplay-mini-game">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/advantplayminigamecrash.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/advantplayminigamecrash.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/advantplayminigamecrash.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/crash-game/joker">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/jokercrash.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/jokercrash.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/jokercrash.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/crash-game/dragoonsoft">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/dragoonsoftcrash.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/dragoonsoftcrash.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/dragoonsoftcrash.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/crash-game/funky-games">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/sbofunkygamecrash.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/sbofunkygamecrash.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/sbofunkygamecrash.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li data-active="false"> <a href="/desktop/arcade"> Arcade <i class="glyphicon glyphicon-chevron-down"></i> </a>
                                <div class="game-list-container">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="games-container">
                                                    <li> <a href="/desktop/arcade/microgaming">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/microgamingfishing.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/microgamingfishing.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/microgamingfishing.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/arcade/spinix">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/spinixfishing.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/spinixfishing.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/spinixfishing.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/arcade/spribe">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/spribe.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/spribe.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/spribe.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/arcade/joker">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/jokerfishing.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/jokerfishing.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/jokerfishing.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/arcade/fachai">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/fachaifishing.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/fachaifishing.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/fachaifishing.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="/desktop/arcade/jili">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/jilifishing.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/jilifishing.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/jilifishing.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li data-active="false"> <a href="/desktop/poker"> Poker <i class="glyphicon glyphicon-chevron-down"></i> </a>
                                <div class="game-list-container">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="games-container">
                                                    <li> <a href="javascript:registerPopup({ content:&#39;<?= session()->has('logged_in') ? 'Saldo anda tidak cukup' : 'Silahkan login terlebih dahulu' ?>.&#39; });">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/g8poker.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/g8poker.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/g8poker.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                    <li> <a href="javascript:registerPopup({ content:&#39;<?= session()->has('logged_in') ? 'Saldo anda tidak cukup' : 'Silahkan login terlebih dahulu' ?>.&#39; });">
                                                            <picture>
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/onepoker.webp?v=20231108-2" type="image/webp" />
                                                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/onepoker.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/providers/shortcuts/onepoker.png?v=20231108-2" />
                                                            </picture>
                                                        </a> </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li data-active="false"> <a href="/desktop/promotion"> Promosi </a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>