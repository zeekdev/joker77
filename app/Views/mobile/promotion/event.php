<?= $this->extend('layouts/mobile/promotion') ?>

<?= $this->section('content') ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="top-tab-container promotion"> <a href="/mobile/promotion" data-active="false"> Semua Promosi </a> <a href="/mobile/provider-event" data-active="true"> Semua Event Provider </a> </div>
            <div class="promotion-selection"> <a href="/mobile/provider-event" data-active="false"> Semua Event Provider </a> <a href="/mobile/provider-event/event" data-active="true"> Event </a> </div>
            <div class="promotion-list">
                <div class="promotion-item promotion_item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_bd646d1d-3ffb-4a50-9e5e-11983b995a1b_1701065916793.png">
                    <div class="button-container">
                        <div class="promotion-label">
                            <h2>Daily Wins - MEGA GACOR LEVEL 6 TOURNAMENT [LIVE CASINO]</h2>
                            <h3>Tanggal akhir: <span>-</span></h3>
                        </div> <a href="javascript:registerPopup({ content:&#39;Silahkan login terlebih dahulu.&#39; });" class="click-for-more-info-button"> Info Detail </a>
                    </div>
                </div>
                <div class="promotion-item promotion_item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_e58e6428-f0cf-451f-82f7-b43565020628_1701063681187.png">
                    <div class="button-container">
                        <div class="promotion-label">
                            <h2>Daily Wins - MEGA GACOR LEVEL 6 TOURNAMENT [SLOTS]</h2>
                            <h3>Tanggal akhir: <span>-</span></h3>
                        </div> <a href="javascript:registerPopup({ content:&#39;Silahkan login terlebih dahulu.&#39; });" class="click-for-more-info-button"> Info Detail </a>
                    </div>
                </div>
                <div class="promotion-item promotion_item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_17cba795-55a3-4d78-ac51-f8965622d240_1701068706990.jpg">
                    <div class="button-container">
                        <div class="promotion-label">
                            <h2>PRAGMATIC PLAY- HUTAN KEMENANGAN NEXUS TOURNAMENT &amp; CASHDROP</h2>
                            <h3>Tanggal akhir: <span>-</span></h3>
                        </div> <a href="javascript:registerPopup({ content:&#39;Silahkan login terlebih dahulu.&#39; });" class="click-for-more-info-button"> Info Detail </a>
                    </div>
                </div>
                <div class="promotion-item promotion_item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_9d21b497-8b7b-49cf-b0d2-fa16a807bf71_1701244205319.png">
                    <div class="button-container">
                        <div class="promotion-label">
                            <h2>PRAGMATIC PLAY - SUGAR RUSH XMAS &amp; CANDY JAR CLUSTERS DAILY CASHDROP</h2>
                            <h3>Tanggal akhir: <span>-</span></h3>
                        </div> <a href="javascript:registerPopup({ content:&#39;Silahkan login terlebih dahulu.&#39; });" class="click-for-more-info-button"> Info Detail </a>
                    </div>
                </div>
                <div class="promotion-item promotion_item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_b3cbd597-a680-4c7e-b3b9-bc08bc029869_1698724489140.gif">
                    <div class="button-container">
                        <div class="promotion-label">
                            <h2>MICROGAMING - Nexus Community Jackpot</h2>
                            <h3>Tanggal akhir: <span>-</span></h3>
                        </div> <a href="javascript:registerPopup({ content:&#39;Silahkan login terlebih dahulu.&#39; });" class="click-for-more-info-button"> Info Detail </a>
                    </div>
                </div>
                <div class="promotion-item promotion_item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_1f18e8e3-aa4b-4740-ab56-7d970ef746cd_1701244205506.jpg">
                    <div class="button-container">
                        <div class="promotion-label">
                            <h2>Cash Blitz Bonanza Turnamen &amp; Cashdrop</h2>
                            <h3>Tanggal akhir: <span>-</span></h3>
                        </div> <a href="javascript:registerPopup({ content:&#39;Silahkan login terlebih dahulu.&#39; });" class="click-for-more-info-button"> Info Detail </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>