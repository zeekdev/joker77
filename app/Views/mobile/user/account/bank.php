<?= $this->extend('layouts/mobile/profile') ?>

<?= $this->section('content') ?>
<div class="standard-form-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="tab-menu-background-container">
                    <div class="tab-menu-container" data-style="vertical"> <a href="/mobile/account-summary" data-active="false"> <i data-icon="profile" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/profile.svg?v=20231205-1);--active-image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/profile-active.svg?v=20231205-1);"></i> Akun Saya </a> <a href="/mobile/password" data-active="false"> <i data-icon="password" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/password.svg?v=20231205-1);--active-image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/password-active.svg?v=20231205-1);"></i> Ubah Kata Sandi </a> </div>
                </div>
                <div class="form-group form-group-link-container"> <a href="/mobile/deposit">Deposit</a> <a href="/mobile/withdrawal">Penarikan</a> </div>
                <?php if(session()->has('msg')): ?>
                <div class="alert alert-success">
                    Akun perbankan berhasil ditambahkan.
                </div>
                <?php endif; ?>
                <div class="standard-form-title"> Informasi Detail Perbankan </div>
                <form action="/Profile/BankAccount" id="form0" method="post" novalidate="novalidate"><input name="__RequestVerificationToken" type="hidden" value="1f38q1OR1R5pNfB72CxNjCro5mFJS1Ns7hhIbXOSoSlKpdvMkjROmmUavfvt8DgOtreAy9BAeMvwlrAx_0u2CTQJeGKSsbAudzTVzk5Qh-w1">
                    <div class="form-group"> <label for="PaymentType">Tipe Pembayaran</label> <select class="form-control valid" data-val="true" data-val-required="The PaymentType field is required." id="payment_method_select" name="PaymentType">
                            <option selected="selected" value="BANK">Bank</option>
                        </select> <span class="standard-required-message">Mohon pilih jenis pembayaran.</span> </div>
                    <div class="form-group"> <label for="Bank">Akun</label> <select class="form-control" data-val="true" data-val-required="The Bank field is required." id="Bank" name="Bank">
                            <option value="">-- Pilih Akun --</option>
                            <option value="BSI">BANK BSI</option>
                            <option value="BCA">BCA</option>
                            <option value="BNI">BNI</option>
                            <option value="BRI">BRI</option>
                            <option value="CIMB">CIMB</option>
                            <option value="DANA">DANA</option>
                            <option value="GOPAY">GOPAY</option>
                            <option value="MANDIRI">MANDIRI</option>
                            <option value="MAYBANK">MAYBANK</option>
                            <option value="OVO">OVO</option>
                        </select> <span class="standard-required-message">Silahkan pilih bank!</span> </div>
                    <div class="form-group"> <label for="Branch">Cabang</label> <input class="form-control" id="Branch" name="Branch" placeholder="Cabang bank anda sesuai dengan buku tabungan" type="text" value=""> </div>
                    <div class="form-group"> <label for="AccountName">Nama Rekening</label> <input class="form-control" data-val="true" data-val-regex="The field AccountName must match the regular expression '^[0-9a-zA-Z ]*$'." data-val-regex-pattern="^[0-9a-zA-Z ]*$" data-val-required="The AccountName field is required." id="AccountName" name="AccountName" placeholder="Nama lengkap anda sesuai dengan buku tabungan" type="text" value="<?= $banks[0]['nama_rekening'] ?>"> <span class="standard-required-message">Nama rekening harus diisi.</span> </div>
                    <div class="form-group"> <label for="AccountNo">Nomor Rekening</label> <input maxlength="20" autocomplete="off" class="form-control" data-val="true" data-val-regex="The field AccountNo must match the regular expression '^[0-9]+$'." data-val-regex-pattern="^[0-9]+$" data-val-required="The AccountNo field is required." id="AccountNo" name="AccountNo" placeholder="Nomor rekening anda" type="text" value=""> <span class="standard-required-message">Rekening bank harus diisi. (Hanya nomor)</span> </div>
                    <div class="form-group"> <label for="VerificationCode">Kode Verifikasi</label>
                        <div data-section="input" class="captcha-input"> <input autocomplete="off" class="form-control" data-val="true" data-val-required="The VerificationCode field is required." id="VerificationCode" name="VerificationCode" placeholder="Validasi" type="text" value=""> <span class="standard-required-message">Silakan masukkan captcha!</span>
                            <div class="captcha-container"> <img id="captcha_image" src="/captcha"> <i class="glyphicon glyphicon-refresh refresh-captcha-button" id="refresh_captcha_button"></i> </div>
                        </div>
                    </div>
                    <div class="standard-button-group"> <input type="submit" class="standard-secondary-button" value="Tambah Akun"> </div>
                </form>
                <?php foreach ($banks as $key => $value) { ?>
                    <div class="bank-account-item">
                    <table>
                        <tbody>
                            <tr>
                                <td>Akun</td>
                                <td>:</td>
                                <td><?= $value['bank'] ?></td>
                            </tr>
                            <tr>
                                <td>Nama Akun</td>
                                <td>:</td>
                                <td><?= $value['nama_rekening'] ?></td>
                            </tr>
                            <tr>
                                <td>Nomor Akun</td>
                                <td>:</td>
                                <td>********<?= substr($value['norek'], 6) ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
               <?php } ?>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>