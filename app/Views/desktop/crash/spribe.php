<?= $this->extend('layouts/desktop/crash') ?>

<?= $this->section('content') ?>
<?= view('components/crash-container', ['title' => 'spribe']) ?>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
<script>
    window.addEventListener('DOMContentLoaded', () => {
        initializeCrashGameGames({
            directoryPath: '//nx-cdn.trgwl.com/Images/providers/',
            provider: 'SPRIBE',
            translations: {
                playNow: 'MAIN',
                demo: 'COBA'
            }
        });
    });
</script>
<?= $this->endSection() ?>    