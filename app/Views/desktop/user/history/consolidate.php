<?= $this->extend('layouts/desktop/history') ?>

<?= $this->section('content') ?>
<div class="standard-form-content">
    <div class="standard-nav-bar reporting-nav-bar">
        <div class="nav-bar-links"> <a href="/desktop/history/deposit" data-active="false"> DEPOSIT </a> <a href="/desktop/history/withdrawal" data-active="false"> PENARIKAN </a> <a href="/desktop/statement/consolidate" data-active="true"> RIWAYAT TARUHAN </a> </div>
    </div>
    <form action="/desktop/statement/consolidate" method="post">
        <div class="standard-reporting-control-group">
            <div class="form-group"> <label>Date Range</label> <input type="text" class="form-control" id="date_range_picker" data-picker="date-range" data-separator="Sampai"> <input type="hidden" name="StartingDate" id="starting_date" value="2023-12-07"> <input type="hidden" name="EndingDate" id="ending_date" value="2023-12-07"> </div>
            <div class="form-group"> <label>Tipe</label> <select class="form-control" id="available_game_types" name="GameType">
                    <option value="Unknown"> Semua </option>
                    <option value="Arcade"> Arcade </option>
                    <option value="Casino"> Live Casino </option>
                    <option value="CrashGame"> Crash Game </option>
                    <option value="ESports"> E-Sports </option>
                    <option value="Lottery"> Togel </option>
                    <option value="Multi"> Poker </option>
                    <option value="Slots"> Slots </option>
                    <option value="Sports"> Olahraga </option>
                </select> </div>
            <div class="form-group"> <label>Provider</label> <select class="form-control" id="available_games" name="Game" disabled="">
                    <option value="Unknown">Semua</option>
                </select> </div> <button type="submit" class="btn btn-primary"> Search </button>
        </div>
        <div class="standard-reporting-scroll-container">
            <table class="table grid_table">
                <thead>
                    <tr>
                        <th scope="col"> </th>
                        <th scope="col"> Tanggal(GMT+7) </th>
                        <th scope="col"> Nomor </th>
                        <th scope="col"> Total Taruhan </th>
                        <th scope="col"> Total Turnover </th>
                        <th scope="col"> Total Win/Lose </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</div>
<?= $this->endSection() ?>