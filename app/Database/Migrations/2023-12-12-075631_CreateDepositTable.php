<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateDepositTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'code' => [
                'type'       => 'VARCHAR',
                'constraint' => '100'
            ],
            'from' => [
                'type'       => 'VARCHAR',
                'constraint' => '100'
            ],
            'target' => [
                'type'       => 'VARCHAR',
                'constraint' => '100'
            ],
            'amount' => [
                'type'       => 'VARCHAR',
                'constraint' => '100'
            ],
            'user_id' => [
                'type'       => 'INTEGER'
            ],
            'created_at' => [
                'type'       => 'DATETIME'
            ]
        ]);
        $this->forge->addKey('id', true, true);
        $this->forge->createTable('deposits');
    }

    public function down()
    {
        $this->forge->dropTable('deposits');
    }
}
