<div class="topbar-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 topbar-inner-container">
                <div class="topbar-left-section">
                    <div class="topbar-item language-selector-container" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/flags.png?v=20231108-2);">
                        <div id="language_selector_trigger" data-toggle="dropdown" class="language-selector-trigger" data-language="id"> <i data-language="id"></i> </div>
                        <ul class="dropdown-menu language-selector">
                            <li class="language_selector" data-language="en"> <i data-language="en"></i>
                                <div class="language-name">
                                    <div>ENGLISH</div>
                                    <div>ENGLISH</div>
                                </div>
                            </li>
                            <li class="language_selector" data-language="id"> <i data-language="id"></i>
                                <div class="language-name">
                                    <div>BHS INDONESIA</div>
                                    <div>INDONESIAN</div>
                                </div>
                            </li>
                            <li class="language_selector" data-language="kr"> <i data-language="kr"></i>
                                <div class="language-name">
                                    <div>한국어</div>
                                    <div>KOREAN</div>
                                </div>
                            </li>
                            <li class="language_selector" data-language="cn"> <i data-language="cn"></i>
                                <div class="language-name">
                                    <div>中文</div>
                                    <div>CHINESE</div>
                                </div>
                            </li>
                            <li class="language_selector" data-language="jp"> <i data-language="jp"></i>
                                <div class="language-name">
                                    <div>日本語</div>
                                    <div>JAPANESE</div>
                                </div>
                            </li>
                            <li class="language_selector" data-language="th"> <i data-language="th"></i>
                                <div class="language-name">
                                    <div>ไทย</div>
                                    <div>THAI</div>
                                </div>
                            </li>
                            <li class="language_selector" data-language="my"> <i data-language="my"></i>
                                <div class="language-name">
                                    <div>မြန်မာစာ</div>
                                    <div>BURMESE</div>
                                </div>
                            </li>
                            <li class="language_selector" data-language="kh"> <i data-language="kh"></i>
                                <div class="language-name">
                                    <div>ខេមរភាសា</div>
                                    <div>KHMER</div>
                                </div>
                            </li>
                            <li class="language_selector" data-language="hi"> <i data-language="hi"></i>
                                <div class="language-name">
                                    <div>हिन्दी</div>
                                    <div>HINDI</div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="topbar-item"> <a href="javascript:void(0)" class="js_live_chat_link"> <i data-icon="live-chat" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/live-chat.svg?v=20231108-2);"></i> Live Chat </a> </div>
                    <div class="topbar-item"> <a href="/mobile/home" rel="nofollow"> <i data-icon="mobile" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/mobile.svg?v=20231108-2);"></i> Versi Mobile </a> </div>
                </div>
                <form action="/Account/Login" method="post"><input name="__RequestVerificationToken" type="hidden" value="QDs7OKFPuctko49HU-DT3r3PNuhH9eTmLGbEOa3u4YVC1SdE0LQdFF3ZwoMj7vv--0QBbBrLoNptDb2-acFf5dpR1kDsyrlRcR46JE45FLI1" />
                    <div class="login-panel">
                        <div class="login-panel-item"> <label> <input type="text" name="Username" placeholder="Nama Pengguna" /> </label> </div>
                        <div class="login-panel-item"> <label> <span class="standard-password-field"> <input type="password" name="Password" placeholder="Kata Sandi" id="password_input" /> <i class="glyphicon glyphicon-eye-close" id="password_input_trigger"></i> </span> </label> </div>
                        <div class="login-panel-item"> <input type="submit" class="login-button" value="Masuk" /> </div>
                        <div class="login-panel-item"> <a href="/#" class="register-button" data-toggle="modal" data-target="#register_modal"> Daftar </a> </div> <a href="/#" class="forgot-password-link" data-toggle="modal" data-target="#forgot_password_modal"> Lupa Kata Sandi </a>
                    </div>
                </form>
                <script>
                    window.addEventListener('DOMContentLoaded', () => {
                        const passwordInputTrigger = document.querySelector('#password_input_trigger');
                        const passwordInput = document.querySelector('#password_input');
                    
                        passwordInputTrigger.onclick = () => {
                            if (passwordInput.type === 'password') {
                                passwordInput.type = 'text';
                    
                                return;
                            }
                    
                            passwordInput.type = 'password';
                        };
                    });
                </script>
            </div>
        </div>
    </div>
</div>