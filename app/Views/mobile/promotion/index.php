<?= $this->extend('layouts/mobile/promotion') ?>

<?= $this->section('content') ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="top-tab-container promotion"> <a href="/mobile/promotion" data-active="true"> Semua Promosi </a> <a href="/mobile/provider-event" data-active="false"> Semua Event Provider </a> </div>
            <div class="promotion-selection"> <a href="/mobile/promotion" data-active="true"> Semua Promosi </a> <a href="/mobile/promotion/new-member" data-active="false"> Member Baru </a> <a href="/mobile/promotion/special" data-active="false"> Spesial </a></div>
            <div class="promotion-list">
                <div class="promotion-item promotion_item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_75287e9f-fc07-44fe-82a0-12e507d46721_1698341365933.png">
                    <div class="button-container">
                        <div class="promotion-label">
                            <h2>HADIAH UANG TUNAI 350 JUTA</h2>
                            <h3>Tanggal akhir: <span>-</span></h3>
                        </div> <a href="javascript:registerPopup({ content:&#39;Silahkan login terlebih dahulu.&#39; });" class="click-for-more-info-button"> Info Detail </a>
                    </div>
                </div>
                <div class="promotion-item promotion_item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_59908d36-0c1f-48a9-a9d0-1a4be0665bb0_1698327402080.png">
                    <div class="button-container">
                        <div class="promotion-label">
                            <h2>Bonus Depan Deposit Harian Slot Game 10% Tiap Harinya</h2>
                            <h3>Tanggal akhir: <span>2052-12-02</span></h3>
                        </div> <a href="javascript:registerPopup({ content:&#39;Silahkan login terlebih dahulu.&#39; });" class="click-for-more-info-button"> Info Detail </a>
                    </div>
                </div>
                <div class="promotion-item promotion_item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_ac9ca828-06f5-4ba1-84b8-4fea9b52f5d5_1698327440910.png">
                    <div class="button-container">
                        <div class="promotion-label">
                            <h2>deposit pulsa tanpa potongan</h2>
                            <h3>Tanggal akhir: <span>-</span></h3>
                        </div> <a href="javascript:registerPopup({ content:&#39;Silahkan login terlebih dahulu.&#39; });" class="click-for-more-info-button"> Info Detail </a>
                    </div>
                </div>
                <div class="promotion-item promotion_item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_54740598-1654-43d9-9fee-a79812587b10_1698327467457.png">
                    <div class="button-container">
                        <div class="promotion-label">
                            <h2>BONUS WELCOME 100% UNTUK SLOT GAME</h2>
                            <h3>Tanggal akhir: <span>2026-01-24</span></h3>
                        </div> <a href="javascript:registerPopup({ content:&#39;Silahkan login terlebih dahulu.&#39; });" class="click-for-more-info-button"> Info Detail </a>
                    </div>
                </div>
                <div class="promotion-item promotion_item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_50ed0933-0fb4-4c3a-85be-11ea04bd75ea_1698327491690.png">
                    <div class="button-container">
                        <div class="promotion-label">
                            <h2>CASHBACK SPORTSBOOK 10% SETIAP MINGGUNYA</h2>
                            <h3>Tanggal akhir: <span>2029-06-01</span></h3>
                        </div> <a href="javascript:registerPopup({ content:&#39;Silahkan login terlebih dahulu.&#39; });" class="click-for-more-info-button"> Info Detail </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>