<!DOCTYPE html>
<html lang="id">
<head>
	<meta charset="UTF-8">
	<title>SITUS TELAH DITUTUP PAKSA</title>
	<link rel="shortcut icon" type="image/png" href="https://patrolisiber.id/img/logo.png"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<style>
		body {
			background-color: #000;
			color: white;
			text-align: center;
			font-family: sans-serif;
		}

		img	{
			margin-top: 60pt;
			filter: drop-shadow(0 0 0.75rem crimson);
		}

		h1 {
			margin-top: 40pt;
			color: red;
		}

		p {
			font-size: 18px;
		}
	</style>
</head>
<body>
	<img src="/js/personal/ci.png" alt="Cyberpolice Indonesia">
	<h1>SITUS DALAM PENGAWASAN POLISI SIBER INDONESIA</h1>
	<p>Segala bentuk aktifitas serta keberadaan pemilik situs ini sedang digali oleh pihak kepolisian Indonesia.</p>
</body>
</html>