<?= $this->extend('layouts/mobile/slot') ?>

<?= $this->section('content') ?>
<div id="page_carousel" class="carousel slide page-carousel" data-ride="carousel" data-interval="5000">
    <ol class="carousel-indicators">
        <li class="active" data-target="#page_carousel" data-slide-to="0"></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/banners/crash-game/banner.jpg?v=20231115"> </div>
    </div>
</div>
<?= $this->include('components/mobile/menu-outer') ?>
<div class="game-list">
    <ul>
        <li> <a href="/mobile/crash-game/pragmatic">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pplivecasinocrash.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pplivecasinocrash.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pplivecasinocrash.png?v=20231115">
                </picture>
            </a> PP Casino </li>
        <li> <a href="/mobile/crash-game/spribe">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/spribecrash.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/spribecrash.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/spribecrash.png?v=20231115">
                </picture>
            </a> Spribe </li>
        <li> <a href="/mobile/crash-game/microgaming">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/microgamingcrash.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/microgamingcrash.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/microgamingcrash.png?v=20231115">
                </picture>
            </a> MicroGaming </li>
        <li> <a href="/mobile/crash-game/spinix">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/spinixcrash.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/spinixcrash.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/spinixcrash.png?v=20231115">
                </picture>
            </a> Spinix </li>
        <li> <a href="/mobile/crash-game/advantplay-mini-game">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/advantplayminigamecrash.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/advantplayminigamecrash.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/advantplayminigamecrash.png?v=20231115">
                </picture>
            </a> AdvantPlay Mini Game </li>
        <li> <a href="/mobile/crash-game/joker">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/jokercrash.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/jokercrash.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/jokercrash.png?v=20231115">
                </picture>
            </a> Joker </li>
        <li> <a href="/mobile/crash-game/dragoonsoft">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/dragoonsoftcrash.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/dragoonsoftcrash.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/dragoonsoftcrash.png?v=20231115">
                </picture>
            </a> Dragoonsoft </li>
        <li> <a href="/mobile/crash-game/funky-games">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/sbofunkygamecrash.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/sbofunkygamecrash.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/sbofunkygamecrash.png?v=20231115">
                </picture>
            </a> Funky Games </li>
    </ul>
</div>

<?= $this->include('components/mobile/popular') ?>

<?= $this->endSection() ?>