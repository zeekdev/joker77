<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Casino extends BaseController
{
    public function index($provider)
    {
        return view('desktop/casino/'.$provider);
    }

    public function mobile($provider)
    {
        return view('mobile/casino/'.$provider);
    }

    public function list($provider)
    {
        $json = APPPATH . 'Controllers/json/casino/'.$provider.'.json';
        
        if($this->isAuth()) {
            $json = APPPATH . 'Controllers/json/casino/auth/'.$provider.'.json';
        }

        $data = file_get_contents($json);
        return $this->response->setJSON($data);
    }
}
