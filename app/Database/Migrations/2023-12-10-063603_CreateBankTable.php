<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateBankTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'bank' => [
                'type'       => 'VARCHAR',
                'constraint' => '100'
            ],
            'nama_rekening' => [
                'type'       => 'VARCHAR',
                'constraint' => '100'
            ],
            'norek' => [
                'type'       => 'VARCHAR',
                'constraint' => '100'
            ],
            'user_id' => [
                'type'       => 'INTEGER'
            ]
        ]);
        $this->forge->addKey('id', true, true);
        $this->forge->createTable('banks');
    }

    public function down()
    {
        $this->forge->dropTable('banks');
    }
}
