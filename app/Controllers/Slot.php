<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Slot extends BaseController
{
    public function index($provider)
    {
        return view('desktop/slots/'.$provider);
    }

    public function mobile($provider)
    {
        return view('mobile/slots/'.$provider);
    }

    public function list($provider)
    {
        $json = APPPATH . 'Controllers/json/slots/'.$provider.'.json';

        if($this->isAuth()) {
            $json = APPPATH . 'Controllers/json/slots/auth/'.$provider.'.json';
        }

        $data = file_get_contents($json);
        return $this->response->setJSON($data);
    }
}
