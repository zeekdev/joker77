<?= $this->extend('layouts/desktop/promotion') ?>

<?= $this->section('content') ?>
<div class="site-content-container" data-container-background="promotions">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="standard-container-with-sidebar">
                    <div class="promotion-side-menu" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/promotions/category-sprite.png?v=20231129-1);">
                        <h5> Category </h5>
                        <div class="top-tab-container promotion"> <a href="/desktop/promotion" data-active="true"> Semua Promosi </a> <a href="/desktop/provider-event" data-active="false"> Semua Event Provider </a> </div> <a href="/desktop/promotion" data-active="false"> <i data-icon="all"></i> Semua Promosi </a> <a href="/desktop/promotion/new-member" data-active="true"> <i data-icon="new-member"></i> Member Baru </a> <a href="/desktop/promotion/special" data-active="false"> <i data-icon="special"></i> Spesial </a>
                    </div>
                    <div class="promotion-list">
                        <div class="promotion-item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_a5bc578c-bbe6-4672-93f5-b39f93d2b086_1698327505470.png">
                            <div class="promotion-footer">
                                <div class="promotion-label">
                                    <h2>BONUS NEW MEMBER LIVE CASINO 10%</h2>
                                    <h3>Tanggal akhir: <span>2030-10-31</span></h3>
                                </div> <button type="button" class="click-for-more-info-button" data-toggle="modal" data-target="#promotion_a5bc578c-bbe6-4672-93f5-b39f93d2b086"> Klik Untuk Info Lebih Lanjut </button>
                            </div>
                            <div id="promotion_a5bc578c-bbe6-4672-93f5-b39f93d2b086" class="modal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                                            <h4 class="modal-title"> BONUS NEW MEMBER LIVE CASINO 10% </h4>
                                        </div>
                                        <div class="modal-body">
                                            <h3>Tanggal akhir: <span>2030-10-31</span></h3>
                                            <p><span style="color: #ff0000;"><strong>BONUS NEW MEMBER LIVE CASINO 10%</strong></span></p>
                                            <p><span style="color: #ff0000;"><strong>Syarat dan Ketentuan :</strong></span></p>
                                            <ul>
                                                <li><span style="color: #ff0000;">Hanya berlaku untuk Member Baru.</span></li>
                                                <li><span style="color: #ff0000;">Minimal Deposit Pertama Rp 50.000&nbsp; (Lima puluh Ribu Rupiah).</span></li>
                                                <li><span style="color: #ff0000;">Maximum Bonus Adalah Sebesar Rp 1.000.000 (Satu Juta Rupiah).</span></li>
                                                <li><span style="color: #ff0000;">Masuk ke AKUN - KLAIM BONUS - AMBIL PROMOSI (di pilih promosi yang sesuai) dan masukan jumlah yang ingin di ikuti.</span></li>
                                                <li><span style="color: #ff0000;">Jika sudah capai syarat anda Harus masuk ke dalam ID anda klik AKUN - KLAIM BONUS.</span></li>
                                                <li><span style="color: #ff0000;">Syarat Bonus 10% adalah 10x Turnover dalam 1 hari.<br>Contoh : Deposit Rp 100.000 + 20.000 = 120.000 x TO 10X = 1.200.000<br></span></li>
                                                <li><span style="color: #ff0000;">Yang Tidak Diperbolehkan Adalah Sebagai Berikut :<br>-. Bet Kedua Sisi Yang (Safety bet)<br></span></li>
                                                <li><span style="color: #ff0000;">Transaksi yang Berakhir&nbsp;Draw / Seri&nbsp;Tidak termasuk kedalam Turn Over. Hanya dihitung dari transaksi yang menang ataupun kalah.</span></li>
                                                <li><span style="color: #ff0000;">Apabila ada kesalahan dan kesamaan&nbsp;data member, kesamaan&nbsp;alamat IP, menggunakan&nbsp;IP Luar&nbsp;(Selain Indonesia) /&nbsp;Hidden IP&nbsp;address /&nbsp;IP Proxy&nbsp;, melakukan&nbsp;taruhan dua sisi&nbsp;(safety bet / opposite betting) atau melakukan taruhan menggunakan&nbsp;mesin, Perusahaan berhak membatalkan&nbsp;Bonus&nbsp;Dan&nbsp;Kemenangan&nbsp;tersebut.</span></li>
                                                <li><span style="color: #ff0000;">Pihak <strong>Pragmatic218 </strong>Berhak menarik semua bonus dan jumlah kemenangan akan disita Jika Kedapatan Melakukan Kecurangan.</span></li>
                                                <li><span style="color: #ff0000;"><strong>Pragmatic218 </strong>memiliki kewenangan penuh untuk merubah, menghentikan dan membatalkan promosi tanpa pemberitahuan sebelumnya.</span></li>
                                                <li><span style="color: #ff0000;"><strong>BONUS NEW MEMBER LIVE CASINO 10%</strong> tidak dapat digabungkan dengan promosi yang lain.</span></li>
                                            </ul>
                                            <div class="expiration-countdown-container">
                                                <div class="expiration-countdown expiration_countdown" data-expiration-date="31-Oct-2030 12:00:00 AM">
                                                    <div data-section="days"> HARI <div data-value="days">2527</div>
                                                    </div>
                                                    <div data-section="hours"> JAM <div data-value="hours">08</div>
                                                    </div>
                                                    <div data-section="minutes"> MENIT <div data-value="minutes">23</div>
                                                    </div>
                                                    <div data-section="seconds"> DETIK <div data-value="seconds">24</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="promotion-item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_88ff90b1-012c-4ceb-a6f5-18d3ca26b41d_1698327517220.png">
                            <div class="promotion-footer">
                                <div class="promotion-label">
                                    <h2>BONUS NEW MEMBER SPORTSBOOK 20%</h2>
                                    <h3>Tanggal akhir: <span>2029-09-01</span></h3>
                                </div> <button type="button" class="click-for-more-info-button" data-toggle="modal" data-target="#promotion_88ff90b1-012c-4ceb-a6f5-18d3ca26b41d"> Klik Untuk Info Lebih Lanjut </button>
                            </div>
                            <div id="promotion_88ff90b1-012c-4ceb-a6f5-18d3ca26b41d" class="modal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                                            <h4 class="modal-title"> BONUS NEW MEMBER SPORTSBOOK 20% </h4>
                                        </div>
                                        <div class="modal-body">
                                            <h3>Tanggal akhir: <span>2029-09-01</span></h3>
                                            <p><span style="color: #ff0000;"><strong>BONUS NEW MEMBER SPORTS 20%</strong></span></p>
                                            <p><span style="color: #ff0000;"><strong>Syarat dan Ketentuan :</strong></span></p>
                                            <ul>
                                                <li><span style="color: #ff0000;">Hanya berlaku untuk Member Baru dan Semua Permainan SPORTSBOOK.</span></li>
                                                <li><span style="color: #ff0000;">Minimal Deposit Pertama Rp 50.000&nbsp; (Lima puluh Ribu Rupiah).</span></li>
                                                <li><span style="color: #ff0000;">Maximum Bonus Adalah Sebesar Rp 1.000.000 (Satu Juta Rupiah).</span></li>
                                                <li><span style="color: #ff0000;">Masuk ke AKUN - KLAIM BONUS - AMBIL PROMOSI (di pilih promosi yang sesuai) dan masukan jumlah yang ingin di ikuti.</span></li>
                                                <li><span style="color: #ff0000;">Jika sudah capai syarat anda Harus masuk ke dalam ID anda klik AKUN - KLAIM BONUS.</span></li>
                                                <li><span style="color: #ff0000;">Syarat Harus Mencapai 8x Turn Over dalam 1 hari. Contoh : Deposit Rp 100.000 X TO 8X = 800.000</span></li>
                                                <li><span style="color: #ff0000;">Yang Tidak Diperbolehkan Adalah Sebagai Berikut :<br>-.&nbsp;Bet Kedua Sisi Yang Berlawanan Home / Away &amp; Over / Under Dalam 1 Partai (&nbsp;Safety Bet atau Tanpa Aduan&nbsp;)</span></li>
                                                <li><span style="color: #ff0000;">Transaksi yang Berakhir&nbsp;Draw / Seri&nbsp;Tidak termasuk kedalam Turn Over. Hanya dihitung dari transaksi yang menang ataupun kalah.</span></li>
                                                <li><span style="color: #ff0000;">Apabila ada kesalahan dan kesamaan&nbsp;data member, kesamaan&nbsp;alamat IP, menggunakan&nbsp;IP Luar&nbsp;(Selain Indonesia) /&nbsp;Hidden IP&nbsp;address /&nbsp;IP Proxy&nbsp;, melakukan&nbsp;taruhan dua sisi&nbsp;(safety bet / opposite betting) atau melakukan taruhan menggunakan&nbsp;mesin, Perusahaan berhak membatalkan&nbsp;Bonus&nbsp;Dan&nbsp;Kemenangan&nbsp;tersebut.</span></li>
                                                <li><span style="color: #ff0000;">Pihak <strong>Pragmatic218 </strong>Berhak menarik semua bonus dan jumlah kemenangan akan disita Jika Kedapatan Melakukan Kecurangan.</span></li>
                                                <li><span style="color: #ff0000;"><strong>Pragmatic218 </strong>memiliki kewenangan penuh untuk merubah, menghentikan dan membatalkan promosi tanpa pemberitahuan sebelumnya.</span></li>
                                                <li><span style="color: #ff0000;"><strong>BONUS NEW MEMBER SPORTS 20%</strong> tidak dapat digabungkan dengan promosi yang lain.</span></li>
                                            </ul>
                                            <div class="expiration-countdown-container">
                                                <div class="expiration-countdown expiration_countdown" data-expiration-date="01-Sep-2029 12:00:00 AM">
                                                    <div data-section="days"> HARI <div data-value="days">2102</div>
                                                    </div>
                                                    <div data-section="hours"> JAM <div data-value="hours">08</div>
                                                    </div>
                                                    <div data-section="minutes"> MENIT <div data-value="minutes">23</div>
                                                    </div>
                                                    <div data-section="seconds"> DETIK <div data-value="seconds">24</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>