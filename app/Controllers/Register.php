<?php

namespace App\Controllers;

use \DateTime;
use App\Models\User;
use App\Models\Bank;
use App\Controllers\BaseController;

class Register extends BaseController
{
    public function __construct()
    {
        $this->user = new User();
        $this->bank = new Bank();
    }

    public function index()
    {
        return view('mobile/register', ['type' => 'register']);
    }

    public function store()
    {
        $username = $this->request->getVar('UserName');
        $email = $this->request->getVar('Email');
        $password = $this->request->getVar('Password');

        $created_at = $this->currentDate();

        $bank = $this->request->getVar('SelectedBank');
        $nama_rekening = $this->request->getVar('BankAccountName');
        $norek = $this->request->getVar('BankAccountNumber');

        $user = $this->user->insert(compact('username', 'email', 'password', 'created_at'));
        $user_id = $user;
        $bank = $this->bank->insert(compact('bank', 'nama_rekening', 'norek', 'user_id'));

        session()->set([
            'id' => $user,
            'username' => $username,
            'logged_in' => true,
            'created' => date_format(new DateTime(), 'd/m/Y H:i:s A')
        ]);

        if($this->isMobile()) {
            return redirect()->to('/mobile/register-done');
        }

        return redirect()->to('/desktop/register-done');
    }

    public function registered()
    {
        return view('desktop/register-done');
    }

    public function registered2()
    {
        return view('mobile/user/registered', ['type' => 'register']);
    }
}
