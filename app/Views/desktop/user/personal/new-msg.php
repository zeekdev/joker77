<?= $this->extend('layouts/desktop/personal') ?>

<?= $this->section('content') ?>
<div class="standard-form-content">
    <div class="standard-nav-bar">
        <div class="nav-bar-title">Tiket Bantuan</div>
        <div class="nav-bar-links"> <a href="/desktop/messages/inbox" data-active="false">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/message/menu/inbox.webp?v=20231205-1" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/message/menu/inbox.png?v=20231205-1" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/message/menu/inbox.png?v=20231205-1">
                </picture> Inbox <span>[0]</span>
            </a><a href="/desktop/new-message" data-active="true">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/message/menu/support-ticket.webp?v=20231205-1" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/message/menu/support-ticket.png?v=20231205-1" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/message/menu/support-ticket.png?v=20231205-1">
                </picture> Tiket Bantuan
            </a> </div>
    </div>
    <form action="/Message/NewMessageSubmit" id="form0" method="post" novalidate="novalidate"><input name="__RequestVerificationToken" type="hidden" value="E0tV8zfD7Kk4FZCHbyAOw8o1ceIPSZAidqeHS8N1F46MR_3LNuJMdSOnDfuLJamlZbJyvDQn3TWDUb9P1bMubaW_UlFI4OcyocPeNK4k7eo1">
        <div class="form-group"> <label for="Subject">Perihal</label> <select class="form-control" data-val="true" data-val-required="The Subject field is required." id="Subject" name="Subject">
                <option value="Game Play">Permainan</option>
                <option value="Transaction">Transaksi</option>
                <option value="Others">Lainnya</option>
            </select> <span class="standard-required-message"> Perihal harus diisi. </span> </div>
        <div class="form-group"> <label for="Message">Pesan Anda</label> <textarea autocomplete="off" class="form-control" cols="20" data-val="true" data-val-required="The Message field is required." id="Message" name="Message" rows="15"></textarea> <span class="standard-required-message"> Pesan harus diisi. </span> </div>
        <div class="standard-button-group"> <input type="submit" class="btn btn-primary" value="Kirim"> </div>
    </form>
</div>
<?= $this->endSection() ?>