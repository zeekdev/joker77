<?php if(session()->has('logged_in')): ?>
<div class="site-menu"> <label for="site_menu_trigger_input"></label>
    <ul>
        <li>
            <div class="side-menu-user-info"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/loyalty/badge/Bronze.svg?v=20231212-1">
                <div>
                    <div class="username"><?= session()->get('username') ?></div> Selamat Datang!
                </div>
                <div class="buttons-container"> <a href="#" class="logout-button" onclick="window.closeWindows(); document.querySelector('#logout-form').submit();">
                        <form action="/Account/Logout" id="logout-form" method="post">Keluar</form>
                    </a> </div>
            </div>
        </li>
        <li> <a href="/mobile/account-summary"> <i data-icon="my-account"> <img alt="MyAccount" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/menu/profile.svg?v=20231212-1"> </i> Akun Saya </a> </li>
        <li> <a href="/mobile/deposit"> <i data-icon="wallet"> <img alt="Wallet" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/menu/wallet.svg?v=20231212-1"> </i> Banking </a> </li>
        <li> <a href="/mobile/messages/inbox"> <i data-icon="message" data-new-notification="false"> <img alt="Message" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/menu/message.svg?v=20231212-1"> </i> Pesan </a> </li>
        <li>
            <details>
                <summary>
                    <section> <span> <i data-icon="games"> <img alt="Games" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/menu/games.svg?v=20231212-1"> </i> Games </span> <i class="glyphicon glyphicon-chevron-right"></i> </section>
                </summary>
                <article>
                    <ul>
                        <li>
                            <details>
                                <summary>
                                    <section> Hot Games <i class="glyphicon glyphicon-chevron-right"></i> </section>
                                </summary>
                                <article>
                                    <ul>
                                        <li> <a href="/mobile/slots/pragmatic"> Pragmatic Play </a> </li>
                                        <li> <a href="javascript:registerPopup({ content:'Saldo anda tidak cukup'});"> Nex4D </a> </li>
                                        <li> <a href="/mobile/slots/microgaming"> MicroGaming </a> </li>
                                        <li> <a href="/mobile/slots/habanero"> Habanero </a> </li>
                                        <li> <a href="/mobile/slots/advantplay"> AdvantPlay </a> </li>
                                        <li> <a href="/mobile/slots/pgsoft"> PG Slots </a> </li>
                                        <li> <a href="/mobile/slots/reel-kingdom"> Reel Kingdom by Pragmatic </a> </li>
                                        <li> <a href="javascript:registerPopup({ content:'Saldo anda tidak cukup'});"> ION Casino </a> </li>
                                        <li> <a href="/mobile/slots/playstar"> Playstar </a> </li>
                                    </ul>
                                </article>
                            </details>
                        </li>
                        <li>
                            <details>
                                <summary>
                                    <section> Slots <i class="glyphicon glyphicon-chevron-right"></i> </section>
                                </summary>
                                <article>
                                    <ul>
                                        <li> <a href="/mobile/slots/pragmatic"> Pragmatic Play </a> </li>
                                        <li> <a href="/mobile/slots/microgaming"> MicroGaming </a> </li>
                                        <li> <a href="/mobile/slots/pgsoft"> PG Slots </a> </li>
                                        <li> <a href="/mobile/slots/reel-kingdom"> Reel Kingdom by Pragmatic </a> </li>
                                        <li> <a href="/mobile/slots/advantplay"> AdvantPlay </a> </li>
                                        <li> <a href="/mobile/slots/no-limit-city"> No Limit City </a> </li>
                                        <li> <a href="/mobile/slots/habanero"> Habanero </a> </li>
                                        <li> <a href="/mobile/slots/joker"> Joker </a> </li>
                                        <li> <a href="/mobile/slots/jili"> Jili </a> </li>
                                        <li> <a href="/mobile/slots/spade-gaming"> Spade Gaming </a> </li>
                                        <li> <a href="/mobile/slots/live22"> Live22 </a> </li>
                                        <li> <a href="/mobile/slots/playstar"> Playstar </a> </li>
                                        <li> <a href="/mobile/slots/spinix"> Spinix </a> </li>
                                        <li> <a href="/mobile/slots/crowd-play"> Crowd Play </a> </li>
                                        <li> <a href="/mobile/slots/vpower"> VPower </a> </li>
                                    </ul>
                                </article>
                            </details>
                        </li>
                        <li>
                            <details>
                                <summary>
                                    <section> Live Casino <i class="glyphicon glyphicon-chevron-right"></i> </section>
                                </summary>
                                <article>
                                    <ul>
                                        <li> <a href="javascript:registerPopup({ content:'Saldo anda tidak cukup'});"> ION Casino </a> </li>
                                        <li> <a href="/mobile/casino/pragmatic"> PP Casino </a> </li>
                                        <li> <a href="javascript:registerPopup({ content:'Saldo anda tidak cukup'});"> MG Live </a> </li>
                                        <li> <a href="/mobile/casino/evo-gaming"> Evo Gaming </a> </li>
                                        <li> <a href="javascript:registerPopup({ content:'Saldo anda tidak cukup'});"> Sexy Baccarat </a> </li>
                                        <li> <a href="javascript:registerPopup({ content:'Saldo anda tidak cukup'});"> Pretty Gaming </a> </li>
                                        <li> <a href="javascript:registerPopup({ content:'Saldo anda tidak cukup'});"> Asia Gaming </a> </li>
                                        <li> <a href="javascript:registerPopup({ content:'Saldo anda tidak cukup'});"> AllBet </a> </li>
                                        <li> <a href="javascript:registerPopup({ content:'Saldo anda tidak cukup'});"> PGS Live </a> </li>
                                        <li> <a href="javascript:registerPopup({ content:'Saldo anda tidak cukup'});"> SA Gaming </a> </li>
                                        <li> <a href="javascript:registerPopup({ content:'Saldo anda tidak cukup'});"> Ebet </a> </li>
                                        <li> <a href="javascript:registerPopup({ content:'Saldo anda tidak cukup'});"> Dream Gaming </a> </li>
                                        <li> <a href="javascript:registerPopup({ content:'Saldo anda tidak cukup'});"> 568Win Casino </a> </li>
                                        <li> <a href="javascript:registerPopup({ content:'Saldo anda tidak cukup'});"> HKB </a> </li>
                                        <li> <a href="javascript:registerPopup({ content:'Saldo anda tidak cukup'});"> SV388 </a> </li>
                                    </ul>
                                </article>
                            </details>
                        </li>
                        <li>
                            <details>
                                <summary>
                                    <section> Togel <i class="glyphicon glyphicon-chevron-right"></i> </section>
                                </summary>
                                <article>
                                    <ul>
                                        <li> <a href="javascript:registerPopup({ content:'Saldo anda tidak cukup'});"> Nex4D </a> </li>
                                    </ul>
                                </article>
                            </details>
                        </li>
                        <li>
                            <details>
                                <summary>
                                    <section> Crash Game <i class="glyphicon glyphicon-chevron-right"></i> </section>
                                </summary>
                                <article>
                                    <ul>
                                        <li> <a href="/mobile/crash-game/pragmatic"> PP Casino </a> </li>
                                        <li> <a href="/mobile/crash-game/spribe"> Spribe </a> </li>
                                        <li> <a href="/mobile/crash-game/microgaming"> MicroGaming </a> </li>
                                        <li> <a href="/mobile/crash-game/spinix"> Spinix </a> </li>
                                        <li> <a href="/mobile/crash-game/advantplay-mini-game"> AdvantPlay Mini Game </a> </li>
                                        <li> <a href="/mobile/crash-game/joker"> Joker </a> </li>
                                        <li> <a href="/mobile/crash-game/dragoonsoft"> Dragoonsoft </a> </li>
                                        <li> <a href="/mobile/crash-game/funky-games"> Funky Games </a> </li>
                                    </ul>
                                </article>
                            </details>
                        </li>
                        <li>
                            <details>
                                <summary>
                                    <section> Arcade <i class="glyphicon glyphicon-chevron-right"></i> </section>
                                </summary>
                                <article>
                                    <ul>
                                        <li> <a href="/mobile/arcade/microgaming"> MicroGaming </a> </li>
                                        <li> <a href="/mobile/arcade/spinix"> Spinix </a> </li>
                                        <li> <a href="/mobile/arcade/spribe"> Spribe </a> </li>
                                        <li> <a href="/mobile/arcade/joker"> Joker </a> </li>
                                        <li> <a href="/mobile/arcade/fachai"> Fachai </a> </li>
                                        <li> <a href="/mobile/arcade/jili"> Jili </a> </li>
                                        <li> <a href="/mobile/arcade/amb-slot"> AMB Slot </a> </li>
                                        <li> <a href="/mobile/arcade/crowd-play"> Crowd Play </a> </li>
                                        <li> <a href="/mobile/arcade/vpower"> VPower </a> </li>
                                        <li> <a href="/mobile/arcade/worldmatch"> Worldmatch </a> </li>
                                        <li> <a href="/mobile/arcade/mario-club"> Mario Club </a> </li>
                                        <li> <a href="/mobile/arcade/dragoonsoft"> Dragoonsoft </a> </li>
                                        <li> <a href="/mobile/arcade/live22"> Live22 </a> </li>
                                        <li> <a href="/mobile/arcade/cq9"> CQ9 </a> </li>
                                        <li> <a href="/mobile/arcade/spade-gaming"> Spade Gaming </a> </li>
                                        <li> <a href="/mobile/arcade/fun-gaming"> Fun Gaming </a> </li>
                                        <li> <a href="/mobile/arcade/arcadia"> Arcadia </a> </li>
                                        <li> <a href="javascript:registerPopup({ content:'Saldo anda tidak cukup'});"> MM Tangkas </a> </li>
                                        <li> <a href="/mobile/arcade/skywind"> Skywind </a> </li>
                                        <li> <a href="/mobile/arcade/playstar"> Playstar </a> </li>
                                        <li> <a href="/mobile/arcade/jdb"> JDB </a> </li>
                                    </ul>
                                </article>
                            </details>
                        </li>
                        <li>
                            <details>
                                <summary>
                                    <section> Poker <i class="glyphicon glyphicon-chevron-right"></i> </section>
                                </summary>
                                <article>
                                    <ul>
                                        <li> <a href="javascript:registerPopup({ content:'Saldo anda tidak cukup'});"> Balak Play </a> </li>
                                        <li> <a href="javascript:registerPopup({ content:'Saldo anda tidak cukup'});"> 9Gaming </a> </li>
                                    </ul>
                                </article>
                            </details>
                        </li>
                    </ul>
                </article>
            </details>
        </li>
        <li> <a href="/mobile/statement/consolidate"> <i data-icon="reporting"> <img alt="Reporting" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/menu/reporting.svg?v=20231212-1"> </i> Riwayat Taruhan </a> </li>
        <li>
            <div class="topbar-item language-selector-container" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/flags.png?v=20231212-1);">
                <details>
                    <summary>
                        <section> <span> <i data-icon="language"> <img alt="Language" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/menu/language.svg?v=20231212-1"> </i> BHS INDONESIA </span> <i class="glyphicon glyphicon-chevron-right"></i> </section>
                    </summary>
                    <article>
                        <ul class="language-selector">
                            <li> <a href="javascript:changeLanguage('en')"> <i data-language="en"></i>
                                    <div class="language-name">
                                        <div>ENGLISH</div>
                                        <div>ENGLISH</div>
                                    </div>
                                </a> </li>
                            <li> <a href="javascript:changeLanguage('id')"> <i data-language="id"></i>
                                    <div class="language-name">
                                        <div>BHS INDONESIA</div>
                                        <div>INDONESIAN</div>
                                    </div>
                                </a> </li>
                            <li> <a href="javascript:changeLanguage('kr')"> <i data-language="kr"></i>
                                    <div class="language-name">
                                        <div>한국어</div>
                                        <div>KOREAN</div>
                                    </div>
                                </a> </li>
                            <li> <a href="javascript:changeLanguage('cn')"> <i data-language="cn"></i>
                                    <div class="language-name">
                                        <div>中文</div>
                                        <div>CHINESE</div>
                                    </div>
                                </a> </li>
                            <li> <a href="javascript:changeLanguage('jp')"> <i data-language="jp"></i>
                                    <div class="language-name">
                                        <div>日本語</div>
                                        <div>JAPANESE</div>
                                    </div>
                                </a> </li>
                            <li> <a href="javascript:changeLanguage('th')"> <i data-language="th"></i>
                                    <div class="language-name">
                                        <div>ไทย</div>
                                        <div>THAI</div>
                                    </div>
                                </a> </li>
                            <li> <a href="javascript:changeLanguage('my')"> <i data-language="my"></i>
                                    <div class="language-name">
                                        <div>မြန်မာစာ</div>
                                        <div>BURMESE</div>
                                    </div>
                                </a> </li>
                        </ul>
                    </article>
                </details>
            </div>
        </li>
        <li> <a href="/desktop/home" rel="nofollow"> <i data-icon="desktop"> <img alt="Desktop" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/menu/desktop.svg?v=20231212-1"> </i> Versi Desktop </a> </li>
    </ul>
</div>
<?php else: ?>
<div class="site-menu"> <label for="site_menu_trigger_input"></label>
    <ul>
        <li>
            <form action="/Account/Login" method="post"><input name="__RequestVerificationToken" type="hidden" value="PI2jN3EzaqXGgKk3z1ZoH7RzgwVNf31fwTGFWsNvx5fJIGpBfKAnOBC6K_Tyxe01NPtE1uq9Kgu68be3utLD19WCvxPJn2GU-TP2tMflu9g1" />
                <div class="side-menu-login-panel">
                    <div class="login-panel-item"> <i data-icon="username" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/login/username.svg?v=20231108-2);"></i> <input type="text" name="Username" placeholder="Nama Pengguna" /> </div>
                    <div class="login-panel-item"> <i data-icon="password" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/login/password.svg?v=20231108-2);"></i> <input type="password" name="Password" placeholder="Kata Sandi" /> </div>
                    <div class="buttons-container"> <input type="submit" class="login-button" value="Masuk" /> <a href="/mobile/register" class="register-button"> Daftar </a> </div>
                </div>
            </form>
        </li>
        <li>
            <details>
                <summary>
                    <section> <span> <i data-icon="games"> <img alt="Games" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/menu/games.svg?v=20231108-2" /> </i> Games </span> <i class="glyphicon glyphicon-chevron-right"></i> </section>
                </summary>
                <article>
                    <ul>
                        <li>
                            <details>
                                <summary>
                                    <section> Hot Games <i class="glyphicon glyphicon-chevron-right"></i> </section>
                                </summary>
                                <article>
                                    <ul>
                                        <li> <a href="/mobile/slots/pragmatic"> Pragmatic Play </a> </li>
                                        <li> <a href="/mobile/login"> Nex4D </a> </li>
                                        <li> <a href="/mobile/slots/microgaming"> MicroGaming </a> </li>
                                        <li> <a href="/mobile/slots/habanero"> Habanero </a> </li>
                                        <li> <a href="/mobile/slots/pgsoft"> PG Slots </a> </li>
                                        <li> <a href="/mobile/slots/jili"> Jili </a> </li>
                                        <li> <a href="/mobile/slots/reel-kingdom"> Reel Kingdom by Pragmatic </a> </li>
                                        <li> <a href="/mobile/login"> ION Casino </a> </li>
                                        <li> <a href="/mobile/login"> Balak Play </a> </li>
                                    </ul>
                                </article>
                            </details>
                        </li>
                        <li>
                            <details>
                                <summary>
                                    <section> Slots <i class="glyphicon glyphicon-chevron-right"></i> </section>
                                </summary>
                                <article>
                                    <ul>
                                        <li> <a href="/mobile/slots/pragmatic"> Pragmatic Play </a> </li>
                                        <li> <a href="/mobile/slots/microgaming"> MicroGaming </a> </li>
                                        <li> <a href="/mobile/slots/pgsoft"> PG Slots </a> </li>
                                        <li> <a href="/mobile/slots/reel-kingdom"> Reel Kingdom by Pragmatic </a> </li>
                                        <li> <a href="/mobile/slots/advantplay"> AdvantPlay </a> </li>
                                        <li> <a href="/mobile/slots/no-limit-city"> No Limit City </a> </li>
                                        <li> <a href="/mobile/slots/habanero"> Habanero </a> </li>
                                        <li> <a href="/mobile/slots/joker"> Joker </a> </li>
                                        <li> <a href="/mobile/slots/jili"> Jili </a> </li>
                                        <li> <a href="/mobile/slots/spinix"> Spinix </a> </li>
                                        <li> <a href="/mobile/slots/crowd-play"> Crowd Play </a> </li>
                                        <li> <a href="/mobile/slots/live22"> Live22 </a> </li>
                                        <li> <a href="/mobile/slots/playstar"> Playstar </a> </li>
                                        <li> <a href="/mobile/slots/vpower"> VPower </a> </li>
                                        
                                    </ul>
                                </article>
                            </details>
                        </li>
                        <li>
                            <details>
                                <summary>
                                    <section> Live Casino <i class="glyphicon glyphicon-chevron-right"></i> </section>
                                </summary>
                                <article>
                                    <ul>
                                        <li> <a href="/mobile/login"> ION Casino </a> </li>
                                        <li> <a href="/mobile/casino/pragmatic"> PP Casino </a> </li>
                                        <li> <a href="/mobile/login"> MG Live </a> </li>
                                        <li> <a href="/mobile/casino/evo-gaming"> Evo Gaming </a> </li>
                                        <li> <a href="/mobile/login"> Sexy Baccarat </a> </li>
                                        <li> <a href="/mobile/login"> Pretty Gaming </a> </li>
                                        <li> <a href="/mobile/login"> Asia Gaming </a> </li>
                                        <li> <a href="/mobile/login"> AllBet </a> </li>
                                        <li> <a href="/mobile/login"> PGS Live </a> </li>
                                        <li> <a href="/mobile/login"> SA Gaming </a> </li>
                                        <li> <a href="/mobile/login"> Ebet </a> </li>
                                        <li> <a href="/mobile/login"> Dream Gaming </a> </li>
                                        <li> <a href="/mobile/login"> 568Win Casino </a> </li>
                                        <li> <a href="/mobile/login"> HKB </a> </li>
                                        <li> <a href="/mobile/login"> SV388 </a> </li>
                                    </ul>
                                </article>
                            </details>
                        </li>
                        <li>
                            <details>
                                <summary>
                                    <section> Togel <i class="glyphicon glyphicon-chevron-right"></i> </section>
                                </summary>
                                <article>
                                    <ul>
                                        <li> <a href="/mobile/login"> Nex4D </a> </li>
                                    </ul>
                                </article>
                            </details>
                        </li>
                        <li>
                            <details>
                                <summary>
                                    <section> Crash Game <i class="glyphicon glyphicon-chevron-right"></i> </section>
                                </summary>
                                <article>
                                    <ul>
                                        <li> <a href="/mobile/crash-game/pragmatic"> PP Casino </a> </li>
                                        <li> <a href="/mobile/crash-game/spribe"> Spribe </a> </li>
                                        <li> <a href="/mobile/crash-game/microgaming"> MicroGaming </a> </li>
                                        <li> <a href="/mobile/crash-game/spinix"> Spinix </a> </li>
                                        <li> <a href="/mobile/crash-game/advantplay-mini-game"> AdvantPlay Mini Game </a> </li>
                                        <li> <a href="/mobile/crash-game/joker"> Joker </a> </li>
                                        <li> <a href="/mobile/crash-game/dragoonsoft"> Dragoonsoft </a> </li>
                                        <li> <a href="/mobile/crash-game/funky-games"> Funky Games </a> </li>
                                    </ul>
                                </article>
                            </details>
                        </li>
                        <li>
                            <details>
                                <summary>
                                    <section> Arcade <i class="glyphicon glyphicon-chevron-right"></i> </section>
                                </summary>
                                <article>
                                    <ul>
                                        <li> <a href="/mobile/arcade/microgaming"> MicroGaming </a> </li>
                                        <li> <a href="/mobile/arcade/spinix"> Spinix </a> </li>
                                        <li> <a href="/mobile/arcade/spribe"> Spribe </a> </li>
                                        <li> <a href="/mobile/arcade/joker"> Joker </a> </li>
                                        <li> <a href="/mobile/arcade/fachai"> Fachai </a> </li>
                                        <li> <a href="/mobile/arcade/jili"> Jili </a> </li>
                                        <li> <a href="/mobile/arcade/amb-slot"> AMB Slot </a> </li>
                                        <li> <a href="/mobile/arcade/crowd-play"> Crowd Play </a> </li>
                                        <li> <a href="/mobile/arcade/vpower"> VPower </a> </li>
                                        <li> <a href="/mobile/arcade/worldmatch"> Worldmatch </a> </li>
                                        <li> <a href="/mobile/arcade/mario-club"> Mario Club </a> </li>
                                        <li> <a href="/mobile/arcade/dragoonsoft"> Dragoonsoft </a> </li>
                                        <li> <a href="/mobile/arcade/live22"> Live22 </a> </li>
                                        <li> <a href="/mobile/arcade/cq9"> CQ9 </a> </li>
                                        <li> <a href="/mobile/arcade/spade-gaming"> Spade Gaming </a> </li>
                                        <li> <a href="/mobile/arcade/fun-gaming"> Fun Gaming </a> </li>
                                        <li> <a href="/mobile/arcade/arcadia"> Arcadia </a> </li>
                                        <li> <a href="/mobile/login"> MM Tangkas </a> </li>
                                        <li> <a href="/mobile/arcade/skywind"> Skywind </a> </li>
                                        <li> <a href="/mobile/arcade/playstar"> Playstar </a> </li>
                                        <li> <a href="/mobile/arcade/jdb"> JDB </a> </li>
                                    </ul>
                                </article>
                            </details>
                        </li>
                        <li>
                            <details>
                                <summary>
                                    <section> Poker <i class="glyphicon glyphicon-chevron-right"></i> </section>
                                </summary>
                                <article>
                                    <ul>
                                        <li> <a href="/mobile/login"> Balak Play </a> </li>
                                        <li> <a href="/mobile/login"> 9Gaming </a> </li>
                                    </ul>
                                </article>
                            </details>
                        </li>
                    </ul>
                </article>
            </details>
        </li>
        <li>
            <div class="topbar-item language-selector-container" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/flags.png?v=20231108-2);">
                <details>
                    <summary>
                        <section> <span> <i data-icon="language"> <img alt="Language" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/menu/language.svg?v=20231108-2" /> </i> BHS INDONESIA </span> <i class="glyphicon glyphicon-chevron-right"></i> </section>
                    </summary>
                    <article>
                        <ul class="language-selector">
                            <li> <a href="javascript:changeLanguage('en')"> <i data-language="en"></i>
                                    <div class="language-name">
                                        <div>ENGLISH</div>
                                        <div>ENGLISH</div>
                                    </div>
                                </a> </li>
                            <li> <a href="javascript:changeLanguage('id')"> <i data-language="id"></i>
                                    <div class="language-name">
                                        <div>BHS INDONESIA</div>
                                        <div>INDONESIAN</div>
                                    </div>
                                </a> </li>
                            <li> <a href="javascript:changeLanguage('kr')"> <i data-language="kr"></i>
                                    <div class="language-name">
                                        <div>한국어</div>
                                        <div>KOREAN</div>
                                    </div>
                                </a> </li>
                            <li> <a href="javascript:changeLanguage('cn')"> <i data-language="cn"></i>
                                    <div class="language-name">
                                        <div>中文</div>
                                        <div>CHINESE</div>
                                    </div>
                                </a> </li>
                            <li> <a href="javascript:changeLanguage('jp')"> <i data-language="jp"></i>
                                    <div class="language-name">
                                        <div>日本語</div>
                                        <div>JAPANESE</div>
                                    </div>
                                </a> </li>
                            <li> <a href="javascript:changeLanguage('th')"> <i data-language="th"></i>
                                    <div class="language-name">
                                        <div>ไทย</div>
                                        <div>THAI</div>
                                    </div>
                                </a> </li>
                            <li> <a href="javascript:changeLanguage('my')"> <i data-language="my"></i>
                                    <div class="language-name">
                                        <div>မြန်မာစာ</div>
                                        <div>BURMESE</div>
                                    </div>
                                </a> </li>
                        </ul>
                    </article>
                </details>
            </div>
        </li>
        <li> <a href="/desktop/home" rel="nofollow"> <i data-icon="desktop"> <img alt="Desktop" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/menu/desktop.svg?v=20231108-2" /> </i> Versi Desktop </a> </li>
    </ul>
</div>
<?php endif; ?>