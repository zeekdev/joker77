<?= $this->extend('layouts/mobile/message') ?>

<?= $this->section('content') ?>
<div class="messaging-side-menu"> <button type="button" class="back-button" onclick="history.back();"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/message/menu/back.svg?v=20231205-1"> </button>
    <div> Inbox </div>
    <div> <a href="/mobile/messages/inbox" data-count="0" data-active="true"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/message/menu/inbox.svg?v=20231205-1" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/message/menu/inbox-active.svg?v=20231205-1);"> </a> <a href="/mobile/new-message" data-active="false"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/message/menu/new-message.svg?v=20231205-1" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/message/menu/new-message-active.svg?v=20231205-1);"> </a> </div>
</div>
<div class="standard-form-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="no-message-container"> Tidak ada data. </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>