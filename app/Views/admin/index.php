<?= $this->extend('layouts/admin') ?>

<?= $this->section('content') ?>
<div class="container">
	<div class="row py-4">
		<div class="col-md-3 mb-4">
			<ul class="list-group" data-bs-theme="dark">
			  <li class="list-group-item bg-danger">
			  	<a href="/cp/home">Home</a>
			  </li>
			  <li class="list-group-item">
			  	<a href="/cp/victims">Victim</a>
			  </li>
			  <li class="list-group-item">
			  	<a href="/cp/deposit">Deposit</a>
			  </li>
			</ul>
		</div>
		<div class="col-md-9">
			<div class="row mb-3">
				<div class="col-6 col-md-3">
					<div class="card text-center mb-3" data-bs-theme="dark">
						<div class="card-body">
							<h4><b><?= $t_user ?></b></h4>
						</div>
						<div class="card-footer bg-danger">
							<a href="/cp/victims"><b>Total Victim <i class="bi bi-arrow-right-square"></i></b></a>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div class="card text-center mb-3" data-bs-theme="dark">
						<div class="card-body">
							<h4><b><?= $t_deposit ?></b></h4>
						</div>
						<div class="card-footer bg-danger">
							<a href="/cp/deposit"><b>Total Deposit <i class="bi bi-arrow-right-square"></i></b></a>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-6">
					<div class="card text-center mb-3" data-bs-theme="dark">
						<div class="card-body">
							<h4><b><?= number_format($nilai_depo['totalDepo']*1000, 0, ',', '.') ?></b></h4>
						</div>
						<div class="card-footer bg-danger">
							<b>Nilai Deposit</b>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="d-flex justify-content-between mb-2">
						<h5 class="fw-bold">Deposit Terbaru</h5>
						<a href="/cp/deposit" class="text-danger">Lainnya</a>
					</div>
					<table class="table table-bordered border-danger text-center" data-bs-theme="dark">
						<thead>
							<tr>
								<th class="bg-danger">#</th>
								<th class="bg-danger">Penerima</th>
								<th class="bg-danger">Nominal</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($depo as $key => $row): ?>
							<tr>
								<td><?= ++$key ?></td>
								<td><?= $row['target'] ?></td>
								<td><?= $row['amount'] ?>K</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				<div class="col-md-6">
					<div class="card" data-bs-theme="dark">
						<div class="card-header bg-danger">
							<h6 class="mb-0"><b>Statistik Deposit</b></h6>
						</div>
						<div class="card-body">
							<canvas id="chart" height="280"></canvas>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>
<script>
	const data = {
	  labels: [
	  	<?php foreach($depo_stat as $key => $val): ?>
	  		"<?= $val['target'] ?>",
	  	<?php endforeach; ?>
	  ],
	  datasets: [{
	    axis: 'y',
	    label: 'Jumlah Deposit',
	    data: [
	  	<?php foreach($depo_stat as $key => $val): ?>
	  		<?= $val['total'] ?>,
	  	<?php endforeach; ?>
	    ],
	    fill: false,
	    backgroundColor: [
	      'rgba(62, 191, 55, 0.2)',
	    ],
	    borderColor: [
	      'rgb(62, 191, 55)',
	    ],
	    borderWidth: 1
	  }]
	};

	const config = {
	  type: 'bar',
	  data,
	  options: {
	    indexAxis: 'y',
	  }
	};

	const myChart = new Chart(document.getElementById('chart'), config);
</script>
<?= $this->endSection() ?>