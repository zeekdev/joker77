<?= $this->extend('layouts/mobile/slot') ?>

<?= $this->section('content') ?>
<?= $this->include('components/mobile/menu-outer') ?>

<div class="game-list">
    <ul>
        <li> <a href="/mobile/slots/pragmatic">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pp.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pp.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pp.png?v=20231115">
                </picture>
            </a> Pragmatic Play </li>
        <li> <a href="/mobile/others/nex-4d">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/balak4d.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/balak4d.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/balak4d.png?v=20231115">
                </picture>
            </a> Nex4D </li>
        <li> <a href="/mobile/slots/microgaming">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/microgaming.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/microgaming.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/microgaming.png?v=20231115">
                </picture>
            </a> MicroGaming </li>
        <li> <a href="/mobile/slots/habanero">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/habanero.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/habanero.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/habanero.png?v=20231115">
                </picture>
            </a> Habanero </li>
        <li> <a href="/mobile/slots/advantplay">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/advantplay.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/advantplay.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/advantplay.png?v=20231115">
                </picture>
            </a> AdvantPlay </li>
        <li> <a href="/mobile/slots/pgsoft">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pgsoft.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pgsoft.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/pgsoft.png?v=20231115">
                </picture>
            </a> PG Slots </li>
        <li> <a href="/mobile/slots/reel-kingdom">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/reelkingdom.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/reelkingdom.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/reelkingdom.png?v=20231115">
                </picture>
            </a> Reel Kingdom by Pragmatic </li>
        <li> <a href="/mobile/casino/ion-casino">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/trg.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/trg.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/trg.png?v=20231115">
                </picture>
            </a> ION Casino </li>
        <li> <a href="/mobile/slots/playstar">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/playstar.webp?v=20231115" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/playstar.png?v=20231115" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/shortcuts/playstar.png?v=20231115">
                </picture>
            </a> Playstar </li>
    </ul>
</div>

<?= $this->include('components/mobile/popular') ?>

<?= $this->endSection() ?>