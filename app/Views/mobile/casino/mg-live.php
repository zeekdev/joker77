<?= $this->extend('layouts/mobile/casino') ?>

<?= $this->section('content') ?>
<?= $this->include('components/mobile/menu-outer') ?>
<div id="page_carousel" class="carousel slide page-carousel" data-ride="carousel" data-interval="5000">
    <ol class="carousel-indicators">
        <li class="active" data-target="#page_carousel" data-slide-to="0"></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/providers/banners/casino/mglive.jpg?v=20231115"> </div>
    </div>
</div>
<div class="game-buttons-field"> <a class="play-now" href="/mobile/login"> MAIN </a> </div>
<?= $this->endSection() ?>