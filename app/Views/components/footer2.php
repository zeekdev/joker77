<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <ul class="footer-links">
                    <li> <a href="/desktop/about-us" rel="nofollow">Tentang <?= getenv('APP_NAME') ?></a> </li>
                    <li> <a href="/desktop/responsible-gaming" rel="nofollow">Responsible Gambling</a> </li>
                    <li> <a href="/desktop/faq" rel="nofollow">Pusat Bantuan</a> </li>
                    <li> <a href="/desktop/terms-of-use" rel="nofollow">Syarat dan Ketentuan</a> </li>
                </ul>
            </div>
            <div class="col-md-4 copyright"> ©2023 <?= getenv('APP_NAME') ?>. All rights reserved | 18+ </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="footer-info-container">
                    <div class="site-info">
                        <div class="site-info-title"> <i data-icon="service" style="background-image: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/icon-sprite.png?v=20231108-2);"></i>
                            <div>
                                <h3> PELAYANAN </h3>
                                <p> Keunggulan Pelayanan </p>
                            </div>
                        </div>
                        <div class="site-info-description">
                            <h5> DEPOSIT </h5>
                            <p> Rata-Rata Waktu </p>
                            <div>
                                <div id="deposit_progress" data-average-time="1.00"></div>
                            </div>
                        </div>
                        <div class="site-info-description">
                            <h5> PENARIKAN </h5>
                            <p> Rata-Rata Waktu </p>
                            <div>
                                <div id="withdrawal_progress" data-average-time="5.00"></div>
                            </div>
                        </div>
                        <div class="site-info-description">
                            <p> * Selama bank maintenance dan bank offline deposit dan penarikan tidak dapat diproses </p>
                            <ul class="bank-list">
                                <li data-ztip-title="Senin - Jumat online 00:00-23:00 WIB Sabtu online 00:00-23:00 WIB Minggu online 00:00-23:00 WIB" data-online="true"> <img src="https://api2-82b.imgnxa.com/images/BANKBSI_be376934-0b97-4259-83ad-e0b506fb6b29_1697948252267.png" /> </li>
                                <li data-ztip-title="Senin - Jumat online 00:00-00:00 WIB Sabtu online 00:00-00:00 WIB Minggu online 00:00-00:00 WIB" data-online="true"> <img src="https://aqua-peaceful-jay-983.mypinata.cloud/ipfs/QmSXuNmg5vL1WC8iUhsVQyguqK5FgcXKiVUsmv44yoEBd3" /> </li>
                                <li data-ztip-title="Senin - Jumat online 06:00-22:00 WIB Sabtu online 06:00-22:00 WIB Minggu online 06:00-22:00 WIB" data-online="true"> <img src="https://api2-82b.imgnxa.com/images/BRI_a458ab91-91a3-49ac-98b3-1bfc5d1966bd_1699833956440.png" /> </li>
                                <li data-ztip-title="Senin - Jumat online 00:00-00:00 WIB Sabtu online 00:00-00:00 WIB Minggu online 00:00-00:00 WIB" data-online="true"> <img src="https://api2-82b.imgnxa.com/images/DANAMON_67568e69-ca77-43c8-bf9b-df628bc3b2d6_1668185529443.png" /> </li>
                                <li data-ztip-title="Senin - Jumat online 04:00-22:50 WIB Sabtu online 04:00-22:50 WIB Minggu online 04:00-22:50 WIB" data-online="true"> <img src="https://api2-82b.imgnxa.com/images/MANDIRI_ec4427ff-2e6e-4657-a2fe-b3702bc15e7c_1690996411540.png" /> </li>
                                <li data-ztip-title="Senin - Jumat online 00:00-00:00 WIB Sabtu online 00:00-00:00 WIB Minggu online 00:00-00:00 WIB" data-online="true"> <img src="https://api2-82b.imgnxa.com/images/TELKOMSEL_708c135d-74c5-482f-9d03-27a5f7035c60_1676674896407.png" /> </li>
                                <li data-ztip-title="Senin - Jumat online 00:00-00:00 WIB Sabtu online 00:00-00:00 WIB Minggu online 00:00-00:00 WIB" data-online="true"> <img src="https://api2-82b.imgnxa.com/images/XL_ea2a82b1-ca96-4eb1-9a52-cf378c6405e7_1697816326943.png" /> </li>
                            </ul>
                        </div>
                    </div>
                    <div class="site-info">
                        <div class="site-info-title"> <i data-icon="product" style="background-image: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/icon-sprite.png?v=20231108-2);"></i>
                            <div>
                                <h3> PRODUK </h3>
                                <p> Keunggulan Produk </p>
                            </div>
                        </div>
                        <div class="site-info-description with-seperator">
                            <h5> SLOTS BETTING </h5>
                            <p> Penyedia slot online dengan beragam pilihan game menarik yang memudahkan pemain untuk mencapai jackpot </p>
                        </div>
                        <div class="site-info-description with-seperator">
                            <h5> TOGEL BETTING </h5>
                            <p> Platform togel yang menarik dari perusahan terbaik di dunia yang menawarkan hadiah kemenangan besar </p>
                        </div>
                        <div class="site-info-description with-seperator">
                            <h5> LIVE CASINO BETTING </h5>
                            <p> Platform Pilihan bagi perusahaan-perusahaan terbaik di dunia, dengan pilihan variasi game terbanyak </p>
                        </div>
                        <div class="site-info-description with-seperator">
                            <h5> SPORTS BETTING </h5>
                            <p> Sportsbook Gaming Platform Terbaik menawarkan lebih banyak game, odds yang lebih tinggi, dan menyediakan pilihan yang lebih banyak untuk pemain. </p>
                        </div>
                    </div>
                    <div class="site-info">
                        <div class="site-info-title"> <i data-icon="help-and-service" style="background-image: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/icon-sprite.png?v=20231108-2);"></i>
                            <div>
                                <h3> BANTUAN &amp; DUKUNGAN </h3>
                                <p> Servis Lainnya </p>
                            </div>
                        </div>
                        <div class="site-info-description with-seperator">
                            <h5> TERHUBUNG DENGAN KAMI </h5>
                            <ul class="social-media-list"> </ul>
                        </div>
                        <div class="site-info-description with-seperator">
                            <div class="qr-codes"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr class="footer-separator" />
        <div class="row">
            <div class="col-md-5"> </div>
            <div class="col-md-3">
                <p class="footer-section-title"> Tanggung Jawab Bermain </p>
                <ul class="hover-list">
                    <li>
                        <picture>
                            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/asf.webp?v=20231108-2" type="image/webp" />
                            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/asf.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/asf.png?v=20231108-2" />
                        </picture>
                        <picture>
                            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/asf-active.webp?v=20231108-2" type="image/webp" />
                            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/asf-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/asf-active.png?v=20231108-2" />
                        </picture>
                    </li>
                    <li>
                        <picture>
                            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/gambling-support.webp?v=20231108-2" type="image/webp" />
                            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/gambling-support.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/gambling-support.png?v=20231108-2" />
                        </picture>
                        <picture>
                            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/gambling-support-active.webp?v=20231108-2" type="image/webp" />
                            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/gambling-support-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/gambling-support-active.png?v=20231108-2" />
                        </picture>
                    </li>
                    <li>
                        <picture>
                            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/18-plus.webp?v=20231108-2" type="image/webp" />
                            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/18-plus.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/18-plus.png?v=20231108-2" />
                        </picture>
                        <picture>
                            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/18-plus-active.webp?v=20231108-2" type="image/webp" />
                            <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/18-plus-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/18-plus-active.png?v=20231108-2" />
                        </picture>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 supported-browser-container">
                <div>
                    <p class="footer-section-title"> Browser Yang Didukung </p>
                    <ul class="hover-list">
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/chrome.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/chrome.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/chrome.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/chrome-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/chrome-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/chrome-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/edge.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/edge.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/edge.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/edge-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/edge-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/edge-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/firefox.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/firefox.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/firefox.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/firefox-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/firefox-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/footer/firefox-active.png?v=20231108-2" />
                            </picture>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <hr class="footer-separator" />
        <div class="row">
            <div class="col-md-12">
                <p class="footer-section-title"> Platform Penyedia Layanan </p>
                <fieldset class="provider-container">
                    <legend> Slots </legend>
                    <ul class="hover-list">
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pp.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pp.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pp.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pp-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pp-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pp-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playtech.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playtech.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playtech.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playtech-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playtech-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playtech-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/joker.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/joker.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/joker.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/joker-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/joker-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/joker-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pgsoft.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pgsoft.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pgsoft.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pgsoft-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pgsoft-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pgsoft-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/cq9.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/cq9.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/cq9.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/cq9-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/cq9-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/cq9-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/habanero.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/habanero.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/habanero.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/habanero-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/habanero-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/habanero-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/microgaming.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/microgaming.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/microgaming.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/microgaming-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/microgaming-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/microgaming-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playngo.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playngo.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playngo.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playngo-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playngo-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playngo-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/rtg.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/rtg.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/rtg.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/rtg-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/rtg-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/rtg-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spade-gaming.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spade-gaming.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spade-gaming.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spade-gaming-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spade-gaming-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spade-gaming-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/slot88.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/slot88.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/slot88.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/slot88-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/slot88-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/slot88-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/yggdrasil.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/yggdrasil.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/yggdrasil.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/yggdrasil-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/yggdrasil-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/yggdrasil-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/live22.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/live22.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/live22.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/live22-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/live22-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/live22-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ion-slot.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ion-slot.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ion-slot.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ion-slot-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ion-slot-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ion-slot-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jdb.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jdb.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jdb.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jdb-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jdb-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jdb-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/advantplay.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/advantplay.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/advantplay.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/advantplay-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/advantplay-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/advantplay-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/amb-slot.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/amb-slot.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/amb-slot.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/amb-slot-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/amb-slot-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/amb-slot-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playstar.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playstar.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playstar.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playstar-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playstar-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playstar-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ttg.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ttg.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ttg.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ttg-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ttg-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ttg-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jili.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jili.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jili.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jili-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jili-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jili-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/reel-kingdom.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/reel-kingdom.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/reel-kingdom.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/reel-kingdom-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/reel-kingdom-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/reel-kingdom-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fachai.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fachai.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fachai.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fachai-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fachai-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fachai-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/crowd-play.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/crowd-play.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/crowd-play.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/crowd-play-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/crowd-play-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/crowd-play-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/vpower.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/vpower.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/vpower.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/vpower-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/vpower-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/vpower-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fun-gaming.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fun-gaming.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fun-gaming.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fun-gaming-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fun-gaming-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fun-gaming-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dragoonsoft.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dragoonsoft.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dragoonsoft.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dragoonsoft-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dragoonsoft-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dragoonsoft-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/mario-club.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/mario-club.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/mario-club.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/mario-club-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/mario-club-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/mario-club-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/naga-games.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/naga-games.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/naga-games.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/naga-games-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/naga-games-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/naga-games-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/worldmatch.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/worldmatch.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/worldmatch.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/worldmatch-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/worldmatch-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/worldmatch-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spinix.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spinix.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spinix.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spinix-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spinix-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spinix-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/skywind.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/skywind.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/skywind.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/skywind-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/skywind-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/skywind-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/no-limit-city.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/no-limit-city.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/no-limit-city.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/no-limit-city-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/no-limit-city-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/no-limit-city-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/red-tiger.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/red-tiger.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/red-tiger.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/red-tiger-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/red-tiger-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/red-tiger-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/netent.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/netent.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/netent.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/netent-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/netent-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/netent-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/big-time-gaming.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/big-time-gaming.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/big-time-gaming.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/big-time-gaming-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/big-time-gaming-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/big-time-gaming-active.png?v=20231108-2" />
                            </picture>
                        </li>
                    </ul>
                </fieldset>
                <fieldset class="provider-container">
                    <legend> Live Casino </legend>
                    <ul class="hover-list">
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/trg.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/trg.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/trg.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/trg-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/trg-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/trg-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/568win.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/568win.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/568win.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/568win-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/568win-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/568win-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ag.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ag.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ag.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ag-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ag-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ag-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sexy-baccarat.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sexy-baccarat.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sexy-baccarat.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sexy-baccarat-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sexy-baccarat-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sexy-baccarat-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/evo-gaming.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/evo-gaming.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/evo-gaming.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/evo-gaming-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/evo-gaming-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/evo-gaming-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pretty-gaming.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pretty-gaming.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pretty-gaming.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pretty-gaming-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pretty-gaming-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pretty-gaming-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dream-gaming.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dream-gaming.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dream-gaming.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dream-gaming-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dream-gaming-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dream-gaming-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/allbet.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/allbet.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/allbet.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/allbet-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/allbet-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/allbet-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sv388.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sv388.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sv388.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sv388-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sv388-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sv388-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pgs-live.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pgs-live.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pgs-live.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pgs-live-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pgs-live-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pgs-live-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/mg-live.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/mg-live.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/mg-live.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/mg-live-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/mg-live-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/mg-live-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sa-gaming.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sa-gaming.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sa-gaming.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sa-gaming-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sa-gaming-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sa-gaming-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ebet.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ebet.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ebet.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ebet-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ebet-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ebet-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/hkb.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/hkb.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/hkb.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/hkb-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/hkb-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/hkb-active.png?v=20231108-2" />
                            </picture>
                        </li>
                    </ul>
                </fieldset>
                <fieldset class="provider-container">
                    <legend> Togel </legend>
                    <ul class="hover-list">
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/nex-4d.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/nex-4d.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/nex-4d.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/nex-4d-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/nex-4d-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/nex-4d-active.png?v=20231108-2" />
                            </picture>
                        </li>
                    </ul>
                </fieldset>
                <fieldset class="provider-container">
                    <legend> Olahraga </legend>
                    <ul class="hover-list">
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sbo.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sbo.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sbo.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sbo-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sbo-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/sbo-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ibc-sports.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ibc-sports.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ibc-sports.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ibc-sports-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ibc-sports-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/ibc-sports-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pp.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pp.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pp.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pp-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pp-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pp-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pinnacle.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pinnacle.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pinnacle.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pinnacle-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pinnacle-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pinnacle-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/wbet.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/wbet.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/wbet.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/wbet-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/wbet-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/wbet-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/opus.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/opus.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/opus.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/opus-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/opus-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/opus-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/cmd.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/cmd.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/cmd.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/cmd-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/cmd-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/cmd-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/im-sportsbook.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/im-sportsbook.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/im-sportsbook.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/im-sportsbook-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/im-sportsbook-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/im-sportsbook-active.png?v=20231108-2" />
                            </picture>
                        </li>
                    </ul>
                </fieldset>
                <fieldset class="provider-container">
                    <legend> Crash Game </legend>
                    <ul class="hover-list">
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spribe.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spribe.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spribe.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spribe-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spribe-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spribe-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/microgaming.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/microgaming.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/microgaming.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/microgaming-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/microgaming-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/microgaming-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/joker.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/joker.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/joker.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/joker-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/joker-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/joker-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dragoonsoft.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dragoonsoft.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dragoonsoft.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dragoonsoft-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dragoonsoft-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dragoonsoft-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/advantplay-mini-game.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/advantplay-mini-game.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/advantplay-mini-game.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/advantplay-mini-game-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/advantplay-mini-game-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/advantplay-mini-game-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/funky-games.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/funky-games.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/funky-games.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/funky-games-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/funky-games-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/funky-games-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pp.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pp.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pp.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pp-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pp-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pp-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spinix.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spinix.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spinix.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spinix-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spinix-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spinix-active.png?v=20231108-2" />
                            </picture>
                        </li>
                    </ul>
                </fieldset>
                <fieldset class="provider-container">
                    <legend> Arcade </legend>
                    <ul class="hover-list">
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/joker.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/joker.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/joker.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/joker-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/joker-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/joker-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/cq9.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/cq9.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/cq9.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/cq9-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/cq9-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/cq9-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/microgaming.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/microgaming.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/microgaming.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/microgaming-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/microgaming-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/microgaming-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spade-gaming.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spade-gaming.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spade-gaming.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spade-gaming-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spade-gaming-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spade-gaming-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jdb.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jdb.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jdb.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jdb-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jdb-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jdb-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/amb-slot.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/amb-slot.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/amb-slot.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/amb-slot-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/amb-slot-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/amb-slot-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/live22.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/live22.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/live22.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/live22-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/live22-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/live22-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/arcadia.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/arcadia.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/arcadia.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/arcadia-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/arcadia-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/arcadia-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playstar.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playstar.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playstar.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playstar-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playstar-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/playstar-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jili.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jili.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jili.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jili-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jili-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/jili-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fachai.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fachai.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fachai.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fachai-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fachai-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fachai-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/crowd-play.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/crowd-play.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/crowd-play.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/crowd-play-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/crowd-play-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/crowd-play-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/vpower.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/vpower.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/vpower.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/vpower-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/vpower-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/vpower-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fun-gaming.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fun-gaming.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fun-gaming.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fun-gaming-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fun-gaming-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/fun-gaming-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dragoonsoft.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dragoonsoft.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dragoonsoft.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dragoonsoft-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dragoonsoft-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/dragoonsoft-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/mario-club.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/mario-club.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/mario-club.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/mario-club-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/mario-club-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/mario-club-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spribe.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spribe.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spribe.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spribe-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spribe-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spribe-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/worldmatch.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/worldmatch.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/worldmatch.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/worldmatch-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/worldmatch-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/worldmatch-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spinix.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spinix.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spinix.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spinix-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spinix-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/spinix-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/skywind.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/skywind.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/skywind.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/skywind-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/skywind-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/skywind-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/g8tangkas.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/g8tangkas.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/g8tangkas.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/g8tangkas-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/g8tangkas-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/g8tangkas-active.png?v=20231108-2" />
                            </picture>
                        </li>
                    </ul>
                </fieldset>
                <fieldset class="provider-container">
                    <legend> Poker </legend>
                    <ul class="hover-list">
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/g8-poker.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/g8-poker.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/g8-poker.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/g8-poker-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/g8-poker-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/g8-poker-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/nine-gaming.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/nine-gaming.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/nine-gaming.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/nine-gaming-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/nine-gaming-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/nine-gaming-active.png?v=20231108-2" />
                            </picture>
                        </li>
                    </ul>
                </fieldset>
                <fieldset class="provider-container">
                    <legend> E-Sports </legend>
                    <ul class="hover-list">
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/tf-gaming.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/tf-gaming.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/tf-gaming.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/tf-gaming-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/tf-gaming-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/tf-gaming-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pinnacle-esports.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pinnacle-esports.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pinnacle-esports.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pinnacle-esports-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pinnacle-esports-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/pinnacle-esports-active.png?v=20231108-2" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/im-esports.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/im-esports.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/im-esports.png?v=20231108-2" />
                            </picture>
                            <picture>
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/im-esports-active.webp?v=20231108-2" type="image/webp" />
                                <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/im-esports-active.png?v=20231108-2" type="image/png" /><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/providers/im-esports-active.png?v=20231108-2" />
                            </picture>
                        </li>
                    </ul>
                </fieldset>
            </div>
        </div>
    </div>
</footer>