<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class CrashGame extends BaseController
{
    public function index($provider)
    {
        return view('desktop/crash/'.$provider);
    }

    public function mobile($provider)
    {
        return view('mobile/crash/'.$provider);
    }

    public function list($provider)
    {
        $json = APPPATH . 'Controllers/json/crash/'.$provider.'.json';

        if($this->isAuth()) {
            $json = APPPATH . 'Controllers/json/crash/auth/'.$provider.'.json';
        }

        $data = file_get_contents($json);
        return $this->response->setJSON($data);
    }
}
