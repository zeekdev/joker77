<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=0.30, maximum-scale=1.0, user-scalable=0">
    <title><?= getenv('APP_NAME') ?>: Daftar Situs Slot Gacor Gampang Jackpot Slot88</title>
    <meta content="<?= getenv('APP_NAME') ?> adalah situs judi slot online terpercaya yang menyediakan game slot gacor, slot88, pragmatic play, <?= getenv('APP_NAME') ?> dan slot pragmatic." name="description" />
    <meta content="slot online, slot88, judi online, slot gacor" name="keywords" />
    <meta content="id_ID" property="og:locale" />
    <meta content="website" property="og:type" />
    <meta content="<?= getenv('APP_NAME') ?>: Daftar Situs Slot Gacor Gampang Jackpot Slot88" property="og:title" />
    <meta content="<?= getenv('APP_NAME') ?> adalah situs judi slot online terpercaya yang menyediakan game slot gacor, slot88, pragmatic play, <?= getenv('APP_NAME') ?> dan slot pragmatic." property="og:description" />
    <meta content="<?= base_url() ?>" property="og:url" />
    <meta content="<?= base_url() ?>" property="og:site_name" />
    <meta content="DarkGold" name="theme-color" />
    <meta content="id-ID" name="language" />
    <meta content="ID" name="geo.region" />
    <meta content="Indonesia" name="geo.placename" />
    <meta content="website" name="categories" />
    <meta content="hCQJkRVLTs-EpjFCSNG_sbtvQaGovuq-fZTG6oyNzq4" name="google-site-verification" />
    <link rel="preload" href="/fonts/glyphicons-halflings-regular.woff" as="font" type="font/woff" crossorigin>
    <link rel="preload" href="/fonts/Lato-Regular.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="/fonts/lato-bold.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="/fonts/Open24DisplaySt.woff2" as="font" type="font/woff2" crossorigin>
    <link href="<?= base_url() ?>" rel="canonical" />
    <link href="<?= getenv('APP_ICON') ?>" rel="shortcut icon" type="image/x-icon" />
    <link href="/css/bootstrap.css" rel="stylesheet" />
    <link href="/css/deposit/desktop.css" rel="stylesheet" />
    <link href="/css/nexus-dark.css" rel="stylesheet" />

    <div style="position:fixed;bottom:80px;left:15px;z-index:10;opacity:0.98"> <a href="https://api.whatsapp.com/send?phone=<?= getenv('NO_WA') ?>" target="_blank" rel="noopener"> <img class="floatingimage" src="https://i.imgur.com/p6zgI5g.gif" alt="Whastapp" width="60" height="60"></a> </div>
    <style>
        /* normal media query */
        @media (min-width: 768px) {
            .btn-bot-wa {
                width: 260px;
                height: 85px;
                display: block;
                position: fixed;
                left: 0;
                bottom: 0;
                background: url(https://imgur.com/hQrHtBq.png) no-repeat;
                z-index: 9999;
            }
        }
        
        /* device media query */
        @media (max-width: 768px) {
            .btn-bot-wa {
         width: 70px;
         height: 70px;
         display: block;
         position: fixed;
         left: 10px;
         bottom: 60px;
                background: url(https://imgur.com/5BzQZ3h.png) no-repeat;
                z-index: 9999;
            }
        }
    </style>
    
    <link rel="amphtml" href="https://<?= getenv('APP_NAME') ?>.cz/amp/">
</head>

<body style="--expand-icon-src: url(//nx-cdn.trgwl.com/Images/icons/expand.gif?v=20231108-2);
      --collapse-icon-src: url(//nx-cdn.trgwl.com/Images/icons/collapse.gif?v=20231108-2);
      --play-icon-src: url(//nx-cdn.trgwl.com/Images/icons/play.png?v=20231108-2);
      --jquery-ui-444444-src: url(//nx-cdn.trgwl.com/Images/jquery-ui/ui-icons_444444_256x240.png?v=20231108-2);
      --jquery-ui-555555-src: url(//nx-cdn.trgwl.com/Images/jquery-ui/ui-icons_555555_256x240.png?v=20231108-2);
      --jquery-ui-ffffff-src: url(//nx-cdn.trgwl.com/Images/jquery-ui/ui-icons_ffffff_256x240.png?v=20231108-2);
      --jquery-ui-777620-src: url(//nx-cdn.trgwl.com/Images/jquery-ui/ui-icons_777620_256x240.png?v=20231108-2);
      --jquery-ui-cc0000-src: url(//nx-cdn.trgwl.com/Images/jquery-ui/ui-icons_cc0000_256x240.png?v=20231108-2);
      --jquery-ui-777777-src: url(//nx-cdn.trgwl.com/Images/jquery-ui/ui-icons_777777_256x240.png?v=20231108-2);">
    
    <?= $this->include('components/navbar') ?>

    <div class="site-content-container" data-container-background="general">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?= $this->include('components/profile-bar') ?>
                    <div class="standard-container-with-sidebar" data-style="light">
                        <?= $this->include('components/side-menu') ?>

                        <?= $this->renderSection('content') ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= $this->include('components/contact') ?>
    
    <?= $this->include('components/footer2') ?>
    <?= $this->include('components/modal') ?>

    
    <script src='/js/nexus-desktop.js' defer></script>
    <script src='/js/deposit/desktop.js' defer></script>
    <script>
        window.addEventListener('DOMContentLoaded', () => {
            initializeBankInfo({
                dropdown: document.querySelector('#deposit_bank_select'),
                translations: {
                    copied: 'Tersalin'
                }
            });

            initializeInputAmount({
                input: document.querySelector('.deposit_amount_input'),
                display: document.querySelector('#real_deposit_amount'),
                accountNumberReference: document.querySelector("#account_number_reference"),
                currency: 'IDR'
            });

            initializeOnFormSubmit({
                translations: {
                    confirmSubmit: 'Apakah anda yakin ingin deposit',
                    amount: 'Jumlah',
                    adminFee: 'Biaya Admin',
                    fromAccount: 'Akun Asal',
                    fromCryptoAddress: 'From Crypto Address',
                    cardNumber: 'Nomor Seri (SN)',
                    telephoneNumber: 'Nomor Telepon',
                    toAccount: 'Akun Tujuan'
                },
                platform: 'desktop'
            });

            initializeDepositPage({
                translations: {
                    copied: 'Tersalin',
                    recommended: 'Rekomendasi',
                    instantProcess: 'Proses Instan',
                    others: 'Pembayaran Lainnya (Proses Standar)',
                    adminFee: 'Biaya Admin'
                }
            });
        });
    </script>

</body>

</html>