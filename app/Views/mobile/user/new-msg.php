<?= $this->extend('layouts/mobile/message') ?>

<?= $this->section('content') ?>
<div class="messaging-side-menu"> <button type="button" class="back-button" onclick="history.back();"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/message/menu/back.svg?v=20231205-1"> </button>
    <div> Inbox </div>
    <div> <a href="/mobile/messages/inbox" data-count="0" data-active="true"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/message/menu/inbox.svg?v=20231205-1" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/message/menu/inbox-active.svg?v=20231205-1);"> </a> <a href="/mobile/new-message" data-active="false"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/message/menu/new-message.svg?v=20231205-1" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/message/menu/new-message-active.svg?v=20231205-1);"> </a> </div>
</div>
<div class="standard-form-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <form action="/Message/NewMessageSubmit" id="form0" method="post" novalidate="novalidate"><input name="__RequestVerificationToken" type="hidden" value="SFwhIPtRlIMa2HH6NrhektJ4gM5oIQLY90M_HJHbCOBgg8MvXD_arH92P3h1VEW0UfpBpSSw6GYmc-ag0BApcQ2pcPGmruK1tHKW5TBs8zM1">
                    <div class="form-group"> <label for="Subject">Perihal</label> <select class="form-control" data-val="true" data-val-required="The Subject field is required." id="Subject" name="Subject">
                            <option value="Game Play">Permainan</option>
                            <option value="Transaction">Transaksi</option>
                            <option value="Others">Lainnya</option>
                        </select> <span class="standard-required-message"> Perihal harus diisi. </span> </div>
                    <div class="form-group"> <label for="Message">Pesan Anda</label> <textarea autocomplete="off" class="form-control" cols="20" data-val="true" data-val-required="The Message field is required." id="Message" name="Message" rows="15"></textarea> <span class="standard-required-message"> Pesan harus diisi. </span> </div>
                    <div class="standard-button-group"> <input type="submit" class="standard-secondary-button" value="Kirim"> </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>