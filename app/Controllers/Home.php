<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index(): string
    {
        if($this->isMobile()){ 
            return view('mobile/index');
        } 
        else { 
            return view('index');
        } 
    }

    public function online()
    {
        $data = [
            "message" => null,
            "logout" => false,
            "closeCurrentGame" => false
        ];
        
        return $this->response->setJSON($data);;
    }

    public function captcha()
    {
        $image = file_get_contents(APPPATH.'captcha/'.rand(1,11).'.jpeg');

        return $this->response
            ->setStatusCode(200)
            ->setContentType('image/jpg')
            ->setBody($image)
            ->send();
    }

    public function balance()
    {
        $json = APPPATH . 'Controllers/json/balance.json';
        $data = file_get_contents($json);
        return $this->response->setJSON($data);
    }

    public function redirect()
    {
        return redirect()->to(site_url());
    }
}
