
<?= $this->extend('layouts/mobile/promotion') ?>

<?= $this->section('content') ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="top-tab-container promotion"> <a href="/mobile/promotion" data-active="true"> Semua Promosi </a> <a href="/mobile/provider-event" data-active="false"> Semua Event Provider </a> </div>
            <div class="promotion-selection"> <a href="/mobile/promotion" data-active="false"> Semua Promosi </a> <a href="/mobile/promotion/new-member" data-active="false"> Member Baru </a> <a href="/mobile/promotion/special" data-active="true"> Spesial </a></div>
            <div class="promotion-list">
                <div class="promotion-item promotion_item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_ac9ca828-06f5-4ba1-84b8-4fea9b52f5d5_1698327440910.png">
                    <div class="button-container">
                        <div class="promotion-label">
                            <h2>deposit pulsa tanpa potongan</h2>
                            <h3>Tanggal akhir: <span>-</span></h3>
                        </div> <a href="javascript:registerPopup({ content:&#39;Silahkan login terlebih dahulu.&#39; });" class="click-for-more-info-button"> Info Detail </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>