<footer class="site-footer"> 
	<a href="/mobile/home" data-active="<?= (uri_string() == 'mobile/home' || uri_string() == '') ? 'true' : 'false' ?>"> <img alt="Home" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/footer/home.svg?v=20231108-2" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/footer/home-active.svg?v=20231108-2);" /> Beranda </a> 
	<a href="/mobile/promotion" data-active="<?= (uri_string() == 'mobile/promotion') ? 'true' : 'false' ?>"> <img alt="Promotion" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/footer/promotion.svg?v=20231108-2" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/footer/promotion-active.svg?v=20231108-2);" /> Promosi </a>
	<?php if(session()->has('logged_in')): ?>
	<a href="/mobile/deposit" data-priority="true" data-active="<?= (uri_string() == 'mobile/deposit') ? 'true' : 'false' ?>">
        <img alt="Banking" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/footer/banking.svg?v=20231212-1" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/footer/banking-active.svg?v=20231212-1);">
        Banking
    </a> 
	<?php else: ?>
	<a href="/mobile/login" data-priority="true" data-active="<?= (uri_string() == 'mobile/login') ? 'true' : 'false' ?>"> <img alt="Login" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/footer/login.svg?v=20231108-2" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/footer/login-active.svg?v=20231108-2);" /> Masuk </a> 
	<?php endif; ?>
	<a href="/mobile/contact-us" data-active="false" class="live-chat-link"> <img alt="Contact Us" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/footer/live-chat.svg?v=20231108-2" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/footer/live-chat-active.svg?v=20231108-2);" /> Hub. Kami </a> 
	<?php if(session()->has('logged_in')): ?>
	<a href="/mobile/account-summary" data-active="false"> <img alt="My Account" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/footer/my-account.svg?v=20231108-2" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/footer/my-account-active.svg?v=20231108-2);" /> Akun Saya </a>
	<?php else: ?>
	<a href="/mobile/login" data-active="false"> <img alt="My Account" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/footer/my-account.svg?v=20231108-2" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/footer/my-account-active.svg?v=20231108-2);" /> Akun Saya </a>
	<?php endif; ?> 
</footer>