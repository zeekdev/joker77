<?php

#--------------------------------------------------------------------
# MENU USER (Desktop)
# - Deposit
# - WD
# - History
# - Personal
#--------------------------------------------------------------------

$routes->group("", ["filter" => "auth"], function($routes){
	// Welcome
	$routes->get('/desktop/register-done', 'Register::registered');
	$routes->get('/mobile/register-done', 'Register::registered2');
	
	#--------------------------------------------------------------------
	# MENU DEPOSIT
	#--------------------------------------------------------------------
	
	$routes->post('/Wallet/BankDeposit', 'Deposit::store');
	$routes->post('/Wallet/PulsaDeposit', 'Deposit::store');

	// Dekstop
	$routes->get('/desktop/deposit', 'Deposit::index');
	$routes->get('/desktop/deposit/BANK', 'Deposit::index');
	$routes->get('/desktop/deposit/PULSA', 'Deposit::pulsa');

	// Mobile
	$routes->get('/mobile/deposit', 'Deposit::mobile');
	$routes->get('/mobile/deposit/BANK', 'Deposit::mobile');
	$routes->get('/mobile/deposit/PULSA', 'Deposit::pulsa_mobile');

	#--------------------------------------------------------------------
	# MENU WITHDRAW
	#--------------------------------------------------------------------

	$routes->post('/Wallet/BankWithdrawal', 'Wd::store');

	// Dekstop
	$routes->get('/desktop/withdrawal', 'Wd::index');
	$routes->get('/desktop/withdrawal/BANK', 'Wd::index');

	// Mobile
	$routes->get('/mobile/withdrawal', 'Wd::mobile');

	#--------------------------------------------------------------------
	# MENU HISTORY
	#--------------------------------------------------------------------

	// Desktop
	$routes->view('/desktop/history/deposit', 'desktop/user/history/deposit');
	$routes->view('/desktop/history/withdrawal', 'desktop/user/history/wd');
	$routes->view('/desktop/statement/consolidate', 'desktop/user/history/consolidate');

	// Mobile
	$routes->view('/mobile/history/deposit', 'mobile/user/history/deposit');
	$routes->view('/mobile/history/withdrawal', 'mobile/user/history/wd');
	$routes->view('/mobile/statement/consolidate', 'mobile/user/history/consolidate');

	#--------------------------------------------------------------------
	# MENU PERSONAL
	#--------------------------------------------------------------------

	$routes->post('/Profile/BankAccount', 'Personal::save_bank');
	$routes->post('/Message/NewMessageSubmit', 'Personal::new_message');
	$routes->post('/Profile/Password', 'Personal::password');

	// Desktop
	$routes->get('/desktop/account-summary', 'Personal::index');
	$routes->get('/desktop/bank-account', 'Personal::bank_account');
	$routes->view('/desktop/messages/inbox', 'desktop/user/personal/inbox');
	$routes->view('/desktop/new-message', 'desktop/user/personal/new-msg');
	$routes->view('/desktop/password', 'desktop/user/personal/password');

	// Mobile
	$routes->get('/mobile/account-summary', 'Personal::index');
	$routes->get('/mobile/bank-account', 'Personal::bank_account2');
	$routes->get('/mobile/bank-account/bank', 'Personal::bank_account2');
	$routes->view('/mobile/password', 'mobile/user/account/password');
	$routes->view('/mobile/messages/inbox', 'mobile/user/inbox');
	$routes->view('/mobile/new-message', 'mobile/user/new-msg');
});
