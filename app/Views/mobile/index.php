<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= getenv('APP_NAME') ?>: Daftar Situs Slot Gacor Gampang Jackpot Slot88</title>
    <meta content="<?= getenv('APP_NAME') ?> adalah situs judi slot online terpercaya yang menyediakan game slot gacor, slot88, pragmatic play, dewa slot88 dan slot pragmatic." name="description" />
    <meta content="slot online, slot88, judi online, slot gacor" name="keywords" />
    <meta content="id_ID" property="og:locale" />
    <meta content="website" property="og:type" />
    <meta content="<?= getenv('APP_NAME') ?>: Daftar Situs Slot Gacor Gampang Jackpot Slot88" property="og:title" />
    <meta content="<?= getenv('APP_NAME') ?> adalah situs judi slot online terpercaya yang menyediakan game slot gacor, slot88, pragmatic play, dewa slot88 dan slot pragmatic." property="og:description" />
    <meta content="<?= base_url() ?>/" property="og:url" />
    <meta content="<?= base_url() ?>/" property="og:site_name" />
    <meta content="DarkGold" name="theme-color" />
    <meta content="id-ID" name="language" />
    <meta content="ID" name="geo.region" />
    <meta content="Indonesia" name="geo.placename" />
    <meta content="website" name="categories" />
    <meta content="hCQJkRVLTs-EpjFCSNG_sbtvQaGovuq-fZTG6oyNzq4" name="google-site-verification" />
    <link rel="preload" href="/fonts/glyphicons-halflings-regular.woff" as="font" type="font/woff" crossorigin>
    <link rel="preload" href="/fonts/Lato-Regular.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="/fonts/lato-bold.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="/fonts/Open24DisplaySt.woff2" as="font" type="font/woff2" crossorigin>
    <link href="<?= base_url() ?>" rel="canonical" />
    <link href="<?= getenv('APP_ICON') ?>" rel="shortcut icon" type="image/x-icon" />
    <link href="/css/nexus-mobile.css" rel="stylesheet" />
    <link href="/css/nexus-mobile-dark.css" rel="stylesheet" />
    <div style="position:fixed;bottom:80px;left:15px;z-index:10;opacity:0.98"> <a href="https://api.whatsapp.com/send?phone=<?= getenv('NO_WA') ?>" target="_blank" rel="noopener"> <img class="floatingimage" src="https://i.imgur.com/p6zgI5g.gif" alt="Whastapp" width="60" height="60"></a> </div>
    <style>
        /* normal media query */
        @media (min-width: 768px) {
            .btn-bot-wa {
                width: 260px;
                height: 85px;
                display: block;
                position: fixed;
                left: 0;
                bottom: 0;
                background: url(https://imgur.com/hQrHtBq.png) no-repeat;
                z-index: 9999;
            }
        }
        
        /* device media query */
        @media (max-width: 768px) {
            .btn-bot-wa {
         width: 70px;
         height: 70px;
         display: block;
         position: fixed;
         left: 10px;
         bottom: 60px;
                background: url(https://imgur.com/5BzQZ3h.png) no-repeat;
                z-index: 9999;
            }
        }
    </style>
    
    <link rel="amphtml" href="https://<?= getenv('APP_NAME') ?>.cz/amp/">
</head>

<body data-route="home" style="--expand-icon-src: url(//nx-cdn.trgwl.com/Images/icons/expand.gif?v=20231108-2);
      --collapse-icon-src: url(//nx-cdn.trgwl.com/Images/icons/collapse.gif?v=20231108-2);
      --play-icon-src: url(//nx-cdn.trgwl.com/Images/icons/play.png?v=20231108-2);
      --desktop-icon-src: url(//nx-cdn.trgwl.com/Images/icons/desktop-white.png?v=20231108-2);
      --jquery-ui-444444-src: url(//nx-cdn.trgwl.com/Images/jquery-ui/ui-icons_444444_256x240.png?v=20231108-2);
      --jquery-ui-555555-src: url(//nx-cdn.trgwl.com/Images/jquery-ui/ui-icons_555555_256x240.png?v=20231108-2);
      --jquery-ui-ffffff-src: url(//nx-cdn.trgwl.com/Images/jquery-ui/ui-icons_ffffff_256x240.png?v=20231108-2);
      --jquery-ui-777620-src: url(//nx-cdn.trgwl.com/Images/jquery-ui/ui-icons_777620_256x240.png?v=20231108-2);
      --jquery-ui-cc0000-src: url(//nx-cdn.trgwl.com/Images/jquery-ui/ui-icons_cc0000_256x240.png?v=20231108-2);
      --jquery-ui-777777-src: url(//nx-cdn.trgwl.com/Images/jquery-ui/ui-icons_777777_256x240.png?v=20231108-2);"> <input type="checkbox" id="site_menu_trigger_input" class="site-menu-trigger-input" />
    <div class="inner-body-container" id="inner_body_container">
        <div class="inner-body" id="scroll_container">
            <?= $this->include('components/mobile/header') ?>

            <div class="announcement-container">
                <div data-section="date"> <i data-icon="news" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/home/news.svg?v=20231108-2);"></i> </div>
                <div data-section="announcements">
                    <ul class="announcement-list" id="announcement_list">
                        <li>Selamat datang di situs judi slot online resmi <?= getenv('APP_NAME') ?>, minimal deposit paling murah 10rb. Main dan menangkan Jackpot besar hingga ratusan juta rupiah</li>
                        <li>Pemeliharaan Terjadwal: Playtech pada 2023-10-30 dari 12.00 PM sampai 2023-12-01 12.00 AM (GMT + 7). Selama waktu ini, Playtech permainan tidak akan tersedia. Kami memohon maaf atas ketidaknyamanan yang mungkin ditimbulkan.</li>
                        <li>Pemeliharaan Terjadwal: PP Virtual Sports pada 2023-10-13 dari 6.00 PM sampai 2023-12-13 7.00 PM (GMT + 7). Selama waktu ini, PP Virtual Sports permainan tidak akan tersedia. Kami memohon maaf atas ketidaknyamanan yang mungkin ditimbulkan.</li>
                        <li>Pemeliharaan Terjadwal: Pinnacle pada 2023-08-08 dari 12.45 PM sampai 2023-12-09 12.29 AM (GMT + 7). Selama waktu ini, Pinnacle permainan tidak akan tersedia. Kami memohon maaf atas ketidaknyamanan yang mungkin ditimbulkan.</li>
                        <li>Pemeliharaan Terjadwal: Pinnacle E-Sports pada 2023-08-08 dari 12.45 PM sampai 2023-12-09 12.29 AM (GMT + 7). Selama waktu ini, Pinnacle E-Sports permainan tidak akan tersedia. Kami memohon maaf atas ketidaknyamanan yang mungkin ditimbulkan.</li>
                        <li>Pemeliharaan Terjadwal: IM Esports pada 2023-11-12 dari 6.14 AM sampai 2023-11-21 11.59 AM (GMT + 7). Selama waktu ini, IM Esports permainan tidak akan tersedia. Kami memohon maaf atas ketidaknyamanan yang mungkin ditimbulkan.</li>
                    </ul>
                </div>
            </div>

            <div data-section="jackpot">
                <div class="progressive-jackpot" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/jackpot/container.png?v=20231108-2);">
                    <div class="winner-btn" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/jackpot/see-winner-bg.png?v=20231108-2);"> <a title="Lihat Pemenang" href="https://jp-api.nexuswlb.com/jackpot-winners" target="_blank" rel="nofollow"> Lihat Pemenang </a> </div>
                    <div class="jackpot-container" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/jackpot/jackpot-amount-bg.png?v=20231108-2);"> <span class="jackpot-currency jackpot_currency"></span><span id="progressive_jackpot" data-progressive-jackpot-url="https://jp-api.nexuswlb.com"></span> </div>
                </div>
            </div>

            <div class="main-menu-outer-container" id="main_menu_outer_container"> <i class="glyphicon glyphicon-chevron-left left_trigger"></i>
                <main> <a href="/mobile/hot-games" data-active="false"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/menu/hot-games.svg?v=20231108-2" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/menu/hot-games-active.svg?v=20231108-2);" /> Hot Games </a> <a href="/mobile/slots" data-active="false"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/menu/slots.svg?v=20231108-2" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/menu/slots-active.svg?v=20231108-2);" /> Slots </a> <a href="/mobile/casino" data-active="false"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/menu/casino.svg?v=20231108-2" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/menu/casino-active.svg?v=20231108-2);" /> Live Casino </a> <a href="/mobile/others" data-active="false"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/menu/others.svg?v=20231108-2" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/menu/others-active.svg?v=20231108-2);" /> Togel </a> <a href="/mobile/crash-game" data-active="false"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/menu/crash-game.svg?v=20231108-2" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/menu/crash-game-active.svg?v=20231108-2);" /> Crash Game </a> <a href="/mobile/arcade" data-active="false"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/menu/arcade.svg?v=20231108-2" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/menu/arcade-active.svg?v=20231108-2);" /> Arcade </a> <a href="/mobile/poker" data-active="false"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/menu/poker.svg?v=20231108-2" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/menu/poker-active.svg?v=20231108-2);" /> Poker </a> <a href="/mobile/e-sports" data-active="false"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/menu/e-sports.svg?v=20231108-2" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/menu/e-sports-active.svg?v=20231108-2);" /> E-Sports </a> </main> <i class="glyphicon glyphicon-chevron-right right_trigger"></i>
            </div>
            <div class="popular-game-title-container">
                <div class="title"> <i data-icon="popular-games" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/popular-games.png?v=20231108-2);"></i> Game Populer </div> <a href="/mobile/slots">Lebih Banyak Game</a>
            </div>
            <div class="bigger-game-list">
                <ul>
                    <li class="game_item" data-game="Nexus Gates of Olympus™"> <label class="inner-game-item"> <input type="radio" name="game-list-radio-button" /> <span class="wrapper-container">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/providers/PP/vs20nexusgates.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/providers/PP/vs20nexusgates.jpg?v=20231108-2" type="image/jpeg" /><img alt="Nexus Gates of Olympus™" loading="lazy" src="//nx-cdn.trgwl.com/Images/providers/PP/vs20nexusgates.jpg?v=20231108-2" />
                                </picture> <span class="link-container"> <a href="/mobile/login" class="play-now" data-game="Nexus Gates of Olympus™"> MAIN </a> </span>
                            </span> <span class="game-name">Nexus Gates of Olympus™</span> </label> </li>
                    <li class="game_item" data-game="Mahjong Ways 2"> <label class="inner-game-item"> <input type="radio" name="game-list-radio-button" /> <span class="wrapper-container">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/providers/PGSOFT/mahjong-ways2.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/providers/PGSOFT/mahjong-ways2.jpg?v=20231108-2" type="image/jpeg" /><img alt="Mahjong Ways 2" loading="lazy" src="//nx-cdn.trgwl.com/Images/providers/PGSOFT/mahjong-ways2.jpg?v=20231108-2" />
                                </picture> <span class="link-container"> <a href="/mobile/login" class="play-now" data-game="Mahjong Ways 2"> MAIN </a> </span>
                            </span> <span class="game-name">Mahjong Ways 2</span> </label> </li>
                    <li class="game_item" data-game="Lucky Twins Nexus"> <label class="inner-game-item"> <input type="radio" name="game-list-radio-button" /> <span class="wrapper-container">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/providers/MICROGAMING/SMG_luckyTwinsNexus.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/providers/MICROGAMING/SMG_luckyTwinsNexus.jpg?v=20231108-2" type="image/jpeg" /><img alt="Lucky Twins Nexus" loading="lazy" src="//nx-cdn.trgwl.com/Images/providers/MICROGAMING/SMG_luckyTwinsNexus.jpg?v=20231108-2" />
                                </picture> <span class="link-container"> <a href="/mobile/login" class="play-now" data-game="Lucky Twins Nexus"> MAIN </a> </span>
                            </span> <span class="game-name">Lucky Twins Nexus</span> </label> </li>
                    <li class="game_item" data-game="Aztec: Bonus Hunt"> <label class="inner-game-item"> <input type="radio" name="game-list-radio-button" /> <span class="wrapper-container">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/providers/ADVANTPLAY/AdvantPlay_10022.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/providers/ADVANTPLAY/AdvantPlay_10022.jpg?v=20231108-2" type="image/jpeg" /><img alt="Aztec: Bonus Hunt" loading="lazy" src="//nx-cdn.trgwl.com/Images/providers/ADVANTPLAY/AdvantPlay_10022.jpg?v=20231108-2" />
                                </picture> <span class="link-container"> <a href="/mobile/login" class="play-now" data-game="Aztec: Bonus Hunt"> MAIN </a> </span>
                            </span> <span class="game-name">Aztec: Bonus Hunt</span> </label> </li>
                    <li class="game_item" data-game="Hot Hot Fruit"> <label class="inner-game-item"> <input type="radio" name="game-list-radio-button" /> <span class="wrapper-container">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/providers/HABANERO/SGHotHotFruit.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/providers/HABANERO/SGHotHotFruit.jpg?v=20231108-2" type="image/jpeg" /><img alt="Hot Hot Fruit" loading="lazy" src="//nx-cdn.trgwl.com/Images/providers/HABANERO/SGHotHotFruit.jpg?v=20231108-2" />
                                </picture> <span class="link-container"> <a href="/mobile/login" class="play-now" data-game="Hot Hot Fruit"> MAIN </a> </span>
                            </span> <span class="game-name">Hot Hot Fruit</span> </label> </li>
                    <li class="game_item" data-game="Golden Lion"> <label class="inner-game-item"> <input type="radio" name="game-list-radio-button" /> <span class="wrapper-container">
                                <picture>
                                    <source srcset="//nx-cdn.trgwl.com/Images/providers/CROWDPLAY/GoldenLion.webp?v=20231108-2" type="image/webp" />
                                    <source srcset="//nx-cdn.trgwl.com/Images/providers/CROWDPLAY/GoldenLion.jpg?v=20231108-2" type="image/jpeg" /><img alt="Golden Lion" loading="lazy" src="//nx-cdn.trgwl.com/Images/providers/CROWDPLAY/GoldenLion.jpg?v=20231108-2" />
                                </picture> <span class="link-container"> <a href="/mobile/login" class="play-now" data-game="Golden Lion"> MAIN </a> </span>
                            </span> <span class="game-name">Golden Lion</span> </label> </li>
                </ul>
            </div>
            <?= $this->include('components/mobile/contact') ?>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="social-media-list"> </ul>
                        <ul class="bank-list">
                            <li>
                                <div data-online="true"> <img src="https://api2-82b.imgnxa.com/images/BANKBSI_be376934-0b97-4259-83ad-e0b506fb6b29_1697948252267.png" /> </div>
                            </li>
                            <li>
                                <div data-online="true"> <img src="https://aqua-peaceful-jay-983.mypinata.cloud/ipfs/QmSXuNmg5vL1WC8iUhsVQyguqK5FgcXKiVUsmv44yoEBd3" /> </div>
                            </li>
                            <li>
                                <div data-online="true"> <img src="https://api2-82b.imgnxa.com/images/BRI_a458ab91-91a3-49ac-98b3-1bfc5d1966bd_1699833956440.png" /> </div>
                            </li>
                            <li>
                                <div data-online="true"> <img src="https://api2-82b.imgnxa.com/images/DANAMON_67568e69-ca77-43c8-bf9b-df628bc3b2d6_1668185529443.png" /> </div>
                            </li>
                            <li>
                                <div data-online="true"> <img src="https://api2-82b.imgnxa.com/images/MANDIRI_ec4427ff-2e6e-4657-a2fe-b3702bc15e7c_1690996411540.png" /> </div>
                            </li>
                            <li>
                                <div data-online="true"> <img src="https://api2-82b.imgnxa.com/images/TELKOMSEL_708c135d-74c5-482f-9d03-27a5f7035c60_1676674896407.png" /> </div>
                            </li>
                            <li>
                                <div data-online="true"> <img src="https://api2-82b.imgnxa.com/images/XL_ea2a82b1-ca96-4eb1-9a52-cf378c6405e7_1697816326943.png" /> </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <?= $this->include('components/mobile/site-info') ?>

            <?= $this->include('components/mobile/footer') ?>
        </div>
        <?= $this->include('components/mobile/menu') ?>
    </div>
    <div id="popup_modal" class="modal popup-modal" role="dialog" data-title="">
        <div class="modal-dialog">
            <div class="modal-content" style="--desktop-popup-alert-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/popup/alert.png?v=20231108-2);;--desktop-popup-notification-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/popup/notification.png?v=20231108-2);;--mobile-popup-alert-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/popup/alert.png?v=20231108-2);;--mobile-popup-notification-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/popup/notification.png?v=20231108-2);;--event-giveaway-popper-src: url(//nx-cdn.trgwl.com/Images/giveaway/popper.png?v=20231108-2);">
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    <h4 class="modal-title" id="popup_modal_title"> </h4>
                </div>
                <div class="modal-body" id="popup_modal_body"> </div>
                <div class="modal-footer"> <button type="button" class="btn btn-primary" data-dismiss="modal" id="popup_modal_dismiss_button"> OK </button> <button type="button" class="btn btn-secondary" data-dismiss="modal" id="popup_modal_cancel_button" style="display: none"> Batal </button> <button type="button" class="btn btn-primary" id="popup_modal_confirm_button" style="display: none"> OK </button> </div>
            </div>
        </div>
    </div>
    <script src='/js/nexus-mobile.js' defer></script>
</body>

</html>