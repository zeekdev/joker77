<?= $this->extend('layouts/desktop/crash') ?>

<?= $this->section('content') ?>
<?= view('components/crash-container', ['title' => 'JOKER']) ?>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
<script>
    window.addEventListener('DOMContentLoaded', () => {
        initializeCrashGameGames({
            directoryPath: '//nx-cdn.trgwl.com/Images/providers/',
            provider: 'JOKER',
            translations: {
                playNow: 'MAIN',
                demo: 'COBA'
            }
        });
    });
</script>
<?= $this->endSection() ?>    