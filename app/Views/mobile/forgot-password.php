<?= $this->extend('layouts/mobile/auth', ['login' => true]) ?>

<?= $this->section('content') ?>
<div class="standard-form-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="forgot-password-container">
                    <i data-icon="lock" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/forgot-password/lock.svg?v=20231108-2);"></i>
                    <h3>Lupa kata sandi Anda?</h3>
                    Masukkan alamat email Anda dan kami akan mengirimkan tautan untuk mengatur ulang kata sandi Anda.
                </div>
                <form action="/Account/MobileForgotPasswordSubmit" method="post" novalidate="novalidate"><input name="__RequestVerificationToken" type="hidden" value="a22WE8Ra7A_ar42cgkQNg0WhJLO0YX1_8RDjo2zZ1A8uUcQ63omZTDmX8aqdLrDOuQOpNwetEukGSsfMqXpGQXOovuFEEbIibQpr9qLKxc41">    <div class="form-group">
                        <label for="Username">Nama Pengguna</label>
                        <input class="form-control" data-val="true" data-val-required="The Username field is required." id="forgot_password_username_input" name="Username" placeholder="Nama Pengguna" type="text" value="">
                        <span class="standard-required-message">Kolom ini tidak boleh kosong.</span>
                    </div>
                    <div class="form-group">
                        <label for="VerificationCode">Kode Verifikasi</label>
                        <div data-section="input" class="captcha-input">
                            <input autocomplete="off" class="form-control" data-val="true" data-val-required="The VerificationCode field is required." id="VerificationCode" name="VerificationCode" placeholder="Validasi" type="text" value="">
                            <span class="standard-required-message">Captcha salah.</span>
                            <div class="captcha-container">
                                <img id="captcha_image" src="/captcha">
                                <i class="glyphicon glyphicon-refresh refresh-captcha-button" id="refresh_captcha_button"></i>
                            </div>
                        </div>
                    </div>
                    <div class="standard-button-group">
                        <input type="submit" class="standard-secondary-button" value="Kirimkan saya kata sandi saya">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>