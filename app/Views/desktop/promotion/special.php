<?= $this->extend('layouts/desktop/promotion') ?>

<?= $this->section('content') ?>
<div class="site-content-container" data-container-background="promotions">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="standard-container-with-sidebar">
                    <div class="promotion-side-menu" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/promotions/category-sprite.png?v=20231129-1);">
                        <h5> Category </h5>
                        <div class="top-tab-container promotion"> <a href="/desktop/promotion" data-active="true"> Semua Promosi </a> <a href="/desktop/provider-event" data-active="false"> Semua Event Provider </a> </div> <a href="/desktop/promotion" data-active="false"> <i data-icon="all"></i> Semua Promosi </a> <a href="/desktop/promotion/new-member" data-active="false"> <i data-icon="new-member"></i> Member Baru </a> <a href="/desktop/promotion/special" data-active="true"> <i data-icon="special"></i> Spesial </a>
                    </div>
                    <div class="promotion-list">
                        <div class="promotion-item"> <img src="https://api2-82b.imgnxa.com/images/id_promo_ac9ca828-06f5-4ba1-84b8-4fea9b52f5d5_1698327440910.png">
                            <div class="promotion-footer">
                                <div class="promotion-label">
                                    <h2>deposit pulsa tanpa potongan</h2>
                                    <h3>Tanggal akhir: <span>-</span></h3>
                                </div> <button type="button" class="click-for-more-info-button" data-toggle="modal" data-target="#promotion_ac9ca828-06f5-4ba1-84b8-4fea9b52f5d5"> Klik Untuk Info Lebih Lanjut </button>
                            </div>
                            <div id="promotion_ac9ca828-06f5-4ba1-84b8-4fea9b52f5d5" class="modal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                                            <h4 class="modal-title"> deposit pulsa tanpa potongan </h4>
                                        </div>
                                        <div class="modal-body">
                                            <h3>Tanggal akhir: <span>-</span></h3>
                                            <p><em><strong>DEPOSIT PULSA TANPA POTONGAN</strong></em></p>
                                            <p>✅ Maxsimal deposit pulsa tanpa potongan perhari adalah IDR 800,000.<br>✅ DEPO pulsa tanpa potongan tidak dapat bermain TOGEL dan POKER.<br>✅ Tidak diperbolehkan mengambil bonus apapun di dalam akun.<br>✅ Jika sudah depo dan tidak bermain langsung melakukan wd maka di potong 35%.<br>✅ Syarat penarikan WD TO 3X contoh: depo IDR 20.000 x 3 TO = IDR 60.000.<br>✅ Berlaku untuk provider TELKOMSEL dan XL.</p>
                                            <p>⛔️ Pihak PRAGMATIC218 berhak membatalkan promo jika terdapat indikasi kecurangan.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>