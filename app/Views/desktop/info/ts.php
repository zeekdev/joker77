<?= $this->extend('layouts/desktop/info') ?>

<?= $this->section('content') ?>
<div class="site-content-container" data-container-background="general">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="standard-container-with-sidebar">
                    <div class="standard-side-menu">
                        <h4> Pusat Bantuan </h4> <a href="/desktop/about-us" data-active="false"> Tentang <?= getenv('APP_NAME') ?> </a> <a href="/desktop/responsible-gaming" data-active="false"> Responsible Gambling </a> <a href="/desktop/faq" data-active="false"> Pusat Bantuan </a> <a href="/desktop/terms-of-use" data-active="true"> Syarat dan Ketentuan </a>
                    </div>
                    <div class="standard-form-content">
                        <div class="info-center-container term-of-use-container">
                            <h2 class="info-center-content-title">Syarat dan Ketentuan</h2>
                            <ul>
                                <li>
                                    <p><strong>PERATURAN <?= getenv('APP_NAME') ?></strong></p>
                                    <ol>
                                        <li style="list-style-type: none;">
                                            <ol>
                                                <li>Pendaftar harus berusia 17 tahun keatas.</li>
                                                <li>Anda wajib memberikan data informasi dengan lengkap dalam formulir yang tersedia( Nomor telepon, Nomor dan Nama Rekening )</li>
                                                <li>Proses Transaksi hanya bisa dilakukan pada jam bank online.</li>
                                                <li>Proses Pemindahan saldo hanya dapat dilakukan sebanyak 2 kali dalam sehari.</li>
                                                <li>Proses Deposit dilakukan melalui Mesin ATM, Internet banking, dan sms ( Diluar dari ini, kami menolak untuk melakukan proses deposit ).</li>
                                                <li>Kami berhak menolak proses setiap member yang memanipulasi atau mencurigakan.</li>
                                                <li>Peringatan keras bagi bonus hunter / player hantu / betting syndicate tidak terima disini. Jika terindikasi adanya keganjilan dalam betlist maka kami berhak menutup akun dan menyita seluruh kredit yang ada</li>
                                                <li>Kami melarang keras penggunaan perangkat, software, "bots", program atau metode apapun yang diaplikasikan untuk menghasilkan taruhan yang tidak wajar dan merugikan pihak kami. Penutupan account akan dilakukan tanpa adanya pemberitahuan terlebih dahulu dan semua kemenangan dari taruhan yang dilakukan akan dibatalkan dan sisa saldo akan dihanguskan.</li>
                                                <li>Kami tidak menerima komplain atas Void dan Reject dalam permainan, Sebab itu diluar tanggung jawab kami.</li>
                                                <li>Kami tidak menerima pendaftaran apabila anda menggunakan IP Address Luar Negri (Malaysia, Singapore, Hongkong, Cambodia, China).</li>
                                                <li>Proses Withdraw atau penarikan tidak dapat dilakukan apabila informasi rekening tertuju berbeda seperti informasi rekening pada database kami.</li>
                                            </ol>
                                        </li>
                                    </ol>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>