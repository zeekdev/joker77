<?php

namespace App\Controllers;

use App\Models\Bank;
use App\Controllers\BaseController;

class Wd extends BaseController
{
    public function __construct()
    {
        $this->bank = new Bank();   
    }


    public function getBanks()
    {
        return $this->bank->where('user_id', session()->get('id'))->findAll();
    }

    public function index()
    {
        $banks = $this->getBanks();
        return view('desktop/user/wd/bank', compact('banks'));
    }

    public function mobile()
    {
        return view('mobile/user/wallet/wd-bank');
    }

    public function store()
    {
        return redirect()->back()->with('msg', 'ok');
    }
}
