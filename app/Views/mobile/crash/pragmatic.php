<?= $this->extend('layouts/mobile/crash') ?>

<?= $this->section('content') ?>
<div class="crash-game-games-container">
    <div class="games-filter-section">
        <div class="name-filter">
            <div class="title"> PP Casino </div> <input type="text" id="filter_input" placeholder="Cari Permainan">
        </div>
        <div class="category-filter" id="filter_categories">
            <div class="category-filter-link active" data-category=""> Semua permainan </div>
        </div>
    </div>
    <div class="bigger-game-list">
        <ul id="game_list" style="--star-on-icon: url(//nx-cdn.trgwl.com/Images/icons/star-on.svg?v=20231115); --star-off-icon: url(//nx-cdn.trgwl.com/Images/icons/star-off.svg?v=20231115);"></ul>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
<script>
    window.addEventListener('DOMContentLoaded', () => {
        initializeCrashGameGames({
            directoryPath: '//nx-cdn.trgwl.com/Images/providers/',
            provider: 'PPLIVECASINO',
            translations: {
                playNow: 'MAIN',
                demo: 'COBA',
            }
        });
    });
</script>
<?= $this->endSection() ?>