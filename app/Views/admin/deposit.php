<?= $this->extend('layouts/admin') ?>

<?= $this->section('content') ?>
<div class="container">
	<div class="row py-4">
		<div class="col-md-3 mb-4">
			<ul class="list-group" data-bs-theme="dark">
			  <li class="list-group-item">
			  	<a href="/cp/home">Home</a>
			  </li>
			  <li class="list-group-item">
			  	<a href="/cp/victims">Victim</a>
			  </li>
			  <li class="list-group-item bg-danger">
			  	<a href="/cp/deposit">Deposit</a>
			  </li>
			</ul>
		</div>
		<div class="col-md-9">
			<div class="row">
				<div class="col-md-12">
					<div class="d-flex justify-content-between mb-2">
						<h5 class="fw-bold">Deposit Masuk</h5>
					</div>
					<table class="table table-bordered border-danger text-center" data-bs-theme="dark">
						<thead>
							<tr>
								<th class="bg-danger">#</th>
								<th class="bg-danger">Tgl Masuk</th>
								<th class="bg-danger">Tujuan</th>
								<th class="bg-danger">Nominal</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($depo as $key => $row): ?>
							<tr>
								<td><?= ++$key ?></td>
								<td><?= date_format(date_create($row['created_at']), 'd/m/Y H:i:s A') ?></td>
								<td><?= $row['target'] ?></td>
								<td><?= $row['amount'] ?>K</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
					<h6 class="fw-bold">Total: <span class="text-danger"><?= $total_deposit ?></span></h6>
				</div>
			</div>
		</div>
	</div>	
</div>
<?= $this->endSection() ?>