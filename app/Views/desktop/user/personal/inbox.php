<?= $this->extend('layouts/desktop/personal') ?>

<?= $this->section('content') ?>
<div class="standard-form-content">
    <div class="standard-nav-bar">
        <div class="nav-bar-title">Inbox</div>
        <div class="nav-bar-links"> <a href="/desktop/messages/inbox" data-active="true">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/message/menu/inbox.webp?v=20231205-1" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/message/menu/inbox.png?v=20231205-1" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/message/menu/inbox.png?v=20231205-1">
                </picture> Inbox <span>[0]</span>
            </a><a href="/desktop/new-message" data-active="false">
                <picture>
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/message/menu/support-ticket.webp?v=20231205-1" type="image/webp">
                    <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/message/menu/support-ticket.png?v=20231205-1" type="image/png"><img loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/message/menu/support-ticket.png?v=20231205-1">
                </picture> Tiket Bantuan
            </a> </div>
    </div>
    <div>Tidak ada data.</div>
</div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
<script src="/js/personal/bank-account.js"></script>
<?= $this->endSection() ?>