<?= $this->extend('layouts/mobile/deposit') ?>

<?= $this->section('content') ?>
<div class="standard-form-container deposit-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ul class="bank-status-list">
                    <li data-online="false"> <img src="https://api2-82b.imgnxa.com/images/BANKBSI_be376934-0b97-4259-83ad-e0b506fb6b29_1697948252267.png"> </li>
                    <li data-online="true"> <img src="https://aqua-peaceful-jay-983.mypinata.cloud/ipfs/QmSXuNmg5vL1WC8iUhsVQyguqK5FgcXKiVUsmv44yoEBd3"> </li>
                    <li data-online="false"> <img src="https://api2-82b.imgnxa.com/images/BRI_a458ab91-91a3-49ac-98b3-1bfc5d1966bd_1699833956440.png"> </li>
                    <li data-online="true"> <img src="https://api2-82b.imgnxa.com/images/DANAMON_67568e69-ca77-43c8-bf9b-df628bc3b2d6_1668185529443.png"> </li>
                    <li data-online="false"> <img src="https://api2-82b.imgnxa.com/images/MANDIRI_ec4427ff-2e6e-4657-a2fe-b3702bc15e7c_1690996411540.png"> </li>
                    <li data-online="true"> <img src="https://api2-82b.imgnxa.com/images/TELKOMSEL_708c135d-74c5-482f-9d03-27a5f7035c60_1676674896407.png"> </li>
                    <li data-online="true"> <img src="https://api2-82b.imgnxa.com/images/XL_ea2a82b1-ca96-4eb1-9a52-cf378c6405e7_1697816326943.png"> </li>
                </ul>
                <div class="tab-menu-background-container">
                    <div class="tab-menu-container"> <a href="/mobile/deposit" data-active="true"> <i data-icon="deposit" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/deposit.svg?v=20231205-1);--active-image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/deposit-active.svg?v=20231205-1);"></i> Deposit </a> <a href="/mobile/withdrawal" data-active="false"> <i data-icon="withdrawal" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/withdrawal.svg?v=20231205-1);--active-image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/tabs/withdrawal-active.svg?v=20231205-1);"></i> Penarikan </a> </div>
                </div>
                <?php if(session()->has('msg')): ?>
                <div class="alert alert-success">
                    Permintaan deposit berhasil dikirim.
                </div>
                <?php endif; ?>
                <div class="standard-form-note deposit-note">
                    <div class="deposit-note-icon"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/wallet/deposit.svg?v=20231205-1"> </div>
                    <div class="deposit-note-content"> <span>Catatan:</span>
                        <ul>
                            <li>Untuk deposit pertama kali member harus menambah akun pembayaran terlebih dahulu.</li>
                            <li>Jika ingin deposit diluar nominal yang sudah ditentukan, harap pilih 'Akun Tujuan' lain.</li>
                            <li>Biaya admin akan diinfokan ketika proses transaksi telah selesai di proses.</li>
                        </ul>
                    </div>
                </div>
                <div class="form-group form-group-link-container"> <a href="/mobile/history/deposit">Riwayat Deposit</a> </div>
                <div class="balance-info-container">
                    <div class="total-balance">
                        <p>TOTAL SALDO</p> <span> 0.00 </span>
                    </div>
                </div>
                <form action="/Wallet/PulsaDeposit" enctype="multipart/form-data" id="deposit_form" method="post" name="depositForm" novalidate="novalidate"><input name="__RequestVerificationToken" type="hidden" value="s8qmh_uPv9y0GrcM7koKbYK-3mVzs6PIa6Xg8nXXleUOKBceOY0xW1a4FJf4DjxpTDr6jUPk2-UlerWbcYpdjbfE17-B4PSlNhS2bOASkmw1">
                    <div class="form-group deposit-form-group"> <label for="PaymentMethod">Metode Pembayaran</label>
                        <div id="payment_method_selection" class="payment-method-selection"> <input type="radio" name="PaymentType" id="payment_method_BANK" value="BANK"> <label for="payment_method_BANK"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/payment-types/BANK.svg?v=20231205-1"> <span>Bank</span> </label> <input type="radio" name="PaymentType" id="payment_method_PULSA" value="PULSA" checked=""> <label for="payment_method_PULSA"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/payment-types/PULSA.svg?v=20231205-1"> <span>Pulsa</span> </label> </div> <span class="field-validation-valid" data-valmsg-for="PaymentType" data-valmsg-replace="true"></span>
                    </div>
                    <div class="form-group deposit-form-group"> <label for="Amount">Jumlah</label>
                        <div class="deposit-amount-container"> <input autocomplete="off" class="form-control deposit_amount_input" data-val="true" data-val-required="The Amount field is required." id="Amount" name="Amount" type="text" value=""> <span class="standard-required-message">Silahkan masukan angka untuk jumlah deposit.</span>
                            <div class="deposit-amount-range"> <span id="deposit_amount_range_label">Min: 10.00 | Max: 1,000.00</span> </div>
                            <div class="real-deposit-amount" id="real_deposit_amount" data-title="Jumlah yang harus di transfer">0 (IDR)</div>
                        </div>
                    </div>
                    <div class="deposit-form-group">
                        <div class="form-group">
                            <div class="to-account-label-container"> <label for="ToAccount">Nomor Tujuan</label> <span id="view_all_available_banks">Lihat Semua</span> </div> <select name="CompanyBankId" id="deposit_bank_select" class="form-control" data-val="true" data-val-required="Pilih bank perusahaan untuk disetor">
                                <option value="XL" data-bank-name="XL" data-account-holder="" data-account-number="<?= getenv('NOMOR_XL') ?>" data-supported-banks="XL" data-is-auto-approve="false" data-conversion-rate="0.0" data-minimum-deposit-amount="10.000000" data-maximum-deposit-amount="1000.000000" data-deposit-amount-range="Min: 10.00 | Max: 1,000.00" data-high-priority="false" data-use-predefined-deposit-amounts="false" data-admin-fee="0" data-need-reveal-button="false" data-bank-id="bb90386f-2cca-48fd-8e34-2b2aae63fa15" data-payment-type="PULSA" data-qr-code="" data-qr-code-format="png" data-is-online="true" data-bank-logo="//nx-cdn.trgwl.com/Images/bank-thumbnails/XL.webp?v=20231129-1"> XL | <?= getenv('NOMOR_XL') ?> </option>
                                <option value="TELKOMSEL" data-bank-name="TELKOMSEL" data-account-holder="" data-account-number="<?= getenv('NOMOR_TELKOMSEL') ?>" data-supported-banks="TELKOMSEL" data-is-auto-approve="false" data-conversion-rate="0.0" data-minimum-deposit-amount="10.000000" data-maximum-deposit-amount="1000.000000" data-deposit-amount-range="Min: 10.00 | Max: 1,000.00" data-high-priority="false" data-use-predefined-deposit-amounts="false" data-admin-fee="0" data-need-reveal-button="false" data-bank-id="02ffd4a2-00b5-41ce-8568-190f02c04e3c" data-payment-type="PULSA" data-qr-code="" data-qr-code-format="" data-is-online="true" data-bank-logo="//nx-cdn.trgwl.com/Images/bank-thumbnails/TELKOMSEL.webp?v=20231129-1"> TELKOMSEL | <?= getenv('NOMOR_TELKOMSEL') ?> </option>
                            </select> <span class="standard-required-message">Pilih bank perusahaan untuk disetor</span>
                        </div>
                        <div class="form-group">
                            <div data-section="input" data-bank-type="pulsa" class="bank-info" id="bank_info" data-high-priority="false">
                                <div data-bank-info="header">
                                    <h1 id="bank_info_account_name"></h1>
                                    <div id="bank_info_logo" data-image-path="//nx-cdn.trgwl.com/Images/banks/"><img src="//nx-cdn.trgwl.com/Images/banks/xl.svg" style="display: block;"></div>
                                    <h3 id="bank_info_name" class="bank-name" style="display: none;">XL</h3>
                                </div>
                                <div data-bank-info="details">
                                    <h2 id="bank_info_account_no">087735570309</h2>
                                </div>
                                <div data-bank-info="qrcode" id="bank_qr_code"></div>
                                <h4 id="qr_code_note" style="display: none;">Scan/Screenshot QR Code ini untuk Transfer ke Provider yang dituju</h4>
                                <hr>
                                <div data-bank-info="actions">
                                    <div class="admin-fee-container"> Biaya Admin: <div id="admin_fee_display" class="admin-fee">0</div>
                                    </div> <button class="btn btn-secondary reveal-bank-account-button" id="reveal_bank_account_button" type="button" data-bank-id="bb90386f-2cca-48fd-8e34-2b2aae63fa15" data-payment-type="PULSA" style="display: none;"> Tunjukkan Nomor Rekening </button> <button class="copy-bank-account-button" id="copy_bank_account_button" type="button"> <span class="glyphicon glyphicon-file"></span> Salin </button>
                                </div> <input id="bank_info_account_no_hidden_input" name="ToAccountNumber" type="hidden" value="087735570309">
                            </div>
                        </div>
                        <div class="form-group">
                            <div id="available_banks_popup" class="available-banks-popup">
                                <div id="available_banks_container" class="available-banks-container" data-default-bank-icon="//nx-cdn.trgwl.com/Images/bank-thumbnails/default.webp?v=20231205-1"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group deposit-form-group" id="pulsa_deposit_method_field"> <label for="DepositMethod">Metode Deposit</label>
                        <div> <label class="checkbox-inline"> <input checked="checked" class="pulsa_deposit_method_radio_button" data-val="true" data-val-required="The DepositMethod field is required." name="DepositMethod" type="radio" value="OTHER"> Nomor Seri (SN) </label> <label class="checkbox-inline"> <input class="pulsa_deposit_method_radio_button" name="DepositMethod" type="radio" value="PHONE"> Nomor Telepon </label> </div>
                    </div>
                    <div class="form-group deposit-form-group" id="card_number_field"> <label for="CardNumber">Nomor Seri (SN)</label> <input autocomplete="off" class="form-control" data-val="true" data-val-required="The CardNumber field is required." id="CardNumber" name="CardNumber" type="text" value=""> <span class="standard-required-message">Silahkan masuk nomor SN</span>
                        <div class="serial-number-deposit-note standard-form-note deposit-note">
                            <div class="deposit-note-icon"> <img loading="lazy" src="//nx-cdn.trgwl.com/Images/wallet/deposit.svg?v=20231205-1"> </div>
                            <div class="deposit-note-content">
                                <ul>
                                    <li>Operator yang dipilih hanya menerima transfer pulsa yang disertai dengan Nomor Seri (SN)</li>
                                    <li>Jika menggunakan Transfer/Bagi Pulsa, harap masukkan SN menggunakan nomor telepon pengirim yang diawali dengan (62). Contoh : 6281312341234</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="form-group deposit-form-group" id="telephone_number_field" style="display: none;"> <label for="TelephoneNumber">Nomor Telepon</label>
                        <div data-section="input"> <input autocomplete="off" class="form-control" data-val="true" data-val-length="The field TelephoneNumber must be a string with a minimum length of 10 and a maximum length of 13." data-val-length-max="13" data-val-length-min="10" data-val-regex="The field TelephoneNumber must match the regular expression '^[0-9]+$'." data-val-regex-pattern="^[0-9]+$" data-val-required="The TelephoneNumber field is required." id="TelephoneNumber" name="TelephoneNumber" type="text" value=""> <span class="standard-required-message">Harap masukkan numerik saja</span> </div>
                    </div>
                    <div class="form-group deposit-form-group"> <label for="TransactionReceipt">Tanda Terima Transaksi</label>
                        <div data-section="input"> <input class="form-control" id="TransactionReceipt" name="TransactionReceipt" type="file" value=""> </div>
                    </div>
                    <div class="standard-button-group"> <input type="submit" class="standard-secondary-button" value="DEPOSIT"> </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>