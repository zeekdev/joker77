<?= $this->extend('layouts/desktop/history') ?>

<?= $this->section('content') ?>
<div class="standard-form-content">
    <div class="standard-nav-bar reporting-nav-bar">
        <div class="nav-bar-links"> <a href="/desktop/history/deposit" data-active="false"> DEPOSIT </a> <a href="/desktop/history/withdrawal" data-active="true"> PENARIKAN </a> <a href="/desktop/statement/consolidate" data-active="false"> RIWAYAT TARUHAN </a> </div>
    </div>
    <form action="/desktop/history/withdrawal" method="post">
        <div class="standard-reporting-control-group"> <label>Date Range</label> <input type="text" class="form-control" id="date_range_picker" data-picker="date-range" data-separator="Sampai"> <input type="hidden" name="StartingDate" id="starting_date" value="2023-12-07"> <input type="hidden" name="EndingDate" id="ending_date" value="2023-12-07"> <button type="submit" class="btn btn-primary"> Search </button> </div>
        <div class="standard-reporting-scroll-container">
            <table class="table grid_table">
                <thead>
                    <tr>
                        <th scope="col"> Nomor Tiket </th>
                        <th scope="col"> Jumlah </th>
                        <th scope="col"> Tanggal/Waktu(GMT+7) </th>
                        <th scope="col"> Status </th>
                        <th scope="col"> Tanda Terima Transaksi </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</div>
<?= $this->endSection() ?>