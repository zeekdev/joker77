<div id="forgot_password_modal" class="modal forgot-password-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    <h4 class="modal-title"> Lupa Kata Sandi? </h4>
                </div>
                <div class="modal-body">
                    <picture>
                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/forgot-password/dice-1.webp?v=20231108-2" type="image/webp" />
                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/forgot-password/dice-1.png?v=20231108-2" type="image/png" /><img class="forgot-password-dice-1" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/forgot-password/dice-1.png?v=20231108-2" />
                    </picture>
                    <picture>
                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/forgot-password/dice-2.webp?v=20231108-2" type="image/webp" />
                        <source srcset="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/forgot-password/dice-2.png?v=20231108-2" type="image/png" /><img class="forgot-password-dice-2" loading="lazy" src="//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/forgot-password/dice-2.png?v=20231108-2" />
                    </picture>
                    <form action="/Account/ForgotPasswordSubmit" data-ajax="true" data-ajax-begin="onAjaxRequestBegin" data-ajax-complete="onAjaxRequestComplete" data-ajax-method="POST" data-ajax-success="onForgotPasswordAjaxRequestSuccess" id="form0" method="post"><input name="__RequestVerificationToken" type="hidden" value="VnQ5fmHnxkroTROat7OjDT5q2kXXcqRZ_VZFAsLVCmoGhGFcu-YjsJ95URfi7ia-Lnz1k72Xx2MSnrrD-XCgLpDKneCfddb73ber3wJw7mw1" />
                        <div class="form-group">
                            <div class="alert-danger" id="forgot_password_alert"></div>
                        </div>
                        <div class="standard-inline-form-group"> <label for="Username">Nama Pengguna</label>
                            <div data-section="asterisk">*</div>
                            <div data-section="input"> <input class="form-control" data-val="true" data-val-required="The Username field is required." id="forgot_password_username_input" name="Username" placeholder="Nama Pengguna" type="text" value="" /> <span class="standard-required-message">Kolom ini tidak boleh kosong.</span> </div>
                        </div>
                        <div class="standard-inline-form-group"> <label for="VerificationCode">Kode Verifikasi</label>
                            <div data-section="asterisk">*</div>
                            <div data-section="input" class="captcha-input"> <input autocomplete="off" class="form-control" data-val="true" data-val-required="The VerificationCode field is required." id="VerificationCode" name="VerificationCode" placeholder="Validasi" type="text" value="" /> <span class="standard-required-message">Captcha salah.</span>
                                <div class="captcha-container captcha_container"> <i class="glyphicon glyphicon-refresh refresh-captcha-button refresh_captcha_button"></i> <img class="captcha_image" src="/captcha" /> </div>
                            </div>
                        </div>
                        <div class="standard-button-group"> <input type="submit" class="btn btn-primary" value="Reset Sekarang" /> </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="register_modal" class="modal register-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    <h4 class="modal-title"> DAFTAR </h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="alert-danger" id="register_alert"></div>
                        <div class="alert-success" id="register_success_alert"></div>
                    </div>
                    <form action="/Register/RegisterSubmit" enctype="multipart/form-data" id="form1" method="post">
                        <div class="standard-sub-section">
                            <div class="standard-form-title"> Informasi Pribadi </div>
                            <div class="standard-form-content form_subcategory">
                                <div class="standard-inline-form-group"> <label for="UserName">Nama Pengguna</label>
                                    <div data-section="asterisk">*</div>
                                    <div data-section="input"> <input MaxLength="12" autocomplete="off" class="form-control lowercase" data-val="true" data-val-regex="The field UserName must match the regular expression &#39;^[0-9a-zA-Z]{3,12}$&#39;." data-val-regex-pattern="^[0-9a-zA-Z]{3,12}$" data-val-required="The UserName field is required." id="UserName" name="UserName" placeholder="Nama Pengguna Anda" type="text" value="" /> <span class="standard-required-message"> Harap masukkan antara 3 - 12 karakter dalam alfanumerik! <br /> Nama pengguna tidak boleh memiliki spasi! </span> </div>
                                </div>
                                <div class="standard-inline-form-group"> <label for="Password">Kata Sandi</label>
                                    <div data-section="asterisk">*</div>
                                    <div data-section="input" class="standard-password-field"> <input MaxLength="20" class="form-control" data-val="true" data-val-regex="The field Password must match the regular expression &#39;^(?=.{8,20}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9]).*$&#39;." data-val-regex-pattern="^(?=.{8,20}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9]).*$" data-val-required="The Password field is required." id="Password" name="Password" placeholder="Kata Sandi Anda" type="password" /> <span class="standard-required-message">Password harus terdiri dari 8-20 karakter, dan harus mengandung setidaknya satu huruf besar, satu huruf kecil, dan satu angka.</span> <i class="glyphicon glyphicon-eye-close password_input_trigger"></i> </div>
                                </div>
                                <div class="standard-inline-form-group"> <label for="ConfirmedPassword">Ulangi Kata Sandi</label>
                                    <div data-section="asterisk">*</div>
                                    <div data-section="input" class="standard-password-field"> <input MaxLength="20" class="form-control" data-val="true" data-val-equalto="&#39;ConfirmedPassword&#39; and &#39;Password&#39; do not match." data-val-equalto-other="*.Password" data-val-required="The ConfirmedPassword field is required." id="ConfirmedPassword" name="ConfirmedPassword" placeholder="Ulangi Kata Sandi Anda" type="password" /> <span class="standard-required-message">Konfirmasi kata sandi tidak cocok!</span> <i class="glyphicon glyphicon-eye-close password_input_trigger"></i> </div>
                                </div>
                                <div class="standard-form-note"> <span>Catatan:</span><br>*Password harus terdiri dari 8-20 karakter.<br>*Password harus mengandung setidaknya satu huruf besar, satu huruf kecil, dan satu angka. <br>*Password tidak boleh mengandung username. </div>
                                <div class="standard-inline-form-group"> <label for="Email">Email</label>
                                    <div data-section="input"> <input MaxLength="100" autocomplete="off" class="form-control" data-val="true" data-val-email="The Email field is not a valid e-mail address." id="Email" name="Email" placeholder="Email@example.com" type="text" value="" /> <span class="standard-required-message">Email salah.</span> </div>
                                </div>
                                <div class="standard-inline-form-group">
                                    <div data-section="input"> Silakan masukkan email yang aktif untuk tujuan reset kata sandi </div>
                                </div>
                                <div class="standard-inline-form-group"> <label for="Phone">No. Kontak</label>
                                    <div data-section="asterisk">*</div>
                                    <div data-section="input" class="copy-input-button-field"> <input MaxLength="13" autocomplete="off" class="form-control" data-val="true" data-val-length="The field Phone must be a string with a minimum length of 10 and a maximum length of 13." data-val-length-max="13" data-val-length-min="10" data-val-regex="The field Phone must match the regular expression &#39;^[0-9]+$&#39;." data-val-regex-pattern="^[0-9]+$" id="Phone" name="Phone" placeholder="Nomor Telepon Anda" required="required" type="text" value="" /> <span class="standard-required-message">Nomor Kontak Harus diantara 10 dan 13 digit</span> <button class="copy-input-button" id="copy_phone_button" type="button"> <i class="glyphicon glyphicon-file"></i> </button> </div>
                                </div>
                            </div>
                        </div>
                        <div class="standard-sub-section" id="bank_account_section">
                            <div class="standard-form-title"> Informasi Pembayaran </div>
                            <div class="standard-form-content form_subcategory">
                                <div class="standard-inline-form-group"> <label for="SelectedBank">Metode Pembayaran</label>
                                    <div data-section="input"> <label> <input type="radio" name="bank_section_radio_group" value="bank" checked> BANK </label> </div>
                                </div>
                                <div class="standard-inline-form-group"> <label for="SelectedBank">Bank</label>
                                    <div data-section="asterisk">*</div>
                                    <div data-section="input"> <select class="form-control" data-val="true" data-val-required="Bank harus diisi." id="selected_bank_select" name="SelectedBank">
                                            <option value="">-- Pilih Bank --</option>
                                            <option value="BSI">BANK BSI</option>
                                            <option value="BCA">BCA</option>
                                            <option value="BNI">BNI</option>
                                            <option value="BRI">BRI</option>
                                            <option value="CIMB">CIMB</option>
                                            <option value="DANA">DANA</option>
                                            <option value="DANAMON">DANAMON</option>
                                            <option value="GOPAY">GOPAY</option>
                                            <option value="MANDIRI">MANDIRI</option>
                                            <option value="MAYBANK">MAYBANK</option>
                                            <option value="OVO">OVO</option>
                                        </select> <span class="standard-required-message">Bank harus diisi.</span> </div>
                                </div>
                                <div class="standard-inline-form-group"> <label for="BankAccountName">Nama Rekening</label>
                                    <div data-section="asterisk">*</div>
                                    <div data-section="input"> <input MaxLength="100" autocomplete="off" class="form-control" data-val="true" data-val-regex="The field BankAccountName must match the regular expression &#39;^[0-9a-zA-Z ]*$&#39;." data-val-regex-pattern="^[0-9a-zA-Z ]*$" data-val-required="Nama rekening bank harus diisi dan hanya boleh berisi karakter alfanumerik." id="bank_account_name_input" name="BankAccountName" placeholder="Nama lengkap anda sesuai dengan buku tabungan" type="text" value="" /> <span class="standard-required-message">Nama rekening bank harus diisi dan hanya boleh berisi karakter alfanumerik.</span> </div>
                                </div>
                                <div class="standard-inline-form-group"> <label for="BankAccountNumber">Nomor Rekening</label>
                                    <div data-section="asterisk">*</div>
                                    <div data-section="input"> <input MaxLength="20" autocomplete="off" class="form-control" data-val="true" data-val-regex="The field BankAccountNumber must match the regular expression &#39;^[0-9]+$&#39;." data-val-regex-pattern="^[0-9]+$" data-val-required="Rekening bank harus diisi. (Hanya nomor)" id="bank_account_number_input" name="BankAccountNumber" placeholder="Nomor rekening anda" type="text" value="" /> <span class="standard-required-message">Rekening bank harus diisi. (Hanya nomor)</span> </div>
                                </div>
                            </div>
                        </div>
                        <div class="standard-sub-section">
                            <div class="standard-form-title"> Lainnya </div>
                            <div class="standard-form-content form_subcategory">
                                <div class="standard-inline-form-group"> <label for="VerificationCode">Kode Verifikasi</label>
                                    <div data-section="asterisk">*</div>
                                    <div data-section="input" class="captcha-input"> <input autocomplete="off" class="form-control" data-val="true" data-val-required="The VerificationCode field is required." id="register_verification_code" name="VerificationCode" placeholder="Validasi" type="text" value="" /> <span class="standard-required-message">Silakan masukkan captcha!</span>
                                        <div class="captcha-container captcha_container"> <i class="glyphicon glyphicon-refresh refresh-captcha-button refresh_captcha_button"></i> <img class="captcha_image" src="/captcha" /> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="standard-button-group"> <input type="submit" class="btn btn-primary" value="Daftar" /> </div>
                    </form>
                    <div class="register-page-reminder"> Dengan meng-klik tombol DAFTAR, saya menyatakan bahwa saya berumur diatas 18 tahun dan telah membaca dan menyetujui syarat &amp; ketentuan <?= getenv('APP_NAME') ?>. </div>
                    <div class="register-page-link"> <a href="javascript:window.openPopup('/desktop/terms-of-use','T&C-Page')"> SYARAT &amp; KETENTUAN </a> </div>
                </div>
            </div>
        </div>
    </div>
    <div id="popup_modal" class="modal popup-modal" role="dialog" data-title="">
        <div class="modal-dialog">
            <div class="modal-content" style="--desktop-popup-alert-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/popup/alert.png?v=20231108-2);;--desktop-popup-notification-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/desktop/layout/popup/notification.png?v=20231108-2);;--mobile-popup-alert-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/popup/alert.png?v=20231108-2);;--mobile-popup-notification-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/popup/notification.png?v=20231108-2);;--event-giveaway-popper-src: url(//nx-cdn.trgwl.com/Images/giveaway/popper.png?v=20231108-2);">
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    <h4 class="modal-title" id="popup_modal_title"> </h4>
                </div>
                <div class="modal-body" id="popup_modal_body"> </div>
                <div class="modal-footer"> <button type="button" class="btn btn-primary" data-dismiss="modal" id="popup_modal_dismiss_button"> OK </button> <button type="button" class="btn btn-secondary" data-dismiss="modal" id="popup_modal_cancel_button" style="display: none"> Batal </button> <button type="button" class="btn btn-primary" id="popup_modal_confirm_button" style="display: none"> OK </button> </div>
            </div>
        </div>
    </div>