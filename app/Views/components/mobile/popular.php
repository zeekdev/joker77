<div data-section="jackpot">
    <div class="progressive-jackpot" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/jackpot/container.png?v=20231108-2);">
        <div class="winner-btn" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/jackpot/see-winner-bg.png?v=20231108-2);"> <a title="Lihat Pemenang" href="https://jp-api.nexuswlb.com/jackpot-winners" target="_blank" rel="nofollow"> Lihat Pemenang </a> </div>
        <div class="jackpot-container" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/jackpot/jackpot-amount-bg.png?v=20231108-2);"> <span class="jackpot-currency jackpot_currency"></span><span id="progressive_jackpot" data-progressive-jackpot-url="https://jp-api.nexuswlb.com"></span> </div>
    </div>
</div>

<div class="popular-game-title-container">
    <div class="title"> <i data-icon="popular-games" style="--image-src: url(//nx-cdn.trgwl.com/Images/nexus-beta/dark-red/mobile/layout/popular-games.png?v=20231108-2);"></i> Game Populer </div> <a href="/mobile/slots">Lebih Banyak Game</a>
</div>
<div class="bigger-game-list">
    <ul>
        <li class="game_item" data-game="Nexus Gates of Olympus™"> <label class="inner-game-item"> <input type="radio" name="game-list-radio-button" /> <span class="wrapper-container">
                    <picture>
                        <source srcset="//nx-cdn.trgwl.com/Images/providers/PP/vs20nexusgates.webp?v=20231108-2" type="image/webp" />
                        <source srcset="//nx-cdn.trgwl.com/Images/providers/PP/vs20nexusgates.jpg?v=20231108-2" type="image/jpeg" /><img alt="Nexus Gates of Olympus™" loading="lazy" src="//nx-cdn.trgwl.com/Images/providers/PP/vs20nexusgates.jpg?v=20231108-2" />
                    </picture> <span class="link-container"> <a href="/mobile/login" class="play-now" data-game="Nexus Gates of Olympus™"> MAIN </a> </span>
                </span> <span class="game-name">Nexus Gates of Olympus™</span> </label> </li>
        <li class="game_item" data-game="Mahjong Ways 2"> <label class="inner-game-item"> <input type="radio" name="game-list-radio-button" /> <span class="wrapper-container">
                    <picture>
                        <source srcset="//nx-cdn.trgwl.com/Images/providers/PGSOFT/mahjong-ways2.webp?v=20231108-2" type="image/webp" />
                        <source srcset="//nx-cdn.trgwl.com/Images/providers/PGSOFT/mahjong-ways2.jpg?v=20231108-2" type="image/jpeg" /><img alt="Mahjong Ways 2" loading="lazy" src="//nx-cdn.trgwl.com/Images/providers/PGSOFT/mahjong-ways2.jpg?v=20231108-2" />
                    </picture> <span class="link-container"> <a href="/mobile/login" class="play-now" data-game="Mahjong Ways 2"> MAIN </a> </span>
                </span> <span class="game-name">Mahjong Ways 2</span> </label> </li>
        <li class="game_item" data-game="Lucky Twins Nexus"> <label class="inner-game-item"> <input type="radio" name="game-list-radio-button" /> <span class="wrapper-container">
                    <picture>
                        <source srcset="//nx-cdn.trgwl.com/Images/providers/MICROGAMING/SMG_luckyTwinsNexus.webp?v=20231108-2" type="image/webp" />
                        <source srcset="//nx-cdn.trgwl.com/Images/providers/MICROGAMING/SMG_luckyTwinsNexus.jpg?v=20231108-2" type="image/jpeg" /><img alt="Lucky Twins Nexus" loading="lazy" src="//nx-cdn.trgwl.com/Images/providers/MICROGAMING/SMG_luckyTwinsNexus.jpg?v=20231108-2" />
                    </picture> <span class="link-container"> <a href="/mobile/login" class="play-now" data-game="Lucky Twins Nexus"> MAIN </a> </span>
                </span> <span class="game-name">Lucky Twins Nexus</span> </label> </li>
        <li class="game_item" data-game="Aztec: Bonus Hunt"> <label class="inner-game-item"> <input type="radio" name="game-list-radio-button" /> <span class="wrapper-container">
                    <picture>
                        <source srcset="//nx-cdn.trgwl.com/Images/providers/ADVANTPLAY/AdvantPlay_10022.webp?v=20231108-2" type="image/webp" />
                        <source srcset="//nx-cdn.trgwl.com/Images/providers/ADVANTPLAY/AdvantPlay_10022.jpg?v=20231108-2" type="image/jpeg" /><img alt="Aztec: Bonus Hunt" loading="lazy" src="//nx-cdn.trgwl.com/Images/providers/ADVANTPLAY/AdvantPlay_10022.jpg?v=20231108-2" />
                    </picture> <span class="link-container"> <a href="/mobile/login" class="play-now" data-game="Aztec: Bonus Hunt"> MAIN </a> </span>
                </span> <span class="game-name">Aztec: Bonus Hunt</span> </label> </li>
        <li class="game_item" data-game="Hot Hot Fruit"> <label class="inner-game-item"> <input type="radio" name="game-list-radio-button" /> <span class="wrapper-container">
                    <picture>
                        <source srcset="//nx-cdn.trgwl.com/Images/providers/HABANERO/SGHotHotFruit.webp?v=20231108-2" type="image/webp" />
                        <source srcset="//nx-cdn.trgwl.com/Images/providers/HABANERO/SGHotHotFruit.jpg?v=20231108-2" type="image/jpeg" /><img alt="Hot Hot Fruit" loading="lazy" src="//nx-cdn.trgwl.com/Images/providers/HABANERO/SGHotHotFruit.jpg?v=20231108-2" />
                    </picture> <span class="link-container"> <a href="/mobile/login" class="play-now" data-game="Hot Hot Fruit"> MAIN </a> </span>
                </span> <span class="game-name">Hot Hot Fruit</span> </label> </li>
        <li class="game_item" data-game="Golden Lion"> <label class="inner-game-item"> <input type="radio" name="game-list-radio-button" /> <span class="wrapper-container">
                    <picture>
                        <source srcset="//nx-cdn.trgwl.com/Images/providers/CROWDPLAY/GoldenLion.webp?v=20231108-2" type="image/webp" />
                        <source srcset="//nx-cdn.trgwl.com/Images/providers/CROWDPLAY/GoldenLion.jpg?v=20231108-2" type="image/jpeg" /><img alt="Golden Lion" loading="lazy" src="//nx-cdn.trgwl.com/Images/providers/CROWDPLAY/GoldenLion.jpg?v=20231108-2" />
                    </picture> <span class="link-container"> <a href="/mobile/login" class="play-now" data-game="Golden Lion"> MAIN </a> </span>
                </span> <span class="game-name">Golden Lion</span> </label> </li>
    </ul>
</div>